-- Manage mouse hover and clicking
-- Handles picking 3d objects

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pSprite

property pSceneCamera
property pOverlayCamera

property plActiveSceneModels
property plActiveOverlayModels
property plModelDescriptions

property pMouseHoverObject
property pMouseDownObject

property pRMBDown

property pX
property pY

property pLockMouse
property pMouseLoc

property pDisableCursor


on new me, _parent
  parent = _parent
  pSprite = parent.pSprite

  pSceneCamera = parent.pSceneCamera
  pOverlayCamera = parent.pOverlayCamera

  pMouseHoverObject = void

  pSprite.scriptInstanceList.add(me)

  plModelDescriptions = [:]
  plActiveSceneModels = []
  plActiveOverlayModels = []

  pMouseLoc = the mouseLoc

  if the platform contains "Macintosh" then the emulateMultibuttonMouse = true

  return me
end

on destroy me
  pSprite.scriptInstanceList.deleteOne(me)
end

on mouseDown me
  parent.pActive = true

  me.showCursor()

  tDoNavigator = true
  if not voidP(pMouseHoverObject) then
    if pMouseHoverObject.pHasClickAction then
      if pMouseHoverObject.pElementType = #ui then tDoNavigator = false

      pMouseDownObject = pMouseHoverObject
      pMouseHoverObject.mouseDownEvent()
    end if
  end if

  if tDoNavigator then parent.poNavigator.mouseDownEvent()
end

on rightMouseDown me
  me.showCursor()
end


on mouseUp me
  me.showCursor()
end

on mouseEnter me
  me.showCursor()
end

on enterFrame me
  if parent.pError or not parent.pActive then exit
  if not pLockMouse then pMouseLoc = the mouseLoc
  me.showCursor()

  pX = the mouseH
  pY = the mouseV

  -- check still being down
  if not voidP(pMouseDownObject) and the mouseDown = false then
    pMouseDownObject.mouseUpEvent()
    pMouseDownObject = VOID
  end if

  -- handle picking
  if plActiveOverlayModels.count > 0 or plActiveSceneModels.count > 0 then
    tlPickData = []
    tPickedObject = [:]

    if pMouseLoc.inside(pOverlayCamera.rect) then
      if plActiveOverlayModels.count > 0 then
        tPickedObject = pickModels(pMouseLoc, pOverlayCamera, plActiveOverlayModels)
      end if

      -- If no picked model found in overlays, look in scene
      if tPickedObject=[:] and pMouseLoc.inside(pSceneCamera.rect) and plActiveSceneModels.count > 0 then
        tPickedObject = pickModels(pMouseLoc - point(pSceneCamera.rect.left, pSceneCamera.rect.top), \
                                   pSceneCamera, plActiveSceneModels)
      end if
    end if

    if tPickedObject[#object] <> pMouseHoverObject then
      if not voidP(pMouseHoverObject) then pMouseHoverObject.mouseLeaveEvent()
      if not voidP(tPickedObject[#object]) then tPickedObject[#object].mouseEnterEvent()

      pMouseHoverObject = tPickedObject[#object]
    end if
  else
    if not voidP(pMouseHoverObject) then pMouseHoverObject.mouseLeaveEvent()
    pMouseHoverObject = VOID
  end if

  pRMBDown = the rightMouseDown
end

on pickModels aPickPoint, aCamera, alActiveModels
  tPickedObject = [:]

  -- last minute check to remove hidden elements
  if parent.pSWV>=10 then
    tlPickData = aCamera.modelsUnderLoc(aPickPoint, [#levelOfDetail: #detailed, #modelList:alActiveModels])
  else
    tlPickData = aCamera.modelsUnderLoc(aPickPoint, 99, #detailed)
    repeat with tPick = tlPickData.count down to 1
      if alActiveModels.getOne(tlPickData[tPick][#model]) = 0 then
        tlPickData.deleteAt(tPick)
      end if
    end repeat
  end if

  if tlPickData.count > 0 then
    if tlPickData.count = 1 then
      tPickedObject = checkHit(tlPickData[1])
    else
      repeat with tlPick in tlPickData
        tIndex = alActiveModels.getOne(tlPick[#model])
        if tIndex = 0 then tIndex = 255

        tlPick[#pickindex] = tIndex
      end repeat
      tlPickData = sortProps(tlPickData, #pickIndex)

      tIndex = 1
      repeat while tIndex <= tlPickData.count and tPickedObject =  [:]
        tPickedObject = checkHit(tlPickData[tIndex])
        tIndex = tIndex + 1
      end repeat
    end if
  end if

  return tPickedObject
end

on checkHit alPickedData
  toHitLayer = plModelDescriptions[alPickedData[#model]][#object].poImageElement.plImageLayers["hitarea"]
  if not voidP(toHitLayer) then
    tHit = false

    -- account for multiple submeshes in model
    toPickedObject = plModelDescriptions[alPickedData[#model]][#object]

    tModel = alPickedData.model
    tMeshID = alPickedData.meshID

    alPickedData[#faceID] = alPickedData[#faceID] + toPickedObject.plMeshFaceCounts[tMeshID]

    tPickedImage = toHitLayer[#object].pImage

    if not voidP(tPickedImage) then
      tShadersH = toPickedObject.plShaders[1].count
      tH = ((tMeshID-1) mod (tShadersH)) + 1
      tV = integer((tMeshID-1) / float(tShadersH) -.499) + 1

      tPickedShader = toPickedObject.plShaders[tV][tH].shader
      tUV = mapPointToTexture(alPickedData)

      -- if model has has a visible layer, find exact pick point in texture chunk
      tlImageLayers = toPickedObject.poImageElement.plImageLayers
      if not voidP(max(tlImageLayers["base"], tlImageLayers["hover"], tlImageLayers["press"])) then
        tSeamFix = toPickedObject.pSeamFix
        repeat with tlShaderV in toPickedObject.plShaders
          repeat with tShader in tlShaderV
            if tShader.shader.name = tPickedShader.name then
              tShaderSize = tShader.size
              tImageSubRect = tShader.rect
              exit repeat
            end if
          end repeat
        end repeat
      else
        tImageSubRect = tPickedImage.rect
        tShaderSize = point(tImageSubRect.width, tImageSubRect.height)
      end if

      tPointInRect = point ( \
        (tUV.locH * tShaderSize.locH) + tImageSubRect.left, \
        (tUV.locV * tShaderSize.locV) + tImageSubRect.top )

      tSpotValue = colorToValue(tPickedImage.getPixel(tPointInRect))
      if tSpotValue >=.5 then tHit = true
    end if

    if tHit = true then
      return plModelDescriptions[alPickedData[#model]]
    else return [:]
  else return plModelDescriptions[alPickedData[#model]]
end


on showCursor me
  if pMouseLoc = point(0,0) then exit  -- prevent mouse jitter for post Eolas Shockwave

  if pRMBDown = true then tCursor="zoom"
  else
    if not voidP(pMouseHoverObject) then
      if pMouseHoverObject.pHasClickAction then
        tCursor = "click"
      end if
    end if
    if voidP(tCursor) then
      if (pMouseLoc).inside(pSceneCamera.rect) then
        tCursor = "pan"
      else tCursor = 0
    end if
  end if

  if not voidP(tCursor) and not pDisableCursor then
    if stringP(tCursor) then tCursor = [member(tCursor&"Cursor"), member(tCursor&"CursorMask")]
    pSprite.cursor = tCursor
  end if
end


on addActiveModel me, aObject
  -- TODO: optimise sorting
  the itemDelimiter = "#"

  tGroup= aObject.pModel.parent

  tZGroup = tGroup.name.item[2]
  case tZGroup of
    "_spv.management":
      tZGroup = "a"
    "_spv.branding":
      tZGroup = "b"
  end case

  tZorder = tZGroup & "." & getChildNr(tGroup, aObject.pModel)
  tParent = aObject.poParent

  repeat while tParent <> #root
    tGroup = tParent.pParentGroup
    tZGroup = tGroup.name.item[2]
    case tZGroup of
      "_spv.management":
        tZGroup = "a"
      "_spv.branding":
        tZGroup = "b"
    end case
    tParentGroup = tParent.pGroup
    tZorder = tZGroup & "." & getChildNr(tGroup, tParentGroup) & "|" & tZorder
    tParent = tParent.poParent
  end repeat

  tlObjectDescription = [:]
  tlObjectDescription[#object] = aObject
  tlObjectDescription[#zOrder] = tZorder
  tlObjectDescription[#root] = tGroup.name.item[1]
  plModelDescriptions[aObject.pModel] = tlObjectDescription

  -- sort list of active models on zOrder
  plModelDescriptions = sortProps(plModelDescriptions, #zOrder)

  -- build two lists containing only model references
  plActiveSceneModels = []
  plActiveOverlayModels = []

  repeat with tlActiveModel in plModelDescriptions
    tModel = tlActiveModel[#object].pModel
    if tlActiveModel[#root] = "_zScene" then plActiveSceneModels.addAt(1,tModel)
    else if tlActiveModel[#root] = "_zOverlay" then plActiveOverlayModels.addAt(1,tModel)
  end repeat
end

on removeActiveModel me, aObject
  if not voidP(aObject.pModel) then
    plModelDescriptions.deleteProp(aObject.pModel)
    plActiveSceneModels.deleteOne(aObject.pModel)
    plActiveOverlayModels.deleteOne(aObject.pModel)
  end if

  if pMouseHoverObject = aObject then
    pMouseHoverObject = VOID
  end if
  if pMouseDownObject = aObject then pMouseDownObject = VOID

  me.showCursor()
end

on getChildNr aGroup, aChild
  tFoundChild = 0

  repeat with tChildNr = 1 to aGroup.child.count
    if aGroup.child[tChildNr] = aChild then
      tFoundChild = tChildNr
      exit repeat
    end if
  end repeat

  return tFoundChild
end
