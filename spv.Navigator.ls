-- Handle camera motion, mouse tracking inclusive

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--
-- Loosely based on Sky Box Behaviour by Barry Swan
-- URL:    http://www.inludo.com/tuts/tutorials.htm


property parent

property pSprite, pWorld, pCamera

-- User defined properties
property pLimitPanoElement
property pPan, pPanMin, pPanMax
property pTilt, pTiltMin, pTiltMax
property pFOV, pFOVMin, pFOVMax

property pRotatePace, pZoomPace
property pRotateInertia, pZoomInertia
property pRollWhenPanning, pZoomWhenPanning

property pDisableNavigation, pDisableKeyNavigation

-- Runtime properties
property pPanSpeed, pTiltSpeed, pZoomSpeed

property pHFOV, pHFOVMax

property pRoll

property pKeyNav
property pDragLoc, pZoomDragLoc

property pForcedRotate, pForcedZoom

property pPanCursorMember, pClickCursorMember

on new me, _parent
  parent = _parent
  
  me.initNavigation()
  
  pSprite.scriptInstanceList.add(me)
  return me
end


on mouseDownEvent me
  if pDisableNavigation then exit
  
  -- Start rotating and set start position of mouse
  tMouseLoc = parent.poMouseManager.pMouseLoc
  
  if tMouseLoc.inside(parent.pSceneCamera.rect) then
    pDragLoc = tMouseLoc
  end if
  
  pForcedRotate = false
  pForcedZoom = false
end

on enterFrame me
  if parent.pError then exit
  
  tMouseLoc = parent.poMouseManager.pMouseLoc
  
  if the rightMouseDown and rollOver(parent.pSprite) and parent.pActive and not pDisableNavigation then
    if voidP(pZoomDragLoc) then pZoomDragLoc = tMouseLoc.locV
    --
  else if not the rightMouseDown then
    pZoomDragLoc = void
    --pForcedZoom = false
  end if
  
  if the mouseDown = false then pDragLoc = void
  
  if not pDisableKeyNavigation then
    pKeyNav = [0,0,0]
    if parent.pActive and rollOver(parent.pSprite) and not pDisableNavigation then
      if keyPressed(123) then pKeyNav[1] = pKeyNav[1] - 1
      if keyPressed(124) then pKeyNav[1] = pKeyNav[1] + 1
      
      if keyPressed(125) then pKeyNav[2] = pKeyNav[2] - 1
      if keyPressed(126) then pKeyNav[2] = pKeyNav[2] + 1
      
      if the shiftdown then   pKeyNav[3] = pKeyNav[3] - 1
      if the controlDown then pKeyNav[3] = pKeyNav[3] + 1
    end if
  end if
  
  repeat with tKeyNav in pKeyNav
    if tKeyNav <> 0 then
      pForcedRotate = false
      pForcedZoom = false
    end if
  end repeat
  
  me.updateView()
end

on destroy me
  pSprite.scriptInstanceList.deleteOne(me)
end

--------------------
-- Private methods
--------------------

on initNavigation me
  pSprite = parent.pSprite
  pWorld = pSprite.member
  pCamera = parent.pSceneCamera
  
  me.setCameraValues(true)
  
  pPan = min(pPanMax, max(pPanMin, 0))
  pTilt = min(pTiltMax, max(pTiltMin, 0))
  pFOV = min(pFOVMax, max(pFOVMin, 90))
  pRoll = 0
  
  pCamera.fieldofview = pFOV
  pCamera.transform.rotation = vector(pTilt, -pPan, 0.0)
  
  pKeyNav = [0,0,0]
end

on setCameraValues me, aDontUpdateView, aoLimitPanoElement
  if voidP(aDontUpdateView) then aDontUpdateView = false
  
  tGlobalLimits = VOID
  tGlobalDynamics = VOID
  tSceneLimits = VOID
  tSceneDynamics = VOID
  
  if not voidP(parent.poTour) then
    if not voidP(parent.poTour.poGlobalNode) then
      toMeta = parent.poTour.poGlobalNode["meta"]
      tGlobalLimits = toMeta["cameralimits"]
      tGlobalDynamics = toMeta["cameradynamics"]
    end if
    
    if not voidP(parent.poTour.poScene) then
      if not voidP(parent.poTour.poScene.node) then
        toMeta = parent.poTour.poScene.node["meta"]
        tSceneLimits = toMeta["cameralimits"]
        tSceneDynamics = toMeta["cameradynamics"]
      end if
    end if
    
    pLimitPanoElement = VOID
    if not voidP(tGlobalLimits) then
      pLimitPanoElement = tGlobalLimits.attributeValue["panoelement"]
    end if
    if not voidP(tSceneLimits) then
      tLimitPanoElement = tSceneLimits.attributeValue["panoelement"]
      if not voidP(tLimitPanoElement) then pLimitPanoElement = tLimitPanoElement
    end if
  end if
  
  tLimitsSet = false
  toPanoElement = aoLimitPanoElement
  if not voidP(pLimitPanoElement) and not voidP(parent.poTour) then
    tlPanoElement = parent.poTour.poScene.plPanoElements[pLimitPanoElement]
    if not voidP(tlPanoElement) or not voidP(toPanoElement) then
      if voidP(toPanoElement) then toPanoElement = tlPanoElement[#object]
      tFOVRanges = toPanoElement.pFOVRanges
      if not voidP(tFOVRanges) then
        if toPanoElement.pTilt <> 0 or toPanoElement.pRoll <> 0 then
          parent.poLogger.show("Panoelement '"& pLimitPanoElement &"' too complex to set cameralimits.", #warning)
        end if
        
        pPanMin = tFOVRanges[1][1] + toPanoElement.pPan
        pPanMax = tFOVRanges[1][2] + toPanoElement.pPan
        pTiltMin = tFOVRanges[2][1]
        pTiltMax = tFOVRanges[2][2]
        
        pHFOVMax = min(170,abs(tFOVRanges[1][2] - tFOVRanges[1][1]))
        pFOVMax = min(170,abs(tFOVRanges[2][2] - tFOVRanges[2][1]))
        
        pFOVMax  = min(pFOVMax, me.getCamValue("fovmax",  0.01, 170,  120, tGlobalLimits, tSceneLimits))
        
        pFOVMin  = me.getCamValue("fovmin",  0.01, 170, min(pHFOVMax, pFOVMax)/3, tGlobalLimits, tSceneLimits)
        if pFOVMin > pFOVMax then pFOVMin = pFOVMax
        
        tLimitsSet = true
        
      end if
    else
      pPanMin =-180
      pPanMax =180
      pTiltMin = -90
      pTiltMax = 90
      pFOVMin = 0.01
      pFOVMax = 170
      pHFOVMax = VOID
      tLimitsSet = true
    end if
  end if
  
  if not tLimitsSet then
    -- get limits normally
    pPanMin  = me.getCamValue("panmin", -360, 360, -180, tGlobalLimits, tSceneLimits)
    pPanMax  = me.getCamValue("panmax", -360, 360,  180, tGlobalLimits, tSceneLimits)
    pPanMin = pPanMax - (min(360, pPanMax - pPanMin))
    
    
    pTiltMin = me.getCamValue("tiltmin", -90,  90,  -90, tGlobalLimits, tSceneLimits)
    pTiltMax = me.getCamValue("tiltmax", -90,  90,   90, tGlobalLimits, tSceneLimits)
    if pTiltMin > pTiltMax then
      tTiltMin = pTiltMin
      pTiltMin = pTiltMax
      pTiltMax = tTiltMin
    end if
    
    tFOVMax  = min(pPanMax - pPanMin, pTiltMax - pTiltMin)
    pFOVMax  = me.getCamValue("fovmax",  0.01, 170,  min(130,tFOVMax), tGlobalLimits, tSceneLimits)
    pFOVMin  = me.getCamValue("fovmin",  0.01, 170,  pFOVMax/2.5, tGlobalLimits, tSceneLimits)
    pHFOVMax = VOID
    if pFOVMin > pFOVMax then
      tFOVMin = pFOVMin
      pFOVMin = pFOVMax
      pFOVMax = tFOVMin
    end if
  end if
  
  pRotatePace = me.getCamValue("rotatepace", 0, 10, 0.8, tGlobalDynamics, tSceneDynamics)
  pZoomPace =   me.getCamValue("zoompace",   0, 10, 1.1 , tGlobalDynamics, tSceneDynamics)
  
  pRotateInertia = me.getCamValue("rotateinertia", 0, 1, 0.8, tGlobalDynamics, tSceneDynamics)
  pZoomInertia =   me.getCamValue("zoominertia",  0, 1, 0.5, tGlobalDynamics, tSceneDynamics)
  
  pRollWhenPanning = me.getCamValue("panroll", -1, 1, 0, tGlobalDynamics, tSceneDynamics)
  pZoomWhenPanning = me.getCamValue("panzoom", -1, 1, 0, tGlobalDynamics, tSceneDynamics)
  
  if not aDontUpdateView then me.updateView(true)
end

on getCamValue me, aPropName, aMin, aMax, aDefault, aoGlobalNode, aoSceneNode
  tValue = void
  if not voidP(aoSceneNode) then
    tValue = myfloat(aoSceneNode.attributeValue[aPropName])
  end if
  
  if not floatP(tValue) then
    if not voidP(aoGlobalNode) then
      tValue = myfloat(aoGlobalNode.attributeValue[aPropName])
    end if
  end if
  
  if floatP(tValue) then
    return min(aMax, max(aMin, tValue))
  else return aDefault
end


on updateView me, aSet
  if not(aSet) then
    tZoomInertia = pZoomInertia
    tRotateInertia = pRotateInertia
    
    if parent.pFrameFactor > 1 then
      tZoomInertia = power(tZoomInertia,parent.pFrameFactor)
      tRotateInertia = power(tRotateInertia,parent.pFrameFactor)
    end if
  else
    tZoomInertia = 1
    tRotateInertia = 1
  end if
  
  tCamRect = pCamera.rect
  
  if not(aSet) then
    -- Handle zoom speed
    if not voidP(pZoomDragLoc) then
      pZoomSpeed = 5 * pZoomPace * (parent.poMouseManager.pMouseLoc.locV - pZoomDragLoc) / tCamRect.height
    else if pKeyNav[3] <> 0 then
      pZoomSpeed = pKeyNav[3] * pZoomPace * 0.75
    else if not pForcedZoom then
      pZoomSpeed = pZoomSpeed * tZoomInertia
    end if
    
    tZoomSpeed = pZoomSpeed
    if parent.pFrameFactor > 1 then
      tZoomSpeed = tZoomSpeed * parent.pFrameFactor
    end if
    
    -- Handle pan/tilt speed
    if not voidP(pDragLoc) and voidP(pZoomDragLoc) then
      if pDragLoc = point(0,0) or pDragLoc = point(1,1) then pDragLoc = parent.poMouseManager.pMouseLoc
      tDragged = (parent.poMouseManager.pMouseLoc - pDragLoc)/75.0
      
      pPanSpeed = tDragged[1]
      pTiltSpeed = -tDragged[2]
    else if pForcedRotate = false then
      pPanSpeed = pPanSpeed * tRotateInertia
      pTiltSpeed = pTiltSpeed * tRotateInertia
    end if
    
    if pKeyNav[1] <> 0 then pPanSpeed = pKeyNav[1] * 0.75
    if pKeyNav[2] <> 0 then pTiltSpeed = pKeyNav[2] * 0.75
    
    -- Actual speed depends on fov and view height
    tRotateScale = pRotatePace * (1.0 * pFOV / tCamRect.height) * 5
    
    tPanSpeed = pPanSpeed * tRotateScale
    tTiltSpeed = pTiltSpeed * tRotateScale
    
    if parent.pFrameFactor > 1 then
      tPanSpeed = tPanSpeed * parent.pFrameFactor
      tTiltSpeed = tTiltSpeed * parent.pFrameFactor
    end if
  else
    tPanSpeed = 0
    tTiltSpeed = 0
    tZoomSpeed = 0
  end if
  
  -- Handle zoom
  if not voidP(pHFOVMax) then
    tFOVMaxH = radDeg(2* atan(tan(degRad(pHFOVMax)/2) * tCamRect.height / float(tCamRect.width)))
    tFOVMax = min(tFOVMaxH , pFOVMax)
  else if abs(pPanMax - pPanMin) <= abs(pTiltMax - pTiltMin) then
    tFOVMaxH = radDeg(2* atan(tan(degRad(pFOVMax)/2) * tCamRect.height / float(tCamRect.width)))
    tFOVMax = min(tFOVMaxH , pFOVMax)
  else tFOVMax = pFOVMax
  
  tFOVMin = pFOVMin
  
  if tZoomSpeed <> 0 then
    tFOVfactor = 1+abs(tZoomSpeed)/50
    if tZoomSpeed < 0 then tFOVfactor = 1/tFOVfactor
    pFOV = pFOV * tFOVfactor
  end if
  pFOV = min(max(pFOV, tFOVMin), tFOVMax)
  pHFOV = radDeg(2* atan(tan(degRad(pFOV)/2) * tCamRect.width / float(tCamRect.height)))
  
  tFOV = pFOV+ abs(tPanSpeed) * pZoomWhenPanning
  tFOV = min(max(tFOV, tFOVMin), tFOVMax)
  if not(aSet) then tFOV = pFOV + (1.0*tFOV - pFOV)*tZoomInertia
  
  -- Handle pan/tilt/roll
  tPanMin = myfloat(pPanMin)
  tPanMax = myfloat(pPanMax)
  if pHFOV > abs(tPanMax - tPanMin) then
    pPan = (tPanMax + tPanMin)/2
  else
    if tPanMax - tPanMin < 360 then
      tPanMin = tPanMin + .5 * pHFOV
      tPanMax = tPanMax - .5 * pHFOV
      
      pPan = min(max(pPan + tPanSpeed, tPanMin), tPanMax)
    else
      pPan = pPan + tPanSpeed
      if pPan<-180 then pPan = pPan + 360
      else if pPan>180 then pPan = pPan -360
    end if
  end if
  
  tTiltMin = myfloat(pTiltMin)
  tTiltMax = myfloat(pTiltMax)
  if pFOV > abs(tTiltMax - tTiltMin) then
    pTilt = (tTiltMax + tTiltMin)/2
  else
    if tTiltMin > -90 then tTiltMin = tTiltMin + .5 * pFOV
    if tTiltMax < 90  then tTiltMax = tTiltMax - .5 * pFOV
    
    pTilt = min(max(pTilt + tTiltSpeed, tTiltMin), tTiltMax)
  end if
  
  tRoll = pRoll + pRollWhenPanning * tPanSpeed
  
  -- prevent looking straight through the seams
  tPan = pPan + 0.001
  tTilt = pTilt + 0.001
  
  pCamera.fieldofview = tFOV
  
  pCamera.transform.rotation = vector(tTilt, -tPan, 0)
  pCamera.rotate(0,0,tRoll, #self)
end


--
-- public properties
--

on setProp me, aProperty, aValue
  case aProperty of
    "pan":
      pPan = float((getValue(#float,aValue,pPan) *1000) mod 360000)/1000
      if pPan < -180 then pPan = pPan + 360
      if pPan >  180 then pPan = pPan - 360
    "tilt":
      pTilt = min(max(getValue(#float,aValue,pTilt), -90), 90)
    "fov":
      pFOV = min(max(getValue(#float,aValue,pFOV), pFOVMin), pFOVMax)
    "panmin","panmax":
      me[symbol("p" & aProperty)] = getValue(#float,aValue,me[symbol("p"&aProperty)])
    "tiltmin":
      pTiltMin = min(getValue(#float,aValue,pTiltMin), pTiltMax)
    "tiltmax":
      pTiltMax = max(getValue(#float,aValue,pTiltMax), pTiltMin)
    "fovmin":
      pFOVMin = min(getValue(#float,aValue,pFOVMin), pFOVMax)
    "fovmax":
      pFOVMax = max(getValue(#float,aValue,pFOVMax), pFOVMin)
    "rotatepace","zoompace":
      me[symbol("p" & aProperty)] = max(0,min(10,getValue(#float,aValue,me[symbol("p" & aProperty)])))
    "rotateinertia","zoominertia":
      me[symbol("p" & aProperty)] = max(0,min(1,getValue(#float,aValue,me[symbol("p" & aProperty)])))
    "panspeed","tiltspeed","zoomspeed":
      tValue = myfloat(aValue)
      if floatP(tValue) then
        me[symbol("p" & aProperty)] = tValue
        if aProperty = "zoomspeed" then
          pForcedZoom = bool(abs(tValue))  
        else pForcedRotate = bool(abs(tValue))
      end if
    "disablenavigation","disablekeynavigation":
      me[symbol("p" & aProperty)] = getValue(#boolean, aValue, not me[symbol("p" & aProperty)])
  end case
  
  me.updateView(true)
end