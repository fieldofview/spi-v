-- Preload external files

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property plTriage
property plActiveJobs
property pFinishedJobs

property pMaxConnections
property pActiveConnections

property pProgressVisible
property poProgressBar

on new me,  _parent
  parent = _parent
  
  pMaxConnections = 4
  pActiveConnections = 0
  
  plActiveJobs = [:]
  
  plTriage = [:]
  pFinishedJobs = 0
  
  pProgressVisible = false
  
  parent.pSprite.scriptInstanceList.add(me)
  return me
end

on addNodes me, aoGlobalNode
  -- add download nodes to XML
  
  tlUIElements = aoGlobalNode.getElementByName("uielement")
  repeat with toUIElement in tlUIElements
    if toUIElement.attributeValue["id"]="_progressBar" then
      toProgressBarNode = toUIElement
    end if
  end repeat
  
  if voidP(toProgressBarNode) then
    toProgressBarNode = addNode("uielement", aoGlobalNode, \
      ["id":"_progressBar", "image":"_progressBarImage", "silent":"true"])
    
    toProgressImageNode = addNode("image", aoGlobalNode, \
      ["id":"_progressBarImage","width":"100","height":"8","silent":"true"])
    
    
    tProgressColor = parent.pForeColor
    
    toProgressLayerNode = addNode("layer", toProgressImageNode, \
      ["class":"base", "type":"matte", "color":"_foreground", "alphatype":"matte", "alphacolor":"000000", "dynamic":"true"])
  end if
  
  toProgressBarNode.attributeValue["zorder"] = "_spv.management"
  toProgressBarNode.attributeValue["visible"] = "false"
end

on addJob me, aURL, aRelativeURL, aPriority, aResult, aCallback, aCallbackTarget
  if voidP(aURL) then exit
  if voidP(aCallbackTarget) then aCallbackTarget = parent
  
  tURL = relativeURL(aURL, aRelativeURL)
  
  tlCurrentJob = plActiveJobs[tURL]
  if not voidP(tlCurrentJob) then
    tlCurrentJob.plJob[#callbacks].add([#handler: aCallback, #object: aCallbackTarget])
  else
    tlCurrentJob = plTriage[tURL]
    if not voidP(tlCurrentJob) then
      tlCurrentJob[#callbacks].add([#handler: aCallback, #object: aCallbackTarget])
    else
      plTriage[tURL] = [ \
        #URL:aURL, #priority: -aPriority, #result: aResult, \
        #importURL: tURL, \
        #callbacks: [[#handler: aCallback, #object: aCallbackTarget]]]
    end if
  end if
end

on removeJobs me, aCallbackTarget
  repeat with tJob = plTriage.count down to 1
    repeat with tCallback = plTriage[tJob][#callbacks].count down to 1
      if plTriage[tJob][#callbacks][tCallback][#object] = aCallbackTarget then
        plTriage[tJob].callbacks.deleteAt(tCallback)
      end if
    end repeat
    if voidP(aCallbackTarget) or plTriage[tJob].callbacks.count = 0 then
      plTriage.deleteAt(tJob)
    end if
  end repeat
  repeat with tJob = plActiveJobs.count down to 1
    repeat with tCallback = plActiveJobs[tJob].plJob[#callbacks].count down to 1
      if plActiveJobs[tJob].plJob[#callbacks][tCallback][#object] = aCallbackTarget then
        plActiveJobs[tJob].plJob.callbacks.deleteAt(tCallback)
      end if
    end repeat
    if voidP(aCallbackTarget) or plActiveJobs[tJob].plJob.callbacks.count = 0 then
      plActiveJobs[tJob].destroy()
      plActiveJobs.deleteAt(tJob)
    end if
  end repeat
end

on destroy me
  me.removeJobs()
  parent.pSprite.scriptInstanceList.deleteOne(me)
end

on enterFrame me
  if parent.pError then exit
  
  -- handle jobs in triage one at a time
  plTriage = sortProps(plTriage, #priority)
  if plTriage.count>0 then
    tlJob = plTriage[1]
    plTriage.deleteAt(1)
    
    if tlJob[#url] starts "#" then
      -- cast member: process immediately
      me.processCastmember(tlJob)
    else if not((the moviepath & tlJob[#importURL]) contains "://" or tlJob[#result]=#text) then
      -- local file: process immediately
      me.processFile(tlJob)
    else
      -- remote file, or text file: preload
      plActiveJobs[tlJob[#importURL]] = new(script("spv.Preloader"),me, tlJob)
    end if
  end if
  
  -- handle active/finished jobs
  repeat with toJob in plActiveJobs
    if toJob.pReady = true then
      toProcessJob = toJob
      exit repeat
    end if
  end repeat
  if not voidP(toProcessJob) then
    tlJob = toProcessJob.plJob
    pFinishedJobs = pFinishedJobs + toProcessJob.pBytesTotal
    plActiveJobs.deleteOne(toProcessJob)
    me.processFile(tlJob)
  end if
  
  -- see if progressbar element is available
  if not voidP(parent.poTour) then
    if not voidP(parent.poTour.poScene) then
      if not voidP(parent.poTour.poScene.plUIElements["_progressBar"]) then
        -- update progress display
        
        toProgressBar = parent.poTour.poScene.plUIElements["_progressBar"][#object]
      end if
    end if
  end if
  
  if plActiveJobs.count > 0 then
    tJobsTotal = pFinishedJobs
    tJobsSofar = pFinishedJobs
    
    repeat with toJob in plActiveJobs
      tJobsTotal = tJobsTotal + toJob.pBytesTotal
      tJobsSofar = tJobsSofar + toJob.pBytesSofar
    end repeat
    
    if not voidP(toProgressBar) then
      if tJobsTotal>0 then
        tPercentage = min((100 * tJobsSofar) / tJobsTotal, 100)
        if not voidP(toProgressBar.poImageElement) then
          if not voidP(toProgressBar.poImageElement.plImageLayers) then
            if not voidP(toProgressBar.poImageElement.plImageLayers["base"]) then
              toProgressLayer = toProgressBar.poImageElement.plImageLayers["base"][#object]
              toProgressLayer.pImage.useAlpha = true
              
              tAlpha = image(toProgressLayer.pImage.width, toProgressLayer.pImage.height, 8, #grayScale)
              tAlpha.fill(tAlpha.rect, [shapeType:#rect, #color:rgb("FFFFFF")])
              
              tFillRect = rect(0,0, (tAlpha.width * tPercentage) / 100.0, tAlpha.height)
              tAlpha.fill(tFillRect, [shapeType:#rect, #color:rgb("000000")])
              
              toProgressLayer.pImage.setAlpha(tAlpha)
              toProgressLayer.updateTextures()
              
              if pProgressVisible = false then
                toProgressBar.setProp("visible", "true")
                pProgressVisible = true
              end if
            end if
          end if
        end if
      else
        if pProgressVisible = true then
          toProgressBar.setProp("visible", "false")
          pProgressVisible = false
        end if
      end if
    end if
  else
    pFinishedJobs = 0
    if not voidP(toProgressBar) then
      if pProgressVisible = true then
        toProgressBar.setProp("visible", "false")
        pProgressVisible = false
      end if
    end if
  end if
end

on processFile me, alJob
  tlResult = [#URL:alJob[#URL],#streamError:alJob[#streamError]]
  
  if ["","OK"].getOne(string(alJob[#streamError])) > 0 then
    case alJob[#result] of
      #text:
        tlResult[#result] = alJob[#text]
      #image, #member:
        if fileExt(alJob[#importURL]) = "gif" then tRemap = true
        else tRemap = false
        
        parent.poLogger.show("Importing '" & alJob[#URL] & "'", #info)
        
        if not voidP(parent.pWindow) then
          tImportMember = new(#text)
          importFileInto(tImportMember, alJob[#importURL], [#trimWhiteSpace:0, #remapImageToStage:tRemap])
          
          if getPos([#animgif, #bitmap, #flash], tImportMember.type) > 0 or alJob[#result] = #member then
            if alJob[#result] = #image then
              tlResult[#result] = tImportMember.image.duplicate()
              tImportMember.erase()
            else
              tlResult[#result] = tImportMember
            end if
          end if
        else
          tell the activeWindow
            tImportMember = new(#text)
            importFileInto(tImportMember, alJob[#importURL], [#trimWhiteSpace:0, #remapImageToStage:tRemap])
            
            if getPos([#animgif, #bitmap, #flash], tImportMember.type) > 0 then
              if alJob[#result] = #image then
                tlResult[#result] = tImportMember.image.duplicate()
                tImportMember.erase()
              else
                -- todo: copy over member
                tlResult[#result] = tImportMember
              end if
            end if
          end tell
        end if
        parent.pDontCountMS = true
    end case
    
    if alJob[#result]<>#preload and voidP(tlResult[#result]) then
      parent.poLogger.show("Could not import '" & alJob[#URL] & "'", #warning)
    end if
  else
    parent.poLogger.show("Could not load '" & alJob[#URL] & "':" && netErrorText(alJob[#streamError]), #warning)
  end if
  
  repeat with tlCallback in alJob[#callbacks]
    call(tlCallback.handler, tlCallback.object, tlResult)
  end repeat
end

on processCastmember me, alJob
  tlResult = [#URL:alJob[#URL]]
  
  tMemberName = chars(alJob[#URL], 2, alJob[#URL].length)
  tMember = member(tMemberName)
  if voidP(tMember) and voidP(parent.pWindow) then
    tell the activeWindow
      tMember = member(tMemberName)
      if not voidP(tMember) then tLDMmember = true
    end tell
  end if
  
  if not voidP(tMember) then
    case alJob[#result] of
      #text:
        if tLDMmember then
          tell the activeWindow
            if tMember.type = #text then tlResult[#result] = tMember.text
          end tell
        else if tMember.type = #text then tlResult[#result] = tMember.text
      #member:
        tlResult[#result] = tMember
      #image:
        if tLDMmember then
          tell the activeWindow
            if getPos([#animgif, #bitmap, #flash], tMember.type) > 0 then tlResult[#result] = tMember.image.duplicate()
          end tell
        else if getPos([#animgif, #bitmap, #flash], tMember.type) > 0 then tlResult[#result] = tMember.image.duplicate()
    end case
    
    if alJob[#result]<>#preload and voidP(tlResult[#result]) then
      parent.poLogger.show("Could not import '" & alJob[#URL] & "'", #warning)
    end if
  else
    parent.poLogger.show("Could not find member '" & tMembername & "'", #warning)
  end if
  
  repeat with tlCallback in alJob[#callbacks]
    call(tlCallback.handler, tlCallback.object, tlResult)
  end repeat
end

