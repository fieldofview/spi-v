-- Retreive navigator.userAgent from the browser and forget itself

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pNetID
property pAgent

on new me, _parent
  parent = _parent
  
  repeat with tParam = 1 to the externalParamCount
    me[symbol("p" & externalParamName(tParam))] = externalParamValue(tParam) 
  end repeat
  
  pNetID=getNetText("javascript:navigator.userAgent")
  
  parent.pSprite.scriptInstanceList.add(me)
  return me
end

on destroy me
  parent.pSprite.scriptInstanceList.deleteOne(me)
end

on enterFrame me
  if parent.pError then exit
  
  if netDone(pNetID) then
    pAgent = string(netTextResult(pNetID))
    
    if the platform contains "Macintosh" and the runmode = "Plugin" then
      case true of:
        (pAgent contains "KHTML"):
          -- don't fix webkit build >= 420
          tUAoffset = offset("AppleWebkit", pAgent)
          if tUAOffset>0 then
            tWebkit = chars(pAgent, tUAoffset, pAgent.length)
            tWebkit = tWebkit.word[1]
            the itemdelimiter = "/"
            if tWebkit.item.count > 1 then
              tWebkit = tWebkit.item[2]
              the itemDelimiter = "."
              if tWebkit.item.count > 1 then
                tWebkit  = myfloat(tWebkit.item[1] & "." & integer(10000 + tWebkit.item[2]))
              else tWebkit = integer(tWebkit.item[1])
              
              if tWebkit < 416.10011 then parent.pBrowserOffset = 21
            end if
          end if
        (pAgent contains "Firefox/3"):
          -- don't fix shockwave 11.5 build > 602
          if parent.pSWV<11.5 or (parent.pSWV=11.5 and (the environment).productBuildVersion <= 602) then
            parent.pBrowserOffset = 21
          end if
        (pAgent contains "Camino"):
          parent.pBrowserOffset = 78
      end case
      
      parent.updateRect()
    end if
    
    parent.pSprite.scriptInstanceList.deleteOne(me)
  end if
end