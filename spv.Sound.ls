-- <sound> node
-- Downloads resources, plays sounds

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pID

property pBuilt
property plBehaviorObjects

property pRelativeURL

property pSrc
property pLoop, pActive, pStreaming

property pPan, pTilt

property pVolume, pRadius, pAmbient

property pDuration, pPosition

property poSoundObject
property pState

property pLoopsLeft

on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []

    if not voidP(poSoundObject) then
      poSoundObject.stop()
      poSoundObject = VOID
    end if

    spv().scriptInstanceList.deleteOne(me)
  end if
end

on enterFrame me
  if not spv().pError then
    case pState of
      #loading:
        me.downloadStatus()
      #playing:
        me.updateSound()
    end case
  end if
end

on initNode me
  if voidP(pBuilt) then
    pBuilt = true

    pID = node.attributeValue["id"]

    me.getBehaviors()

    poSoundObject = spv().pSWFSprite.newSound()

    pRelativeURL = node.attributeValue["relativeurl"]
    if [void, "","@","#"].getPos(pRelativeURL) > 0 then pRelativeURL = spv().pSourceURL

    pSrc = node.attributeValue["src"]

    pStreaming = getValue(#boolean, node.attributeValue["streaming"], false)
    pLoop = node.attributeValue["loop"]
    if pLoop <> "true" then pLoop = max(0,getValue(#integer, pLoop, 1))
    pActive = getValue(#boolean, node.attributeValue["active"], false)

    pVolume = max(0,min(100, getValue(#float, node.attributeValue["volume"], 100)))

    pPan = getValue(#float, node.attributeValue["pan"], void)
    if not voidP(pPan) then pPan = pPan mod 360
    pTilt = getValue(#float, node.attributeValue["tilt"], void)
    if not voidP(pTilt) then pTilt = max(-90,min(90, pTilt))

    pRadius = max(0.01,min(180,getValue(#float, node.attributeValue["radius"], 90)))
    pAmbient = max(0,min(100,getValue(#float, node.attributeValue["ambient"], 0)))

    me.loadSound()
    if pActive then me.playSound()
  end if
end

on loadSound me
  if not voidP(pSrc) then
    poSoundObject.loadSound(relativeURL(pSrc, pRelativeURL), pStreaming)
    pPosition = 0
    if not pStreaming then
      pState = #loading
      spv().scriptInstanceList.add(me)
    else
      pState = #ready
      if pActive then
        me.playSound()
      else poSoundObject.stop()
    end if
  end if
end

on playSound me
  if pState<>#loading then
    poSoundObject.start(pPosition,0)

    if voidP(pLoopsLeft) then pLoopsLeft = integer(pLoop)

    if pState<> #playing then
      --me.updateSound()
      spv().scriptInstanceList.deleteOne(me)
      spv().scriptInstanceList.add(me)
      spv().poTour.queueAction(me, "start")
      pState = #playing
    end if
  end if
end

on stopSound me
  if pState<>#loading then
    spv().poTour.queueAction(me, "stop")
    pActive = false
    pState = #stopped
    poSoundObject.stop()
    pPosition = 0
    spv().scriptInstanceList.deleteOne(me)
    pLoopsLeft = void
  end if
end

on downloadStatus me
  tBytesTotal = poSoundObject.getBytesTotal()
  tBytesLoaded = poSoundObject.getBytesLoaded()
  if tBytesTotal >0 and tBytesLoaded = tBytesTotal then
    spv().poTour.queueAction(me, "ready")
    spv().scriptInstanceList.deleteOne(me)
    pState = #ready
    if pActive then me.playSound()
  end if
end

on updateSound me
  pDuration = poSoundObject.duration / 1000
  tPosition = poSoundObject.position / 1000
  if tPosition <> pPosition then
    pPosition = tPosition
    spv().poTour.queueAction(me, "change")
  end if

  if pStreaming and pActive and pPosition = 0 and pDuration>1 then poSoundObject.start()

  if pPosition >= pDuration then
    if pLoopsLeft>1 or [0,"true"].getOne(pLoop) > 0 then
      spv().poTour.queueAction(me, "loop")
      pLoopsLeft = pLoopsLeft -1
      pPosition = 0
      poSoundObject.stop()

      me.playSound()
    else if not(pStreaming) or pDuration>0 then
      poSoundObject.start()
      poSoundObject.stop()

      me.stopSound()
    end if
  else
    tlTransform = [:]
    tDistance = 0

    if not voidP(pPan) then
      tCameraPan = degRad(spv().poNavigator.pPan)
      tCameraTilt = degRad(spv().poNavigator.pTilt)

      tPan = degRad(pPan)
      if voidP(pTilt) then
        tTilt = tCameraTilt
      else tTilt = degRad(pTilt)

      tCameraVector = vector(cos(tCameraTilt)*cos(tCameraPan),cos(tCameraTilt)*sin(tCameraPan),sin(tCameraTilt))
      tSoundVector = vector(cos(tTilt)*cos(tPan),cos(tTilt)*sin(tPan),sin(tTilt))

      tDistance = radDeg(acos(tCameraVector.dot(tSoundVector)))

      tPanAngle = (pPan - spv().poNavigator.pPan) mod 360
      if tPanAngle < 0 then tPanAngle = tPanAngle + 360
      if tPanAngle > 180 then tPanAngle = tPanAngle - 360

      tPanAngle = degRad(tPanAngle)

      if tPanAngle < 0 then
        tlTransform[#ll] = 1
        tlTransform[#rl] = 0
        tlTransform[#rr] = sqrt(1-power(sin(tPanAngle),2))
        tlTransform[#lr] = 1 - tlTransform[#rr]
      else
        tlTransform[#ll] = sqrt(1-power(sin(-tPanAngle),2))
        tlTransform[#rl] = 1 - tlTransform[#ll]
        tlTransform[#rr] = 1
        tlTransform[#lr] = 0
      end if
      if abs(tPanAngle)>PI/2 then
        tRR = tlTransform[#lr]
        tRL = tlTransform[#ll]
        tlTransform[#lr] = tlTransform[#rr]
        tlTransform[#ll] = tlTransform[#rl]
        tlTransform[#lr] = tRR
        tlTransform[#ll] = tRL
      end if
    else
      if not voidP(pTilt) then
        tDistance = abs(myfloat((pTilt - spv().poNavigator.pTilt) mod 180))
      end if

      tlTransform[#ll] = 0.5
      tlTransform[#rl] = 0.5
      tlTransform[#rr] = 0.5
      tlTransform[#lr] = 0.5
    end if

    tRelativeVolume = pVolume * (pAmbient+ (max(0,(pRadius - tDistance))/pRadius)*(100-pAmbient))/100
    tlTransform = tlTransform * tRelativeVolume

    toTransform = poSoundObject.getTransform()
    toTransform.ll = tlTransform.ll
    toTransform.lr = tlTransform.lr
    toTransform.rl = tlTransform.rl
    toTransform.rr = tlTransform.rr

    poSoundObject.setTransform(toTransform)
  end if
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end

--
-- public calls
--


on setProp me, aProperty, aValue
  case aProperty of
    "active":
      tActive = getValue(#boolean, aValue, not pActive)
      if tActive <> pActive then
        pActive = tActive
        if pActive then
          me.playSound()
        else
          me.stopSound()
        end if
      end if
    "loop":
      if aValue = "true" then pLoop = "true"
      else pLoop = max(0,getValue(#integer, pLoop, pLoop))
    "volume":
      pVolume = max(0,min(100, getValue(#float, aValue, pVolume)))
    "radius":
      pVolume = max(0.01,min(180, getValue(#float, aValue, pVolume)))
    "ambient":
      pVolume = max(0,min(100, getValue(#float, aValue, pVolume)))
    "pan":
      if string(aValue) <> "" then
        pPan = getValue(#float, aValue, pPan) mod 360
      else pPan = void
    "tilt":
      if string(aValue) <> "" then
        pTilt = max(-90,min(90, getValue(#float, aValue, pTilt)))
      else pTilt = void
    "position":
      pPosition = max(0,min(pDuration, getValue(#float, aValue, pPosition)))
      if pState = #playing then poSoundObject.start(pPosition)
  end case
end