-- <variable> node
-- Holds a value

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pID
property pValue

property pBuilt
property plBehaviorObjects


on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []
  end if
end

on initNode me
  if voidP(pBuilt) then
    pBuilt = true

    pID = node.attributeValue["id"]

    me.getBehaviors()

    pValue = getValue(#string, node.attributeValue["value"], "")
  end if
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end

--
-- public calls
--


on setProp me, aProperty, aValue
  case aProperty of
    "value":
      tValue = getValue(#string, aValue, "")
      if tValue <> pValue then
        pValue = tValue
        spv().poTour.queueAction(me, "change")
      end if
  end case
end