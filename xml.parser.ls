-- �1999 Andrew White for Cathode
-- andyw@cathode.co.uk
-- Additional API Hussein Boon for www.shocknet.org.uk
-- info@shocknet.org.uk

-- Version 1.0.5AH

-- Change history
-- Version 1.0.5AH
-- 03 APR 2006: Made parseString() check for <?xml> header
-- 24 FEB 2006: Removed unused/unneeded methods to conserve bytes
-- 13 JUL 2004: Added error checking to parseString()
-- 05 NOV 2003: Removed XML downloading to conserve space

-- Version 1.0.5
-- 13 MAR 2002: Removed node cloning methods
--              Added getAttributeByName() method (as getElementByName())
--              Standardised the returns from getElementByName().
--              Returns a propList when used with a wildcard; a list when used
--              with a named element and an empty list when the named element
--              is not present in the document (previously returned VOID)
-- 17 FEB 2002: Added totalNodes method for memory management issues
-- Version 1.0.4
-- 15 JAN 2002: Initial public release of these extensions
-- 28 OCT 2001: Added name element to parser to stop crashing when
--              working out ancestry etc
-- 28 OCT 2001: Added Private method _appendElement
-- 28 OCT 2001: Fixed abnormal behaviour in node cloning method
-- 21 AUG 2001: Added node cloning method.
-- 18 AUG 2001: Stopped getElementByName from crashing if an element name
--              is not found in the _tagList.
--              This method now returns a void if the
--              element name cannot be found
-- 14 AUG 2001: Added method to handle net error codes and to convert them to
--              a more meaningful string.

-- Version 1.0.3
-- 28 SEP 1999: Fixed bug in count due to dot syntax failure of me.list.count
-- 27 SEP 1999: Added getText method
-- 27 SEP 1999: Fixed bug in makeList due to dot syntax failure of me.list.count
-- 12 SEP 1999: Changed parseAttributes to allow more lenient handling of white
--              space in attributes, specifically to allow whitespace between
--              the attribute name, equals sign and attribute value.
--              Strictly this is a WF error but since it causes a infinite loop
--              in the old version it's best to change the code to allow it.

-- Version 1.0.2
-- 06 SEP 1999: Fixed bug with PUBLIC DOCTYPE declarations
-- 05 SEP 1999: Added count property and count method to override the default
--              behavior of the count property which is to return the number
--              of properties in the object

-- Version 1.0.1
-- 04 JUL 1999: Added makeDocument method to support creation of documents
-- 04 JUL 1999: Changed behavior of getElementByName to return element rather
--              than a list if there's only one element with that name
-- 04 JUL 1999: parseElement now sets whether the element is empty or not.
--              This is to support creation of documents from a node tree
-- 04 JUL 1999: decodeEntities now supports hex encoded entities

-- Version 1.0
-- 01 JUL 1999: Original Release

-- Description

-- The xml.parser class implements a Lingo parser which is
-- functionally equivalent to the XML Parser Xtra - minus the bugs :-)

-- To replace the xtra version with the Lingo version just change
-- the new(xtra "XMLParser") to new(script "xml.parser") and everything
-- should work fine. If you find any bugs please send reports to the
-- email address above.

-- This Lingo version implements additional functionality to aid
-- access to the XML node tree. These are:

-- 1. getElementByID
-- This method returns the element with the unique ID passed in
-- Note that if the XML document contains elements with the same
-- ID then only the last element with that ID is returned.

-- 2. getElementByName
-- This method returns the tag (or list of tags) with the tag name
-- passed in. If the tag name "*" is used then all element tags are
-- returned. If your document is all elements then this is an efficient
-- way to access those elements.

-- 3. Bracket access
-- The getAt method is overidden in the xml.node class to provide shortened
-- bracket access e.g., to get a sub node you can do this:

--   parser["document"]["html"]["body"]

-- Without this access method you'd have to find the 'document' node manually
-- from the parser.child property and then find the 'html' from the 'document'
-- node and so on until you reach the depth that you want.

-- To recreate the XML document from the XML node tree use makeDocument().
-- Note that the DTD is not written out as entities are fully parsed and it
-- would be excessive to try and recreate arbitary entities.

-- If you wish to maintain document formatting then parse the document with
-- ignoreWhiteSpace set to false (use the ignoreWhiteSpace(trueOrFalse) method)
-- otherwise the document will be written in a condensed format.

-- Public properties
property child
property count
property name

-- Private properties
property _ignoreWhiteSpace
property _doneParsing

-- Private parser properties
property _whiteSpace
property _entityList
property _docType
property _errorStr
property _idList
property _tagList
property _attList


on new me

  -- Constructor method

  -- Initialise some properties
  me.child = []

  me._idList = [:]
  me._tagList = [:]
  me._attList = [:]


  me._ignoreWhiteSpace = TRUE
  me._doneParsing = TRUE
  me._errorStr = ""

  -- Define the XML whitespace chars
  me._whiteSpace = SPACE & RETURN & TAB & numToChar(10)
  me.name = "Parser"
  return me

end


on parseString me, xmlString
  -- Main parser method - functionally equivalent to the Xtra's
  -- method of the same name, but fixes a key bug with entities
  -- having padded spaces around them

  -- Initialise the storage lists for XML nodes
  me.child = []
  me._idList = [:]
  me._tagList = [:]
  me._attList = [:]

  -- Initialise the entity storage list and add the default entities
  me._entityList = [:]
  me._entityList.addProp("lt","<")
  me._entityList.addProp("gt",">")
  me._entityList.addProp("amp","&")
  me._entityList.addProp("apos","'")
  me._entityList.addProp("quot",QUOTE)

  -- Initialise the node stack variables
  currentNode = me
  me.count = 0
  elementStack = []

  -- AH 13 JUL 04: Initialise error handling
  errorMsg = ""

  -- AH 03 APR 06: Initialise error handling
  if offset("<?xml ",xmlString.line[1])=0 then
    errorMsg = "unexpected start of xml string"
  else

    -- Break up the xml string into tokenized chunks
    xmlTokens = new(script "xml.tokenizer", xmlString, "<>", TRUE)

    -- Scan through the token list and handle the tags appropriately
    repeat while xmlTokens.hasMoreTokens()


      token = xmlTokens.nextToken()

      -- If the token is a '<' this is a start of an element
      -- and sets of a sequence of events.

      if token = "<" then

        token = xmlTokens.nextToken()


        case token.char[1] of

          "?":  -- PROCESSING INSTRUCTION node
            if token.char[2..4] <> "xml" then
              currentNode.child.append(me.parseProcInst(token))
            end if

          "!":  -- Either a DOCTYPE, CDATA or COMMENT section
            case token.char[1..3] of

              "!DO":

                -- If it's a DOCTYPE then parse the tag and
                -- continue parsing tokens until we reach the
                -- end of the DOCTYPE as whitespace is ignored
                -- inside of the internal DTD

                me.parseDocType(token)

                if token contains "[" then
                  repeat while xmlTokens.hasMoreTokens()
                    token = xmlTokens.nextToken()

                    -- The end of the DTD so jump out of the repeat
                    if token contains "]" then
                      exit repeat
                    end if

                    -- Since we are not a validating parser we
                    -- only need to worry about ENTITY definitions
                    case token.char[1..3] of
                      "!EN":
                        me.parseEntity(token)
                    end case

                  end repeat
                end if


              "!--":

                -- With a comment node all we do is scan for the end
                -- of the comment and ignore the contents

                repeat while xmlTokens.hasMoreTokens()

                  newToken = xmlTokens.nextToken()

                  if newToken = ">" then

                    if token.char[(token.length - 1)..(token.length)] = "--" then
                      exit repeat
                    end if
                  end if

                  token = newToken

                end repeat


              "![C":

                -- With CDATA sections just scan for the end and add the
                -- whole chunk to the node tree with any further parsing
                repeat while xmlTokens.hasMoreTokens()

                  newToken = xmlTokens.nextToken()

                  if newToken = ">" then



                    if token.char[(token.length - 1)..(token.length)] = "]]" then
                      textNode = me.parseCDATA(token)
                      if textNode.ilk = #instance then
                        -- Assign the parent of this node
                        textNode.parent = currentNode
                        currentNode.child.append(textNode)
                      end if
                      exit repeat
                    end if

                  end if

                  token = token & newToken

                end repeat

            end case


          "/":
            -- closing element tag so we pop the
            -- parent element off the stack

            -- AH 13 JUL 04: Check if we are closing proper tag
            if elementStack.count then
              if token = "/" & currentNode.name then
                currentNode = elementStack[1]
                elementStack.deleteAt(1)
                currentNode.count = currentNode.count + 1
              else
                errorMsg = "unexpected <" & token & ">, expected <" & currentNode.name & ">"
              end if
            else
              errorMsg = "unexpected <" & token & ">, tag not open"
            end if


          otherwise
            -- It must be just a general element so
            -- we parse it in the standard way

            -- If it's an empty element then just append
            -- it to the current child list otherwise
            -- push the current element onto the stack and
            -- set the new element to be the current element

            if token.char[token.length] = "/" then

              delete token.char[token.length]
              newNode = me.parseElement(token, TRUE)
              if voidP(newNode) then

                exit repeat
              end if

              -- Assign the parent of this node
              newNode.parent = currentNode

              currentNode.child.append(newNode)
              currentNode.count = currentNode.count + 1

            else

              elementStack.addAt(1, currentNode)
              newNode = me.parseElement(token, FALSE)
              if voidP(newNode) then

                exit repeat
              end if

              -- Assign the parent of this node
              newNode.parent = currentNode
              currentNode.child.append(newNode)
              currentNode = newNode
              currentNode.count = 0

            end if

        end case

      else if token = ">" then
        -- do nothing



      else

        -- Text nodes are not allowed outside of the
        -- root document node so check that we are
        -- not at the parser level

        if currentNode <> me then

          -- Call the parseText method to handle
          -- whether the text has whitespace stripped
          -- from it or not

          textNode = me.parseText(token)

          if textNode.ilk = #instance then
            -- Assign the parent of this node
            textNode.parent = currentNode
            currentNode.child.append(textNode)
          end if

        end if

      end if

      -- AH 13 JUL 04: Stop parsing if encountered error
      if errorMsg <> "" then exit repeat

    end repeat

    -- AH 13 JUL 04: Check if any tags were left open
    if elementStack.count > 0 then
      errorMsg = "unexpected end of XML string, open tags:"
      repeat with element in elementStack
        errorMsg = errorMsg && element.child[element.child.count].name

      end repeat
    end if

  end if

  -- AH 13 JUL 04: Return error if any
  if errorMsg <> "" then
    xmlTokens = void
    return errorMsg

  else return void

end


--on ignoreWhiteSpace me, ignore
--
--  -- Mutator method for ignoring whitespace
--  -- Defaults to TRUE
--
--  me._ignoreWhiteSpace = ignore
--
--end
--
--
--on doneParsing me
--
--  -- Accessor method for checking whether a
--  -- parseURL command has completed
--
--  return me._doneParsing
--
--end


--on makeList me
--
--  -- Functionally equivalent to the Xtra's makeList()
--  -- method. It returns a formatted property list of
--  -- the XML documents structure.
--
--  -- I recommend that you use the dot syntax and/or
--  -- the extended API for more efficient access to
--  -- the document's nodes
--
--  docList = [:]
--  childList = ["!ATTRIBUTES": [:]]
--
--  repeat with node in me.child
--
--    case node.type of
--      #element:
--        childList.addProp(node.name, node.toList())
--
--      #procInst:
--        childList.addProp("!PROCINST", node.toList())
--
--      #charData:
--        childList.addProp("!CHARDATA", node.toList())
--    end case
--
--  end repeat
--
--  if count(me.child) then
--    docList.addProp("ROOT OF XML DOCUMENT", childList)
--  end if
--
--  return docList
--
--end



-- Extended api

on getAt me, index

  -- Accessor method to implement [] access for subnodes.
  -- e.g.: parser["document"]["form"]["element"]

  -- If the index value is an integer then get the subnode by
  -- postion, otherwise search for a matching node name.
  -- It only returns the first matching node name

  if index.ilk = #integer then
    if getPos(me.child,index) then
      return me.child[index]
    else
      return VOID
    end if
  else

    index = string(index)
    repeat with node in me.child
      if node.name = index then
        return node
      end if
    end repeat

  end if

end


--on count me
--  -- Return the number of children for this node
--  return count(me.child)
--end


on getElementByID me, ID
  -- Changed by Aldo Hoeben
  -- 21 MAR 2007
  --
  -- less code
  --
  -- Altered by Hussein Boon
  -- http://www.shocknet.org.uk
  -- 02 APR 2002
  -- now returns either an object (ID present) or void (ID not found)
  -- Accessor method for elements with UIDs
  --  if findPos(me._idList,ID) <> VOID then
  return me._idList[ID]
  --  else
  --    return VOID
  --  end if
end


on getElementByName me, tagName

  -- Accessor method for returning element(s)
  -- with a particular tag name. The "*" wildcard
  -- returns all element nodes

  if tagName = "*" then
    return me._tagList
  else

    elemList = me._tagList[tagName]
    -- The next if test has been added to
    -- stop the parser crashing when a tagName is
    -- not present in the _tagList
    if not voidP(elemList) then
      if elemList.count > 1 then
        return elemList
      else
        return [elemList[1]]
      end if
    else
      -- Altered by Hussein Boon
      -- http://www.shocknet.org.uk
      -- 02 APR 2002
      -- now returns an empty list [] instead of void
      -- as user requested an element that's not
      -- present in the current document
      return []
    end if
  end if
end

-- Added by Hussein Boon
-- http://www.shocknet.org.uk
-- 13 MAR 2002


--on getAttributeByName me, attName
--
--  -- Accessor method for returning element(s)
--  -- with a particular attribute name. The "*" wildcard
--  -- returns all element nodes that have attributes
--
--  if attName = "*" then
--    return me._attList
--  else
--
--    attList = me._attList[attName]
--
--    if not voidP(attList) then
--      if attList.count > 1 then
--        return attList
--      else
--        return [attList[1]]
--      end if
--    else
--      -- return an empty list
--      -- as user requested an attribute that's not
--      -- present in the current document
--      return []
--    end if
--  end if
--end


on getText me

  -- Return all of the text under this node
  str = ""
  repeat with node in me.child
    str = str & node.getText()
  end repeat
  return str

end

on makeDocument me

  -- Build an XML document based on the current nodes

  -- Insert the xml declaration
  xmlDoc = "<?xml version=" & QUOTE & "1.0" & QUOTE & "?>" & RETURN

  -- The DTD should be inserted here but since entities are
  -- fully parsed there's no point since the nodes themselves
  -- will only contain basic character entities

  -- Append the document nodes
  repeat with node in me.child
    xmlDoc = xmlDoc & node.toString() & RETURN
  end repeat

  return xmlDoc

end


--on totalNodes me
--  -- This handler is provided for those developers who want to know
--  -- how many nodes are in memory for a particular document
--  -- or document fragment
--  return child[1].totalNodes(1)
--end


-- Private methods


on parseProcInst me, tokenStr

  -- Private method for parsing PI nodes

  delete tokenStr.char[tokenStr.length]
  delete tokenStr.char[1]

  piName = tokenStr.word[1]
  piData = tokenStr.word[2..(tokenStr.word.count)]
  piNode = new(script "xml.node", piName, #procInst, piData)

  return piNode

end


on parseDocType me, tokenStr

  -- Private method for parsing DOCTYPE nodes
  me._docType = tokenStr.word[2]

end



on parseEntity me, tokenStr

  -- Private method for parsing ENTITY declarations
  entityName = tokenStr.word[2]

  -- We don't need to handle paramter entities
  if entityName <> "%" then

    entityValue = tokenStr.word[3..(tokenStr.word.count)]
    delete entityValue.char[entityValue.length]
    delete entityValue.char[1]

    entityValue = me.decodeEntities(entityValue)

    if not me._entityList.findPos(entityName) then
      me._entityList.addProp(entityName, entityValue)
    end if

  end if

end


on parseCDATA me, tokenStr

  -- Private method for parsing CDATA sections

  delete tokenStr.char[(tokenStr.length - 1)..(tokenStr.length)]
  delete tokenStr.char[1..8]

  if tokenStr.length then
    return new(script "xml.node", VOID, #charData, tokenStr)
  else
    return VOID
  end if

end


on parseElement me, tokenStr, isEmpty

  -- Private method for parsing ELEMENT nodes

  elementName = tokenStr.word[1]
  elementNode = new(script "xml.node", elementName, #element, VOID)
  elementNode.isEmpty = isEmpty

  -- If we have attributes then parse them
  if offset("=", tokenStr) then

    attributeStr = tokenStr.word[2..(tokenStr.word.count)]
    attributeList = me.parseAttributes(attributeStr)
    elementNode.attributeValue = attributeList

    numAttributes = attributeList.count

    repeat with i = 1 to numAttributes
      elementNode.attributeName.append(attributeList.getPropAt(i))
      -- Add attribute names and element nodes to list for use with
      -- getAttributeByName()

      if not me._attList.findPos(attributeList.getPropAt(i)) then
        me._attList.addProp(attributeList.getPropAt(i),[elementNode])
      else
        attList = me._attList[getPropAt(attributeList,i)]
        attList.append(elementNode)
      end if

    end repeat

    -- If the element has a UID then add to the list for getElementByID
    if attributeList.findPos("id") then
      me._idList.setaProp(attributeList["id"], elementNode)
    end if

  end if

  -- Add an element to the getElementByName list
  if not me._tagList.findPos(elementName) then
    me._tagList.addProp(elementName,[elementNode])
  else
    tagList = me._tagList[elementName]
    tagList.append(elementNode)
  end if

  return elementNode

end


on parseAttributes me, attrStr

  -- Private method for parsing the attributes of an element

  attrList = [:]
  attrTokens = new(script "xml.tokenizer", attrStr, QUOTE & "'=" & me._whiteSpace, TRUE)

  repeat while attrTokens.hasMoreTokens()

    token = attrTokens.nextToken()

    -- Eat up any whitespace
    repeat while me._whiteSpace contains token
      token = attrTokens.nextToken()
    end repeat

    -- First token is the attribute name
    attrName = token

    -- Eat up the equals token
    repeat while token <> "="
      token = attrTokens.nextToken()
    end repeat

    -- Set the enclosing quote type for this attribute
    quoteType = attrTokens.nextToken()
    repeat while (quoteType <> "'") and (quoteType <> QUOTE)
      quoteType = attrTokens.nextToken()
    end repeat

    -- Build up the attribute value until we hit the end quote
    token = attrTokens.nextToken()
    attrValue = ""

    repeat while token <> quoteType
      attrValue = attrValue & token
      token = attrTokens.nextToken()
    end repeat

    -- Decode any entities in the attribute value
    attrValue = me.decodeEntities(attrValue)

    -- Store the name-value pair
    attrList.addProp(attrName,attrValue)

  end repeat

  return attrList

end


on parseText me, tokenStr

  -- Private method for parsing text sections

  textStr = ""

  if me._ignoreWhiteSpace then

    -- Tokenise the text based on whitespace and
    -- then ignore that white space. Append the
    -- tokens together using a single space

    tokens = new(script "xml.tokenizer", tokenStr, me._whiteSpace)

    if tokens.hasMoreTokens() then
      textStr = me.decodeEntities(tokens.nextToken())
    end if

    repeat while tokens.hasMoreTokens()
      textToken = tokens.nextToken()
      textStr = textStr && me.decodeEntities(textToken)
    end repeat

  else

    textStr = me.decodeEntities(tokenStr)

  end if

  if textStr.length then
    return new(script "xml.node", VOID, #charData, textStr)
  else
    return VOID
  end if

end


on decodeEntities me, str

  -- Private method for decoding entities (&#xxx;)
  -- This needs to be done for text sections and with
  -- element attributes

  if offset("&", str) and offset(";", str) then

    tokens = new(script "xml.tokenizer", str, "&;", TRUE)
    decodedStr = ""

    repeat while tokens.hasMoreTokens()

      token = tokens.nextToken()

      if token = "&" then

        entityName = tokens.nextToken()

        if entityName.char[1] = "#" then

          if entityName.char[2] = "x" then

            -- This is a real hack :-)

            -- Rather than implementing a hexToDec method we bend
            -- the color object to our needs. This will only work
            -- for ASCII & UTF8 encoded XML files

            entityValue = numToChar(rgb(entityName.char[3..4] & "FFFF").red)

          else
            entityValue = numToChar(entityName.value)
          end if
        else
          entityValue = me._entityList.getaProp(entityName)
        end if

        decodedStr = decodedStr & entityValue
        tokens.nextToken()

      else

        decodedStr = decodedStr & token

      end if

    end repeat

    return decodedStr

  else
    return str
  end if
end


