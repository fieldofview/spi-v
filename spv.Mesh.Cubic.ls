-- Cube modelresource (horizontal or vertical)

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pMesh
property pIsMesh

on new me, _parent, aUIElement
  pIsMesh = true
  return __new(me, _parent, void, aUIElement)
end

on destroy me
  -- Destroy model resource
  if not voidP(pMesh) then spv().pWorld.deleteModelResource(pMesh.name)
end

on buildMesh me
  tImageWidth = parent.poImageElement.pWidth
  tImageHeight = parent.poImageElement.pHeight

  if tImageWidth >= tImageHeight then
    tSubType = #horizontal
    case tImageWidth of
      (tImageHeight * 6):
        tCubeFaces = 6
      (tImageHeight * 4):
        tCubeFaces = 4
      otherwise:
        tCubeFaces = 6
        spv().poLogger.show("Unusual image dimensions for cubic panoelement '"& parent.pID &"'", #warning)
    end case

    tCubeFaceWidth = tImageWidth / float(tCubeFaces)
    tCubeFaceHeight = tImageHeight
  else
    tSubType = #vertical
    case tImageHeight of
      (tImageWidth * 6):
        tCubeFaces = 6
      (tImageWidth * 4):
        tCubeFaces = 4
      otherwise:
        tCubeFaces = 6
        spv().poLogger.show("Unusual image dimensions for cubic panoelement '"& parent.pID &"'", #warning)
    end case

    tCubeFaceHeight = tImageHeight / float(tCubeFaces)
    tCubeFaceWidth = tImageWidth
  end if

  tHFOV = degRad(360)
  if tCubeFaces = 4 then tVFOV = degRad(90)
  else tVFOV = degRad(180)

  parent.pFOVRanges = [[radDeg(-tHFOV/2.0), radDeg(tHFOV/2.0) ], \
                       [radDeg(-tVFOV/2.0), radDeg(tVFOV/2.0) ]]


  -- create describing list for each face
  tlCubeFaces = []
  repeat with tCubeFaceNr = 0 to tCubeFaces-1
    tCubeFaceNr = float(tCubeFaceNr)
    tlCubeFace = [:]
    if tSubType = #horizontal then
      tlCubeFace[#rect] = rect(tCubeFaceNr * tCubeFaceWidth, 0, (tCubeFaceNr+1) * tCubeFaceWidth, tCubeFaceHeight )
    else tlCubeFace[#rect] = rect(0, tCubeFaceNr * tCubeFaceHeight, tCubeFaceWidth, (tCubeFaceNr+1.0) * tCubeFaceHeight )
    tlCubeFace[#rect] = tlCubeFace[#rect]
    tlCubeFace[#texPointsH] = [tlCubeFace[#rect].left, tlCubeFace[#rect].right]
    tlCubeFace[#texPointsH].sort()
    tlCubeFace[#texPointsV] = [tlCubeFace[#rect].top, tlCubeFace[#rect].bottom]
    tlCubeFace[#texPointsV].sort()

    -- find first shader in face
    repeat with tShaderV = 1 to parent.plShaders.count
      if tlCubeFace[#rect].top < parent.plShaders[tShaderV][1][#rect].bottom then exit repeat
    end repeat
    repeat with tShaderH = 1 to parent.plShaders[1].count
      if tlCubeFace[#rect].left < parent.plShaders[1][tShaderH][#rect].right then exit repeat
    end repeat
    tlCubeFace[#firstShader] = [min(tShaderV, parent.plShaders.count),min(tShaderH, parent.plShaders[1].count)]

    tlCubeFaces.add(tlCubeFace)
  end repeat

  -- add horizontal texPoints
  repeat with tH = 1 to parent.plShaders[1].count
    tRight = parent.plShaders[1][tH][#rect].right
    repeat with tlCubeFace in tlCubeFaces
      if tRight <= tlCubeFace.rect.right and tRight >= tlCubeFace.rect.left then
        addOnce(tlCubeFace.texPointsH ,tRight)
      end if
    end repeat
  end repeat

  -- add vertical texPoints
  repeat with tV = 1 to parent.plShaders.count
    tBottom = parent.plShaders[tV][1][#rect].bottom
    repeat with tlCubeFace in tlCubeFaces
      if tBottom <= tlCubeFace.rect.bottom and tBottom >= tlCubeFace.rect.top then
        addOnce(tlCubeFace.texPointsV, tBottom)
      end if
    end repeat
  end repeat


  tlFaces = []
  tlVertices = []
  tlTextureCoordinates = []

  repeat with tCubeFaceNr = 1 to tlCubeFaces.count

    -- create 6 faces
    tlCubeFace = tlCubeFaces[tCubeFaceNr]

    tTexRect = tlCubeFace[#rect]
    tlTexPointsH = tlCubeFace[#texPointsH]
    tlTexPointsV = tlCubeFace[#texPointsV]

    tShaderV = tlCubeFace[#firstShader][1]

    repeat with tV = 1 to tlTexPointsV.count
      tVCoord = (tlTexPointsV[tV] - tTexRect.top) / myfloat(tTexRect.bottom-tTexRect.top)

      tY = 1 - 2*tVCoord

      tShaderH = tlCubeFace[#firstShader][2]

      repeat with tH = 1 to tlTexPointsH.count
        tHCoord = (tlTexPointsH[tH] - tTexRect.left) / myfloat(tTexRect.right - tTexRect.left)

        case tCubeFaceNr of
          1: -- front
            tX = -1 + 2*tHCoord
            tZ = -1
          2: -- right
            tX = 1
            tZ = -1 + 2*tHCoord
          3: -- back
            tX = 1 - 2*tHCoord
            tZ = 1
          4: -- left
            tX = -1
            tZ = 1 - 2*tHCoord
          5: -- top
            tX = -1 + 2*tHCoord
            tY = 1
            tZ = 1 - 2*tVCoord
          6: -- bottom
            tX = -1 + 2*tHCoord
            tY = -1
            tZ = -1 + 2*tVCoord
        end case

        -- add vertex
        tlVertices.add(vector(tX, tY, tZ))

        -- add face
        if tH < tlTexPointsH.count and tV < tlTexPointsV.count then
          -- determine shader
          if tlTexPointsV[tV] >= parent.plShaders[tShaderV][tShaderH][#rect].bottom then tShaderV = tShaderV + 1
          if tlTexPointsH[tH] >= parent.plShaders[tShaderV][tShaderH][#rect].right then tShaderH = tShaderH + 1
          tlShader = parent.plShaders[tShaderV][tShaderH]

          -- determine UV coordinates
          tTexU = (tlTexPointsH[tH] - tlShader[#rect].left) / myfloat(tlShader[#size].locH)
          tTexU2 = (tlTexPointsH[tH+1] - tlShader[#rect].left) / myfloat(tlShader[#size].locH)
          tTexV = 1 - ((tlTexPointsV[tV] - tlShader[#rect].top) / myfloat(tlShader[#size].locV))
          tTexV2 = 1 - ((tlTexPointsV[tV+1] - tlShader[#rect].top) / myfloat(tlShader[#size].locV))

          tUV1 = addOnce(tlTextureCoordinates, point(tTexU,  tTexV))
          tUV2 = addOnce(tlTextureCoordinates, point(tTexU2, tTexV))
          tUV3 = addOnce(tlTextureCoordinates, point(tTexU,  tTexV2))
          tUV4 = addOnce(tlTextureCoordinates, point(tTexU2, tTexV2))

          -- create face data
          tFace = [:]
          tFace[#shader] = tlShader[#shader]

          tFace[#vertices] = [tlVertices.count + 1, tlVertices.count, tlVertices.count + tlTexPointsH.count]
          tFace[#textureCoordinates] = [tUV2, tUV1, tUV3]
          tlFaces.add(tFace.duplicate())

          tFace[#vertices] = [tlVertices.count + tlTexPointsH.count + 1, tlVertices.count + 1, tlVertices.count + tlTexPointsH.count]
          tFace[#textureCoordinates] = [tUV4, tUV2, tUV3]
          tlFaces.add(tFace.duplicate())
        end if

      end repeat
    end repeat
  end repeat

  __buildMesh(me, tlFaces, tlVertices, tlTextureCoordinates)
end