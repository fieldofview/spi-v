-- <layer> node
-- Downloads resources, builds imagelayers

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pID
property pBuilt

property pRelativeURL

property plResources

property pImage
property plTextures

property pSeamFix

property pColor, pEndColor
property pAlphaColor, pAlphaEndColor

property pDirection
property pAlphaDirection


on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void
    
    if not voidP(pID) then spv().poTour.removeNamedObject(me)
    
    -- purge pending/active download jobs
    spv().poDownloader.removeJobs(me)
    
    -- remove textures from world - if any
    me.destroyTextures()
    
    plTextures = VOID
    
    pImage = VOID
    plResources = VOID
  end if
end


on initNode me
  if voidP(pBuilt) then
    pBuilt = true
    
    pRelativeURL = node.attributeValue["relativeurl"]
    if [void, "","@","#"].getPos(pRelativeURL) > 0 then pRelativeURL = spv().pSourceURL
    
    pID = node.attributeValue["id"]
    if not voidP(pID) then spv().poTour.addNamedObject(me, pID)
    
    plResources = [:]
    
    if node.attributeValue["type"] = "text" then tIsText = true
    tPriority = getValue(#integer, parent.node.attributeValue["priority"], 0)
    if node.attributeValue["class"] = "preview" then tPriority = tPriority + 10
    
    pColor = getValue(#string, node.attributeValue["color"], "000000")
    if pColor = "_background" then pColor = spv().pWorld.bgColor.hexString()
    else if pColor = "_foreground" then pColor = spv().pForeColor
    pEndColor = getValue(#string, node.attributeValue["endcolor"], "000000")
    if pEndColor = "_background" then pEndColor = spv().pWorld.bgColor.hexString()
    else if pEndColor = "_foreground" then pEndColor = spv().pForeColor
    
    pAlphaColor = getValue(#string, node.attributeValue["alphacolor"], "FFFFFF")
    pAlphaEndColor = getValue(#string, node.attributeValue["alphaendcolor"], "FFFFFF")
    
    pDirection = node.attributeValue["direction"]
    if pDirection <> "radial" then pDirection = -getValue(#float, pDirection, 0) +90 mod 360
    pAlphaDirection = node.attributeValue["alphadirection"]
    if pAlphaDirection <> "radial" then pAlphaDirection = -getValue(#float, pAlphaDirection, 0) +90 mod 360
    
    me.initResource("src", tPriority)
    me.initResource("alphasrc", tPriority)
  end if
end

on initResource me, aChannel, aPriority
  tSrc = node.attributeValue[aChannel]
  tChannel = symbol(aChannel)
  
  tType = "type"
  if aChannel = "alphasrc" then tType = "alphatype"
  if node.attributeValue[tType] = "text" then
    tResult = #text
  else tResult = #image
  
  if not voidP(tSrc) then
    plResources[tChannel] = [#url:tSrc, #resource: VOID, #status: #init]
    spv().poDownloader.addJob(tSrc, pRelativeURL, aPriority, tResult, #getResource , me)
  end if
end

on getResource me, alResult
  if not voidP(plResources[#alphasrc]) then
    if plResources[#alphasrc][#url] = alResult[#URL] then tlResource = plResources[#alphasrc]
  end if
  if not voidP(plResources[#src]) then
    if plResources[#src][#url] = alResult[#URL] then tlResource = plResources[#src]
  end if
  
  if not voidP(alResult[#result]) then
    tlResource[#resource] = alResult[#result]
    tlResource[#status] = #ready
  else
    tlResource[#status] = #error
  end if
  
  me.prepareLayer()
end

on prepareLayer me
  tAllReady = true
  
  repeat with tlResource in plResources
    if voidP(tlResource[#resource]) and tlResource[#status] <> #error then tAllReady = false
  end repeat
  
  if tAllReady then parent.layerReady(node.attributeValue["class"])
end

on makeLayer me, aUpdate
  if aUpdate = true then
    -- trying to update something that's not updateable
    if node.attributeValue["dynamic"] <> "true" then exit
    
    tImageRect = pImage.rect
  end if
  
  if node.attributeValue["class"] = "hitarea" then tImageDepth = 8
  else tImageDepth = 32
  
  pImage = image(parent.pWidth, parent.pHeight, tImageDepth)
  
  tImageOk = false
  
  case node.attributeValue["type"] of
    "bitmap":
      -- use imported image (resampled to correct size)
      if not voidP(plResources[#src]) then
        if ilk(plResources[#src][#resource]) = #image then
          if pImage.depth > 8 then pImage.useAlpha = plResources[#src][#resource].useAlpha
          pImage.copyPixels(plResources[#src][#resource], pImage.rect, plResources[#src][#resource].rect)
          
          tImageOk = true
        end if
      end if
    "text":
      -- create image from html text
      tImage = me.textImage(#src)
      
      if parent.pAutoHeight then
        pImage = image(parent.pWidth, tImage.height,tImageDepth)
        parent.pHeight = tImage.height
        --parent.pAutoHeight = false
      end if
      
      pImage.useAlpha = true
      pImage.setAlpha(0)
      
      me.textCopyImage(tImage, pImage)
      tImageOk = true
    "matte":
      pImage.fill(pImage.rect, [#shapetype:#rect, #color:rgb(pColor)])
      pImage.useAlpha = false
      tImageOk = true
    "ellipse":
      pImage.fill(pImage.rect, [#shapetype:#rect, #color:rgb(pColor)])
      tAlpha = image(pImage.width, pImage.height, 8, #grayscale)
      tAlpha.fill(tAlpha.rect, [#shapetype:#rect, #color:color(0)])
      tAlpha.fill(tAlpha.rect, [#shapetype:#oval, #color:color(255)])
      
      pImage.useAlpha = true
      pImage.setAlpha(tAlpha)
      tImageOk = true
    "gradient":
      pImage = me.gradientImage(pColor, pEndColor, pDirection)
      tImageOk = true
  end case
  
  if tImageOk = false then
    -- Draw broken image if image is not ok
    pImage.fill(pImage.rect, rgb("#FFFFFF"), [#shapetype:#rect])
    pImage.draw(pImage.rect, rgb("#FF0000"), [#shapetype:#rect, #strokeWidth:2])
    pImage.draw(0, 0, pImage.width, pImage.height, rgb("#FF0000"), [#shapetype:#line, #strokeWidth:2])
    pImage.draw(0, pImage.height, pImage.width, 0, rgb("#FF0000"), [#shapetype:#line, #strokeWidth:2])
  end if
  
  -- add/remove alpha-channel if necessary
  tAlphaImage = image(pImage.width, pImage.height, 8, #grayScale)
  case node.attributeValue["alphatype"] of
    "none":
      -- remove alpha
      pImage.setAlpha(0)
      pImage.useAlpha = false
    "color":
      -- TODO:
      -- extract a single #RRGGBB color and use it as a mask
    "bitmap":
      -- use imported image (resampled to correct size)
      if not voidP(plResources[#alphasrc]) and ilk(plResources[#alphasrc][#resource]) = #image then
        tAlphaImage.copyPixels(plResources[#alphasrc][#resource], tAlphaImage.rect, plResources[#alphasrc][#resource].rect, [dither:true])
        tAlphaOk = true
      end if
    "text":
      -- create image from html text
      me.textCopyImage(me.textImage(#alphaSrc), tAlphaImage)
      tAlphaOk = true
    "matte":
      tAlphaImage.fill(tAlphaImage.rect, [#shapetype:#rect, #color:rgb(pAlphaColor)])
      tAlphaOk = true
    "ellipse":
      tAlphaImage.fill(tAlphaImage.rect, [#shapetype:#rect, #color:color(0)])
      tAlphaImage.fill(tAlphaImage.rect, [#shapetype:#oval, #color:rgb(pAlphaColor)])
      tAlphaOk = true
    "gradient":
      tAlphaImage.copyPixels(me.gradientImage(pAlphaColor, pAlphaEndColor, pAlphaDirection), tAlphaImage.rect, tAlphaImage.rect)
      tAlphaOk = true
  end case
  
  if tAlphaOk then
    pImage.useAlpha = true
    pImage.setAlpha(tAlphaImage)
  end if
  
  if voidP(aUpdate) then me.makeTextures()
  else
    if pImage.rect = tImageRect then
      me.updateTextures()
    else
      me.destroyTextures()
      me.makeTextures()
      repeat with toElement in parent.plInterestedElements
        toElement.destroy(true)
        toElement.imageReady()
        toElement.pBuilt = true
      end repeat
    end if
  end if
end

on textImage me, aResource
  tTextMember = new(#text)
  tTextMember.width = parent.pWidth
  
  if voidP(plResources[aResource]) then plResources[aResource] = [#resource: me.makeHTML(node.getText())]  
  if stringP(plResources[aResource][#resource]) then tTextMember.html = plResources[aResource][#resource]
  
  if node.attributeValue["antialias"] = "true" then
    tTextMember.antiAlias = true
    tTextMember.antiAliasThreshold = 4
    if spv().pSWV >= 11.5 then
      tTextMember.hinting = #none
    end if
  end if
  
  tImage = tTextMember.image.duplicate()
  tTextMember.erase()
  
  return tImage
end textImage

on textCopyImage me, aTextImage, aDestImage
  case node.attributeValue["valign"] of
    VOID, "", "top":
      aDestImage.copyPixels(aTextImage, aDestImage.rect, aDestImage.rect)
    "bottom":
      if aTextImage.height <= aDestImage.height then
        aDestImage.copyPixels(aTextImage, aTextImage.rect, aTextImage.rect)
      else
        aDestImage.copyPixels(aTextImage, aDestImage.rect, aDestImage.rect.offset(0,aTextImage.height - aDestImage.height))
      end if
  end case
end textCopyImage

on makeHTML me, aHTML
  return "<html><body><font face='Arial Unicode MS, Arial, Helvetica, Sans-serif'>" & aHTML & "</font></body></html>" 
end 

on gradientImage me, aColor, aEndColor, aDirection
  tShape = new(#vectorshape)
  repeat with tVertex in [point(0,0), point(1,0), point(1,1), point(0,1)]
    tShape.addVertex(1,tVertex * point(pImage.width, pImage.height))
  end repeat
  tShape.curve[1].closed = true
  tShape.strokeWidth = 0
  tShape.fillMode = #gradient
  tShape.fillColor = rgb(aColor)
  tShape.endColor = rgb(aEndColor)
  if pDirection = "radial" then
    tShape.gradientType = #radial
    tShape.fillScale = 50
  else tShape.fillDirection = aDirection
  
  tImage = tShape.image.duplicate()
  tShape.erase()
  
  return tImage
end gradientImage


on makeTextures me
  pSeamFix = 4
  
  -- create textures if layer is of appropriate class
  tClass = node.attributeValue["class"]
  tTextureClasses = ["base", "hover", "press", "disabled", "preview"]
  
  if tTextureClasses.getOne(tClass)>0 then
    tBaseName = "_tex_" & parent.pID & "_" & tClass
    
    tlCleanCutsH = []
    tlCleanCutsV = []
    if pImage.width = 6 * pImage.height then
      tlCleanCutsH = [4, 5] * pImage.height
    else if pImage.height = 6 * pImage.width then
      tlCleanCutsV = [1,2,3,4,5] * pImage.width
    end if
    tlCleanCutsH.add(pImage.width)
    tlCleanCutsV.add(pImage.height)
    
    tlHChunks = quadChunks(tlCleanCutsH)
    tlVChunks = quadChunks(tlCleanCutsV)
    
    plTextures=[]
    
    repeat with tV = 1 to tlVChunks.count-1
      tVSrc = tlVChunks[tV+1] - tlVChunks[tV]
      tVTexture = powerOf2(tVSrc+2*pSeamFix)
      
      tlTexturesH = []
      
      repeat with tH = 1 to tlHChunks.count-1
        tHSrc = tlHChunks[tH+1] - tlHChunks[tH]
        tHTexture = powerOf2(tHSrc+2*pSeamFix)
        
        tChunkName = tBaseName & "(" & tH & "," & tV & ")"
        
        tTextureImage = image(tHTexture, tVTexture,32)
        tTexture = spv().pWorld.newTexture(tChunkName, #fromImageObject, tTextureImage.duplicate())
        
        if node.attributeValue["depth"] <> "high" then
          if pImage.useAlpha = true then tRenderFormat = #rgba8888
          else tRenderFormat = #rgba8880
        else
          if pImage.useAlpha = true then tRenderFormat = #rgba4444
          else tRenderFormat = #rgba5650
        end if
        
        tTexture.renderFormat = tRenderFormat
        tTexture.nearFiltering = true
        tTexture.quality = #high
        tTexture.compressed = true
        
        tSrcRect = rect(0,0, tHSrc, tVSrc).offset(tlHChunks[tH], tlVChunks[tV])
        
        tlTexturesH.add([#name:tChunkName, #rect:tSrcRect, #size:point(tHTexture, tVTexture), #texture:tTexture])
      end repeat
      plTextures.add(tlTexturesH)
    end repeat
    
    me.updateTextures()
    
    if getValue(#boolean, node.attributeValue["dynamic"], false) = false then
      pImage = VOID
      plResources = VOID
    end if
    
    me.updateScale(parent.pScaleTextures)
  end if
end

on destroyTextures me
  if not voidP(plTextures) then
    repeat with tlTexturesV in plTextures
      repeat with tlTextures in tlTexturesV
        spv().pWorld.deleteTexture(tlTextures[#name])
      end repeat
    end repeat
  end if
end

on updateTextures me
  if not voidP(pImage) then
    repeat with tlTexturesV in plTextures
      repeat with tlTexture in tlTexturesV
        tTexture = tlTexture[#texture]
        
        tHSrc = tlTexture[#rect].width
        tHTexture = powerOf2(tHSrc+2*pSeamFix)
        
        tVSrc = tlTexture[#rect].height
        tVTexture = powerOf2(tVSrc+2*pSeamFix)
        
        tSrcRect = tlTexture[#rect]
        
        tChunkImage = image(tTexture.width, tTexture.height,32)
        tChunkImage.setAlpha(0)
        
        tChunkImage.useAlpha = pImage.useAlpha
        tChunkImage.copyPixels(pImage, rect(0,0,tHSrc, tVSrc).offset(pSeamFix,pSeamFix), tSrcRect)
        
        -- horizontal seams
        tHSeamRect = rect(tHSrc+pSeamFix, 0, tHTexture, tVTexture)
        tHSeamSrc = rect(tHSrc+pSeamFix-1, 0, tHSrc+pSeamFix, tVTexture)
        tChunkImage.copyPixels(tChunkImage, tHSeamRect, tHSeamSrc)
        tHSeamRect = rect(0, 0, pSeamFix, tVTexture)
        tHSeamSrc = rect(pSeamFix, 0, pSeamFix+1, tVTexture)
        tChunkImage.copyPixels(tChunkImage, tHSeamRect, tHSeamSrc)
        
        -- vertical seams
        tVSeamRect = rect(0, tVSrc+pSeamFix, tHTexture, tVTexture)
        tVSeamSrc = rect(0, tVSrc+pSeamFix-1, tHTexture, tVSrc+pSeamFix)
        tChunkImage.copyPixels(tChunkImage, tVSeamRect, tVSeamSrc)
        tVSeamRect = rect(0, 0, tHTexture, pSeamFix)
        tVSeamSrc = rect(0, pSeamFix, tHTexture, pSeamFix+1)
        tChunkImage.copyPixels(tChunkImage, tVSeamRect, tVSeamSrc)
        
        
        tTexture.image = tChunkImage
      end repeat
    end repeat
  end if
end

on updateScale me, aFactor
  repeat with tlTexturesV in plTextures
    repeat with tlTexture in tlTexturesV
      tTexture = tlTexture[#texture]
      
      if aFactor >= 0 then
        -- changing both the compressed and renderformat property of a texture resets its scaling.
        -- (interesting!)
        tTexture.compressed=not(tTexture.compressed)
        tFormat = tTexture.renderFormat
        if tFormat<>#rgba4444 then
          tTexture.renderFormat = #rgba4444
        else tTexture.renderFormat = #rgba5650
        
        tTexture.compressed=not(tTexture.compressed)
        tTexture.renderFormat = tFormat
      else
        repeat with tI = 1 to abs(aFactor)
          tTexture.scaleDown()
        end repeat
      end if
    end repeat
  end repeat
end



-- ------------------------
-- returns a list of power-
-- of-2s chunks (for creating
-- texture chunks from large
-- images)
--
on quadChunks alCleanCuts
  tMaxRest = 0.12 -- allowed wasting textures for performance
  
  tSeamFix = 2*pSeamFix -- reserved pixels for seam fix
  
  tMinChunk = 16 - tSeamFix
  tMaxChunk = min(512, getRendererServices().getHardwareInfo()[#maxTextureSize][1]) - tSeamFix
  
  tlAllChunks = []
  tTotal = 0
  
  repeat with tNumber in alCleanCuts
    tlChunks = []
    tChunkSize = tMaxChunk
    
    repeat while tTotal < tNumber
      if tTotal + tChunkSize < tNumber * (1.0 + tMaxRest) then
        tChunkSize = max(tMinChunk, tChunkSize)
        
        tTotal = tTotal + tChunkSize
        tlChunks.add(tChunkSize)
      else
        tChunkSize = ((tChunkSize + tSeamFix) / 2) - tSeamFix
      end if
    end repeat
    
    repeat with tChunkNr = tlChunks.count down to 2
      if tlChunks[tChunkNr] = tlChunks[tChunkNr -1] and \
       tSeamFix + 2 * tlChunks[tChunkNr-1] <= tMaxChunk then
        
        tlChunks[tChunkNr-1] = tSeamFix + 2 * tlChunks[tChunkNr-1]
        tTotal = tTotal + tSeamFix
        tlChunks.deleteAt(tChunkNr)
      end if
    end repeat
    
    tlChunks.sort()
    tlChunks[1] = tlChunks[1] - (tTotal - tNumber)
    tTotal = tNumber
    
    tlAllChunks = appendList(tlAllChunks, tlChunks)
  end repeat
  
  tlAllChunks = accumulate(tlAllChunks)
  repeat with tChunkNr = tlAllChunks.count down to 1
    if tlAllChunks[tChunkNr]<0 then tlAllChunks.deleteAt(tChunkNr)
  end repeat
  
  tlAllChunks.addAt(1,0)
  return tlAllChunks
end


--
-- public properties
--

on setProp me, aProperty, aValue
  if node.attributeValue["dynamic"] = "true" then
    tAttribute = "type"
    tA = "alpha"
    case aProperty of
      "text", "alphatext":
        tResource = "src"
        if aProperty contains tA then 
          tResource = tA & tResource
          tAttribute = tA & tAttribute
        end if
        tResource = symbol(tResource)
        
        if node.attributeValue[tAttribute] = "text" then
          tValue = me.makeHTML(aValue)
          if plResources[tResource][#resource] <> tValue then
            plResources[tResource][#resource] = tValue
            me.makeLayer(true)
          end if
        end if
        
      "color", "alphacolor", "endcolor", "alphaendcolor":
        tProperty = "Color"
        if aProperty contains "end" then tProperty = "End" & tProperty
        if aProperty contains tA then 
          tProperty = tA & tProperty
          tAttribute = tA & tAttribute
        end if
        tProperty = symbol("p" & tProperty)
        
        if ["matte", "ellipse", "gradient"].getOne(node.attributeValue[tAttribute]) >0 then
          if aValue<>me[tProperty] then
            me[tProperty] = aValue
            me.makeLayer(true)
          end if
        end if
        
      "direction", "alphadirection":
        tProperty = "Direction"
        if aProperty contains tA then 
          tProperty = tA & tProperty
          tAttribute = tA & tAttribute
        end if
        tProperty = symbol("p" & tProperty)
        
        if node.attributeValue[tAttribute] = "gradient" then
          tValue = getValue(#float, aValue, me[tProperty]) mod 360
          if me[tProperty] <> tValue then
            me[tProperty] = aValue
            me.makeLayer(true)
          end if
        end if
        
    end case
  end if
end

