-- <tour> node
-- Instantiates/manages <scene> nodes

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt

property poGlobalNode
property plSceneNodes

property plXMLparts

property poScene

property pTitle, pDescription, pCopyright

property plNamedObjects
property plActionQueue

on new me,  _parent, aoNode
  parent = _parent
  
  node = aoNode
  
  return me
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void
    
    destroyObject(poScene)
    
    plSceneNodes = VOID
    poGlobalNode = VOID
    
    plNamedObject = VOID
    
    me.removeNamedObject(me)
    me.removeNamedObject(parent.poMouseManager)
    me.removeNamedObject(parent.poNavigator)
  end if
end

on initNode me
  if voidP(pBuilt) then
    pBuilt = true
    
    plActionQueue = []
    
    -- initialise named nodes
    plNamedObjects = [:]
    me.addNamedObject(parent.poNavigator, "_camera")
    me.addNamedObject(parent.poMouseManager, "_mouse")
    me.addNamedObject(me, "_tour")
    if not voidP(parent.poPlugin) then me.addNamedObject(parent.poPlugin,"_plugin")
    
    spv().poLogger.show("Analysing XML document", #info)
    
    -- find or create <global> node
    poGlobalNode = node["global"]
    if voidP(poGlobalNode) then poGlobalNode = addNode("global", node, [:])
    
    toMeta = poGlobalNode["meta"]
    if voidP(toMeta) then toMeta = addNode("meta", poGlobalNode, [:])
    
    -- hash nodes before external XML parts are added.
    --spv().poLicenseManager.hashNodes()
    
    tlLicenses = poGlobalNode.getElementByName("license")
    if tlLicenses.count = 0 then
      toLicense = addNode("license", toMeta, ["src":the moviePath & "license.xml"])
      tlLicenses.add(toLicense)
    end if
    
    -- add panoelement, image, layer and cameralimits if simple mode
    if parent.pSimpleMode then
      tlNodes = []
      repeat with tNodeName in ["panogroup","uigroup","layer","behavior"]
        tlNodes = appendList(tlNodes, node.getElementByName(tNodeName))
      end repeat
      
      repeat with toNodeNr = 1 to tlNodes.count
        toNode = tlNodes[toNodeNr]
        if voidP(toNode.attributeValue["relativeurl"]) then
          toNode.attributeValue["relativeurl"] = "@"
        end if
      end repeat
      
      toScene = node["scene"]
      if voidP(toScene) then toScene = addNode("scene",node,["id":"simple-scene"])
      
      toPanoElement = addNode("panoelement", toScene, ["id":"simple-pano"])
      toImage = addNode("image", toPanoElement, ["id":"simple-image"])
      toLayer = addNode("layer", toImage, ["class":"base", "type":"bitmap", "src":parent.pSourceURL])
      
      toMeta = toScene["meta"]
      if voidP(toMeta) then toMeta = addNode("meta", toScene, [:])
      toCameraLimits = toMeta["cameralimits"]
      if voidP(toCameraLimits) then toCameraLimits = addNode("cameralimits", toMeta, [:])
      toCameraLimits.attributeValue["panoelement"]="simple-pano"
    end if
    
    -- check global node for src
    plXMLparts = []
    tlCheckNodes = node.getElementByName("scene")
    tlCheckNodes.add(poGlobalNode)
    repeat with toLicense in tlLicenses
      tlCheckNodes.add(toLicense)
    end repeat
    
    repeat with tCheckNr = 1 to tlCheckNodes.count
      toCheckNode = tlCheckNodes[tCheckNr]
      if not voidP(toCheckNode.attributeValue["src"]) then
        plXMLparts.add ([#node:toCheckNode, #src:toCheckNode.attributeValue[#src], #xml:void])
      else
        repeat with tGroupName in ["panogroup", "uigroup"]
          tlGroups = toCheckNode.getElementByName(tGroupName)
          repeat with tlGroupNr = 1 to tlGroups.count
            toGroup = tlGroups[tlGroupNr]
            if not voidP(toGroup.attributeValue["src"]) then
              if toGroup.attributeValue["relativeurl"] = "@" then
                tRelativeURL = the moviePath
              else tRelativeURL = parent.pSourceURL
              
              --plXMLparts.add ([#node:toGroup, #src:relativeURL(toGroup.attributeValue[#src], tRelativeURL), #xml:void])
              plXMLparts.add ([#node:toGroup, #src:toGroup.attributeValue[#src], #xml:void])
            end if
          end repeat
        end repeat
      end if
    end repeat
    
    if plXMLparts.count = 0 then me.openTour()
    else
      repeat with tlXMLpart in plXMLparts
        spv().poDownloader.addJob(tlXMLpart[#src], spv().pSourceURL, 0, #text, #getXMLpart, me)
      end repeat
    end if
  end if
end

on getXMLpart me, alResult
  tResult = false
  if not voidP(alResult[#result]) then
    toXML = new(script("xml.parser"))
    tError = toXML.parseString(alResult[#result])

    if not voidP(tError) then
      spv().poLogger.show("XML error parsing file:" && tError, #warning)
      toXML = false
    end if
  else toXML = false

  tAllDone = true
  repeat with tlXMLpart in plXMLparts
    if tlXMLpart[#src] = alResult[#URL] then
      tlXMLpart[#xml] = toXML
    else if voidP(tlXMLpart[#xml]) then tAllDone = false
  end repeat

  if tAllDone = true then
    repeat with tPartNr = 1 to plXMLparts.count
      tlXMLpart = plXMLparts[tPartNr]
      if objectP(tlXMLpart[#xml]) then
        tDestParent = tlXMLpart[#node].parent
        tDestChildNr = tDestParent.child.getOne(tlXMLpart[#node])

        tlSrcNodes = tlXMLpart[#xml].getElementByName(tlXMLpart[#node].name)
        if tlSrcNodes.count>0 then
          tSrcNode = tlSrcNodes[1]
          tRelativeURL = tSrcNode.attributeValue["relativeurl"]
          if not stringP(tRelativeURL) then tRelativeURL = tlXMLpart[#node].attributeValue["relativeurl"]
          if stringP(tRelativeURL) then
            case tRelativeURL of:
              "":
                tRelativeURL = parent.pSourceURL
              "#":
                tRelativeURL = relativeURL(tlXMLpart[#src], parent.pSourceURL)
              "@":
                tRelativeURL = the moviepath
            end case

            -- layer and behavior nodes are affected by relativeURL
            tlSrcNodes = []
            repeat with tNodeName in ["layer","behavior"]
              appendList(tlSrcNodes, tSrcNode.getElementByName(tNodeName))
            end repeat

            repeat with tNodeNr = 1 to tlSrcNodes.count
              toNode = tlSrcNodes[tNodeNr]
              if voidP(toNode.attributeValue["relativeurl"]) then
                toNode.attributeValue["relativeurl"] = tRelativeURL
              end if
            end repeat
            tSrcNode.attributeValue["relativeurl"] = tRelativeURL
          end if

          tlXMLpart[#node].parent.child[tDestChildNr] = tSrcNode
          tlXMLpart[#node].parent.child[tDestChildNr].parent = tDestParent
        else spv().poLogger.show("Could not load <"& tlXMLpart[#node].name &"> node from file '"& tlXMLpart[#src] &"':" && tError, #warning)

        tlXMLpart[#node] = VOID
        tlXMLpart[#xml] = VOID
      end if
    end repeat
    plXMLparts = VOID

    -- reset <global> node, in case it changed
    poGlobalNode = node["global"]

    me.openTour()
  end if
end


on openTour me
  -- add "emptyImage" <image> node to global
  toEmptyImageNode = addNode("image", poGlobalNode,["id":"_spv.emptyImage", "silent":"true"])

  -- add background panoelement
  toBGelement = addNode("panoelement", poGlobalNode,["id":"_spv.preview.bg", "silent":"true", "zorder":"_spv.preview"])
  toBGimage = addNode("image", toBGelement, ["silent":"true"])
  toBGlayer = addNode("layer", toBGimage, ["class":"base", "type":"bitmap", "src":"#_PanoBG"])

  -- add scene and history lists
  toHistoryList = addNode("list",poGlobalNode,["id":"_history"])

  tlProperties = ["id":"_scenes_id","behavior":"_scenesChangeBehavior","wrap":"true"]
  toSceneList = addNode("list",poGlobalNode,tlProperties)
  tlProperties = ["id":"_scenes_title","behavior":"_scenesChangeBehavior","wrap":"true"]
  toTitleList = addNode("list",poGlobalNode,tlProperties)
  tlProperties = ["id":"_scenes_description","behavior":"_scenesChangeBehavior","wrap":"true"]
  toDescriptionList = addNode("list",poGlobalNode,tlProperties)

  toSceneChange = addNode("behavior", poGlobalNode, ["id":"_scenesChangeBehavior", "object":"_none"])
  tlProperties = ["event":"index","type":"setProperty","target":"_scenes_*","property":"index","value":"=getProperty("&QUOTE&"_this"&QUOTE&","&QUOTE&"index"&QUOTE&")"]
  addNode("action", toSceneChange, tlProperties)
  addNode("action", toSceneChange, tlProperties)

  tlScenes = node.getElementByName("scene")
  repeat with tScene = 1 to tlScenes.count
    toScene = tlScenes[tScene]
    tID = getValue(#string, toScene.attributeValue["id"], spv().randomID("_scn"))
    toScene.attributeValue["id"] = tID

    tTitle = ""
    tDescription = ""

    toMeta = toScene["meta"]
    if not voidP(toMeta) then
      if not voidP(toMeta["title"]) then tTitle = toMeta["title"].getText()
      if not voidP(toMeta["description"]) then tDescription = toMeta["description"].getText()
      --
    else toMeta = addNode("meta", toScene)

    toValue = addNode("value", toSceneList)
    addNode(void, toValue, tID)
    toValue = addNode("value", toTitleList)
    addNode(void, toValue, tTitle)
    toValue = addNode("value", toDescriptionList)
    addNode(void, toValue, tDescription)
  end repeat

  -- get title, description and copyright info
  toMeta = poGlobalNode["meta"]
  repeat with tlValue in [[#pTitle, "title"],[#pDescription, "description"],[#pCopyright, "copyright"]]
    if not voidP(toMeta[tlValue[2]]) then me[tlValue[1]] = toMeta[tlValue[2]].getText()
    else me[tlValue[1]] = ""
  end repeat

  -- call logger and about scripts to add their stuff to XML
  spv().poLogger.addNodes()
  -- add about nodes
  spv().poLicenseManager.addNodes()
  -- downloader nodes
  spv().poDownloader.addNodes(poGlobalNode)

  spv().poLogger.show("XML document complete", #info)

  plSceneNodes = node.getElementByName("scene")
  if plSceneNodes.count > 0 then
    -- find and open initial scene
    toScene = VOID

    tPan = VOID
    tTilt = VOID
    tFOV = VOID

    if not voidP(spv().pSourceArgs) then
      the itemDelimiter = ","
      tSceneId = spv().pSourceArgs.item[1]
      if tSceneId = "" then tSceneId = void
      tPan = myfloat(spv().pSourceArgs.item[2])
      tTilt = myfloat(spv().pSourceArgs.item[3])
      tFOV = myfloat(spv().pSourceArgs.item[4])
    end if

    if voidP(tSceneId) then
      toDefaultView = toMeta["defaultview"]
      if not voidP(toDefaultView) then
        tSceneId = toDefaultView.attributeValue["scene"]
        if voidP(tPan) then tPan = myfloat(toDefaultView.attributeValue["pan"])
        if voidP(tTilt) then tTilt = myfloat(toDefaultView.attributeValue["tilt"])
        if voidP(tFOV) then tFOV = myfloat(toDefaultView.attributeValue["fov"])
      end if
    end if

    if not voidP(tSceneId) then
      repeat with tSceneNode in plSceneNodes
        if tSceneNode.attributeValue["id"] = tSceneId then
          toScene = tSceneNode
          exit repeat
        end if
      end repeat

      if voidP(toScene) then spv().poLogger.show("Scene '"& tSceneId &"' not defined.", #warning)
    end if

    if voidP(toScene) then
      toScene = plSceneNodes[1]
      tSceneId = toScene.attributeValue["id"]
    end if

    spv().poLogger.show("Preparing scene '" & tSceneId & "'", #info)
    poScene = new(script("spv.Scene"), me)
    poScene.openScene(toScene, tPan, tTilt, tFOV)
  else
    spv().poLogger.show("No scenes defined in XML file.", #error)
  end if
end

on addNamedObject me, aoObject, aID
  if voidP(aID) then aID = aoObject.node.attributeValue["id"]
  plNamedObjects[aID] = aoObject
end

on removeNamedObject me, aoObject
  repeat while plNamedObjects.getPos(aoObject)>0
    plNamedObjects.deleteOne(aoObject)
  end repeat
end

on queueAction me, aObject, aEvent, aImmediate
  plActionQueue.add([#object:aObject, #event:aEvent])
  if aImmediate then
    me.doActions(true)
  else newTimeout("doActions",0,#doActions,me)
end

on doActions me, aImmediate
  if not aImmediate then killTimeout("doActions")

  if not voidP(plActionQueue) then
    repeat while plActionQueue.count > 0
      tlAction = plActionQueue[1]
      plActionQueue.deleteAt(1)
      if not voidP(tlAction.object[#plBehaviorObjects]) then
        tlBehaviors = []
        repeat with toBehavior in tlAction.object.plBehaviorObjects
          if toBehavior[#object].pEnabled=true then tlBehaviors.add(toBehavior[#object])
        end repeat

        repeat with toBehavior in tlBehaviors
          toBehavior.doAction(tlAction.event, tlAction.object)
        end repeat

        if voidP(plActionQueue) then exit repeat
      end if
    end repeat
  end if
end
