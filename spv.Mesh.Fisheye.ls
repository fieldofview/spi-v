-- Fisheye modelresource

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pMesh
property pIsMesh

on new me, _parent, aUIElement
  pIsMesh = true
  return __new(me, _parent, void, aUIElement)
end

on destroy me
  -- Destroy model resource
  if not voidP(pMesh) then spv().pWorld.deleteModelResource(pMesh.name)
end

on buildMesh me
  tImageWidth = parent.poImageElement.pWidth
  tImageHeight = parent.poImageElement.pHeight
  
  tHFOV = getValue(#float, parent.pHFOV, 360)
  tHFOV = degRad(max(0.001, min(360, tHFOV)))
  tVFOV = getValue(#float, parent.pVFOV, tHFOV * tImageHeight / tImageWidth)
  tVFOV = degRad(max(0.001, min(360, tVFOV)))
  
  parent.pFOVRanges = [[radDeg(-tHFOV/2), radDeg(tHFOV/2)], \
                       [radDeg(-tVFOV/2), radDeg(tVFOV/2)]]
  
  tResolution = getValue(#integer, parent.pMeshResolution,6)
  tPixelsPerVertex = float(tImageWidth) * degRad(tResolution) / tHFOV
  
  tlFaces = []
  tlVertices = []
  tlTextureCoordinates = []
  
  repeat with tV = 1 to parent.plShaders.count
    tRowRect = parent.plShaders[tV][1][#rect]
    
    tFOVStartV = tVFOV * (-.5 + myfloat(tRowRect.top) / tImageHeight)
    tFOVHeight = tVFOV * tRowRect.height / myfloat(tImageHeight)
    
    -- find vertical points in texture chunks
    tlPointsV = []
    repeat with tVLoc = 0 to tRowRect.height / tPixelsPerVertex
      --tlPointsV.add(integer(tVLoc * tPixelsPerVertex))
      tlPointsV.add(tVLoc * tPixelsPerVertex)
    end repeat
    
    if tlPointsV.count > 1 and tRowRect.height - tlPointsV[tlPointsV.count] < .3 * tPixelsPerVertex then
      tlPointsV[tlPointsV.count] = tRowRect.height
    else tlPointsV.add(tRowRect.height)
    
    repeat with tH = 1 to parent.plShaders[tV].count
      tShader = parent.plShaders[tV][tH].shader
      tRect = parent.plShaders[tV][tH][#rect]
      tChunkSize = parent.plShaders[tV][tH][#size]
      
      tFOVStartH = tHFOV * (-.5 + myfloat(tRect.left) / tImageWidth)
      tFOVWidth = tHFOV * tRect.width / myfloat(tImageWidth)
      
      -- find horizontal points in texture chunks
      tlPointsH = []
      repeat with tHLoc = 0 to tRect.width / tPixelsPerVertex
        --tlPointsH.add(integer(tHLoc * tPixelsPerVertex))
        tlPointsH.add(tHLoc * tPixelsPerVertex)        
      end repeat
      
      if tlPointsH.count > 1 and tRect.width - tlPointsH[tlPointsH.count] < .3 * tPixelsPerVertex then
        tlPointsH[tlPointsH.count] = tRect.width
      else tlPointsH.add(tRect.width)
      
      repeat with tVertexV = 1 to tlPointsV.count
        tVertexPosV = tFOVStartV + tFOVHeight * myfloat(tlPointsV[tVertexV]) / tRect.height
        
        tTexCoordV = 1 - myfloat(tlPointsV[tVertexV]) / tChunkSize.locV  
        repeat with tVertexH = 1 to tlPointsH.count
          tVertexPosH = tFOVStartH + tFOVWidth * myfloat(tlPointsH[tVertexH]) / tRect.width
          
          tTheta = sqrt(power(tVertexPosH,2)+ power(tVertexPosV,2))
          if tVertexPosH = 0 then
            if tVertexPosV>=0 then tPhi = 0
            else tPhi = PI
          else tPhi = atan(tVertexPosV/tVertexPosH)
          
          tX = cos(tPhi)*sin(tTheta)
          tY = sin(tPhi)*sin(tTheta)
          tZ = cos(tTheta)
          
          if tVertexPosH<0 then tX = - tX
          if tX < 0 then tY = - tY
          
          tlVertices.add(vector(tX, -tY, -tZ))
          
          tTexCoordH = myfloat(tlPointsH[tVertexH]) / tChunkSize.locH  
          tlTextureCoordinates.add(point(tTexCoordH, tTexCoordV))
          
          if tVertexH < tlPointsH.count and tVertexV < tlPointsV.count then
            tFace = [:]
            
            if not (tVertexV = 0 and tFOVStartV = -PI/2) then
              tFace[#vertices] = [tlVertices.count, tlVertices.count+tlPointsH.count, tlVertices.count+1]
              --tFace[#textureCoordinates] = [] -- use same index as #vertices...
              tFace[#shader] = tShader
              tlFaces.add(tFace.duplicate())
            end if
            
            if not (tVertexV = tlPointsV.count and tFOVStartV + tFOVHeight = PI/2) then
              tFace[#vertices] = [tlVertices.count+1, tlVertices.count+tlPointsH.count, tlVertices.count+tlPointsH.count+1]
              --tFace[#textureCoordinates] = [] -- use same index as #vertices...
              tFace[#shader] = tShader
              tlFaces.add(tFace.duplicate())
            end if
          end if
        end repeat
      end repeat
    end repeat
  end repeat
  
  __buildMesh(me, tlFaces, tlVertices, tlTextureCoordinates, true)
end
