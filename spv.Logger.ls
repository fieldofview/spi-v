-- Provide a common place to show errors, warnings and info

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pLogMember

property poDebugGroup
property poMessageLayer

property poLogCallback

property pSilent

on new me, _parent
  parent = _parent

  pSilent = true
  if the optionDown and the commandDown then
    the debugPlaybackEnabled = true
    pSilent = false
  end if

  if not voidP(member("_LogText")) then member("_LogText").erase()
  pLogMember = new(#text)
  pLogMember.fontSize = 9
  if the platform contains "Windows" then
    pLogMember.font = "Tahoma"
  else pLogMember.font = "Arial"
  pLogMember.name = "_LogText"

  return me
end

on destroy me
  if not pSilent then put "Shutting down engine"

  killTimeout("clearWarning")
  killTimeout("clearTitle")

  if not voidP(pLogMember) then pLogMember.erase()
end

on show me, aMessage, aType
  tTypeString = ""
  tShow = false
  tFatal = false

  tNowShowing = bool(getProperty("_spv.logger", "visible"))

  case aType of
    #info, #silent:
      nothing
    #warning:
      tTypeString = "WARNING: "
      --tShow = true

      killTimeout("clearWarning")
      if tNowShowing = false then
        newTimeout("clearWarning", 4000, #clearWarning, me)
      end if

    #error:
      tTypeString = "ERROR: "
      tShow = true
      tFatal = true
  end case

  if aType <> #silent then
    if the runMode = "Plugin" then netStatus(tTypeString & aMessage)
    if not voidP(parent.poLocalConnection) then parent.poLocalConnection.broadCast(tTypeString & aMessage)

    if not pSilent then put tTypeString & aMessage

    killTimeout("clearTitle")
    newTimeout("clearTitle", 2500, #clearTitle, me)

    pLogMember.text = pLogMember.text & tTypeString & aMessage & RETURN
    pLogMember.text = pLogMember.text.line[pLogMember.text.line.count-25 .. pLogMember.text.line.count]
  end if

  if not voidP(parent.poLDMscript) then
    if parent.poLDMscript.handler(#spvMessage) then
      toLDMscript = parent.poLDMscript
      tell the activeWindow to call(#spvMessage, toLDMscript, aMessage, aType)
    end if
  end if

  if the runmode = "Plugin" then
    externalEvent("spv#" & aType & ":" & aMessage)
  end if

  if tShow then setState me, #show
  if tNowShowing=true then me.updateTexture(aType)

  if tFatal then
    if getObjects("_spv.logger").count=0 then
      tSprite = parent.pSprite
      tMember = member("_PanelBG")
      tSprite.member = tMember
      tSprite.width = tMember.width
      tSprite.height = tMember.height
      pLogMember.width = 240
      pLogMember.antiAliasThreshold = 0
      tMember.image.useAlpha = false
      tMember.image.copyPixels(pLogMember.image, rect(8,8,248,248), rect(0,0,240,240))
    end if

    parent.pError = true
    abort()
  end if
end

on clearTitle me
  killTimeout("clearTitle")
  if the runMode = "Plugin" then netStatus("")
end

on clearWarning me
  killTimeout("clearWarning")
  setState me, #hide
end

on setState me, aShowHide
  tNowShowing = bool(getProperty("_spv.logger", "visible"))

  killTimeout("clearWarning")

  case aShowHide of
    #show:
      tShow = true
    #hide:
      tShow = false
    otherwise:
      tShow = not tNowShowing
  end case

  if tShow<>tNowShowing then
    if tShow = true then
      me.updateTexture()

      setProperty("_spv.logger", "visible", "true")
      if not voidP(parent.poLicenseManager) then
        toAboutGroup = parent.poLicenseManager.poAboutGroup
        if not voidP(toAboutGroup) then
          setProperty(toAboutGroup.attributeValue["id"], "visible", "false")
        end if
      end if

    else
      setProperty("_spv.logger", "visible", "false")
    end if
  end if
end

on addNodes me
  toGlobalNode = parent.poTour.poGlobalNode

  poDebugGroup = addNode("uigroup", toGlobalNode, ["id":"_spv.logger",  \
     "width":"256", "height":"256", "zorder":"_spv.branding", "visible":"false"])
  toBG =      addNode("uielement", poDebugGroup, ["silent":"true", "catchevents":"true"] )
  toBGImage = addNode("image", toBG, ["silent":"true"] )
  toBGLayer = addNode("layer", toBGImage, ["class":"base", "type":"bitmap", "src":"#_PanelBG"] )

  toText = addNode("uielement", poDebugGroup, ["silent":"true", \
    "halign":"left", "valign":"top", "hoffset":"8", "voffset":"-13", "zorder":"1"] )
  toTextImage = addNode("image", toText, ["silent":"true", "width":"248", "height":"16"] )
  toTextLayer = addNode("layer", toTextImage, ["class":"base", "type":"text", "antialias":"true"] )
  toTextText = addNode(VOID, toTextLayer, "SPi-V engine " & parent.pVersion & \
    ", Shockwave " & (the globals).version & " r" & string((the environment).productBuildVersion) )

  toMessage = addNode("uielement", poDebugGroup, ["silent":"true", \
    "halign":"left", "valign":"top", "hoffset":"8", "voffset":"-34", "zorder":"1"] )
  toMessageImage = addNode("image", toMessage, \
    ["id":"_spv.loggerImage", "silent":"true", "width":"240", "height":"204"])
  toMessageLayer = addNode("layer", toMessageImage, \
    ["class":"base", "type":"text", "src":"#_LogText", "valign":"bottom", "antialias":"true", "dynamic":"true"] )

  toDismiss = addNode("uielement", poDebugGroup, ["silent":"true", \
    "valign":"bottom", "voffset":"2", "zorder":"1"] )
  toDismissImage = addNode("image", toDismiss, ["silent":"true", "width":"200", "height":"24"] )
  toDismissLayer = addNode("layer", toDismissImage, ["class":"base", "type":"text", "antialias":"true"] )
  toDismissText = addNode(VOID, toDismissLayer, "<center><i>Click to dismiss...</i></center>")

  toDismissBehavior = addNode("behavior", toDismiss)
  toDismissPress = addNode("action", toDismissBehavior, ["event":"press", "type":"setProperty", \
    "target":"_spv.logger", "property":"visible", "value":"false"])
end

on updateTexture me, aType
  if not voidP(parent.poTour) then
    if not voidP(parent.poTour.poScene) then
      if not voidP(parent.poTour.poScene.plImages) then
        toLogImage = parent.poTour.poScene.plImages["_spv.loggerImage"]
        if not voidP(toLogImage) then
          if not voidP(toLogImage.plImageLayers) then
            if not voidP(toLogImage.plImageLayers["base"]) then
              toBaseLayer = toLogImage.plImageLayers["base"][#object]
              if not voidP(toBaseLayer) then
                if not voidP(toBaseLayer.plTextures) then
                  toBaseLayer.setProp("text", pLogMember.html)
                  if aType=#warning then updateStage()
                end if
              end if
            end if
          end if
        end if
      end if
    end if
  end if
end