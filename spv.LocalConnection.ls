-- Create and handle a Flash Local Connection object

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pEngineID
property plClients
property pLocalConnection

on new me, _parent
  parent = _parent

  pEngineID = parent.plSettings["spvID"]
  if voidP(pEngineID) then pEngineID = "spv.engine"
  plClients = []

  if parent.pSWV >= 10 then
    pLocalConnection = newObject("LocalConnection")

    setCallback(pLocalConnection, "onStatus", #messageSent, me)
    setCallback(pLocalConnection, "allowDomain", #allowDomain, me)
    setCallback(pLocalConnection, "spv.API", #incomingMessage, me)
    setCallback(pLocalConnection, "spv.subscribe", #subscribe, me)
    setCallback(pLocalConnection, "spv.unsubscribe", #unsubscribe, me)

    tResult = pLocalConnection.connect(pEngineID)
  end if

  if not(tResult) then parent.poLogger.show("Creating LocalConnection failed.", #warning)

  return me
end

on destroy me
  if objectP(pLocalConnection) then pLocalConnection.close()
end

on broadcast me, aMessage
  repeat with tClient in plClients
    pLocalConnection.send(tClient, "spv.message", aMessage)
  end repeat
end

on subscribe me, aClient
  if not voidP(aClient) and aClient<>"" then addOnce(plClients, aClient)
end

on unsubscribe me, aClient
  if not voidP(aClient) and aClient<>"" then plClients.deleteOne(aClient)
end

on messageSent me, aObject, aoInfo
  --  if aoInfo.level = "error" then parent.poLogger.show("Sending LocalConnection message failed.", #warning)
end

on allowDomain me, aObject, aDomain
  if aDomain = pLocalConnection.domain() then return true

  --  if the moviepath contains "://" then
  --    if domainFromPath(aDomain & "/") = domainFromPath(the moviePath) then return true
  --    -- nothing
  --  else if aDomain = "localhost" then return true

  --put "Message received from " & aDomain
end

on incomingMessage me, aObject, aMessage
  if not voidP(aMessage) then
    if validFunctions(aMessage, true) then
      return value(aMessage)
    else
      parent.poLogger.show("Illegal command received from LocalConnection: '" & aMessage & "'", #warning)
    end if
  end if
end
