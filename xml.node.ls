-- �1999 Andrew White for Cathode
-- andyw@cathode.co.uk
-- Additional API Hussein Boon for www.shocknet.org.uk
-- info@shocknet.org.uk

-- Version 1.0.5AH

-- Change history
-- 24 FEB 2006: Changed getAttributeByName() to use attributeValue instead of
--              attributeName for checking available attributes in a node
--              Code optimised hasAttribute and getAttribute
--              Removed unused/unneeded methods to conserve bytes
--
-- 13 MAR 2002: Removed node cloning methods
--              Standardised the returns from getElementByName().
--              Returns a propList when used with a wildcard; a list when used
--              with a named element and an empty list when the named element
--              is not present in the document (previously returned VOID)
--              Added getAttributeByName() method (as getElementByName())
--              Added hasAttribute() method
--              Added getAttribute() method
-- 17 FEB 2002: Added totalNodes method for memory management issues
-- Version 1.0.4
-- 15 JAN 2002: Initial public release of these extensions
-- 28 OCT 2001: Fixed abnormal behaviour in node cloning method
-- 21 AUG 2001: Added node cloning method and parent property.

-- Version 1.0.3
-- 28 SEP 1999: Fixed bug in count due to dot syntax failure of me.list.count
-- 28 SEP 1999: Fixed bug in attrString due to dot syntax failure of me.list.count
-- 27 SEP 1999: Added getText method
-- 07 SEP 1999: Added getElementByName method

-- Version 1.0.2
-- 05 SEP 1999: Added count property and count method to override the default
--              behavior of the count property which is to return the number
--              of properties in the object

-- Version 1.0.1
-- 04 JUL 1999: added isEmpty private property to store whether the element
--              is empty or not. This is to support document creation, as
--              we need to write a '/>' end to the tag of an empty element
-- 04 JUL 1999: added toString method to support creation of documents
-- 04 JUL 1999: added attrString method to support creation of documents
-- 04 JUL 1999: added encodeString method to support creation of documents

-- Version 1.0
-- 01 JUL 1999: Original Release

-- Description

-- The xml.node class implements a generic node of an XML document.
-- This a private class of the XML parser and implements the dot access
-- equivalent of the XML Parser Xtra.

-- Public properties
property name
property type
property text
property child
property count
property attributeName
property attributeValue
property parent

-- Private properties
property isEmpty


on new me, _name, _type, _data, _parent

  -- Constructor method.

  me.name = _name
  me.type = _type
  me.text = _data
  me.child = []
  me.attributeName = []
  me.attributeValue = [:]
  me.parent = _parent
  return me
end


-- Public API

--on makeSublist me
--
--  -- Accesor method to return a property list of the elements contents
--  -- Functionaly equivalent to the XML Parser Xtra's makeSubList() method
--
--  case me.type of then
--
--    #element:
--      xmlList = [:]
--      xmlList.addProp(me.name, me.toList())
--
--    #procInst:
--      xmlList = [:]
--      xmlList.addProp("!PROCINST", me.toList())
--
--    #charData:
--      xmlList = [:]
--      xmlList.addProp("!CHARDATA", me.toList())
--
--  end case
--
--  return xmlList
--
--end


-- Extended API

on getAt me, index

  -- Accessor method to implement [] access for subnodes.
  -- e.g.: parser["document"]["form"]["element"]

  -- If the index value is an integer then get the subnode by
  -- postion, otherwise search for a matching node name.
  -- It only returns the first matching node name

  if index.ilk = #integer then
    if getPos(me.child,index) then
      return me.child[index]
    else
      return VOID
    end if
  else

    index = string(index)
    repeat with node in me.child
      if node.name = index then
        return node
      end if
    end repeat

  end if

end


--on count me
--  -- Return the number of children for this node
--  return count(me.child)
--end
--
--on getParent me
--  return me.parent
--end


on getElementByName me, tagName, nodeList

  -- Accessor method to return all elements of a particular name

  if nodeList.ilk = #void then

    -- If the nodeList is void then initialise
    nodeList = []

  end if

  -- Iterate through the child nodes and if it is an element then
  -- call this method recursively, passing in the current node list
  repeat with node in me.child

    if node.type = #element then

      -- If the node's name matches add it to the nodeList
      if node.name = tagName then
        nodeList.add(node)
      end if

      -- Call ourselves recursively
      node.getElementByName(tagName,nodeList)

    end if

  end repeat

  -- If there's only one node then return
  -- that node rather than a list
  if nodeList.count = 1 then
    return [nodeList[1]]
  else
    return nodeList
  end if
end

on getElementById me, ID, nodeList

  -- Accessor method to return the child element of with specified id

  if nodeList.ilk = #void then

    -- If the nodeList is void then initialise
    nodeList = []

  end if

  -- Iterate through the child nodes and if it is an element then
  -- call this method recursively, passing in the current node list
  repeat with node in me.child

    if node.type = #element then

      -- If the node's id matches add it to the nodeList
      if node.attributeValue["id"] = ID then
        nodeList.add(node)
      else

        -- Call ourselves recursively
        node.getElementById(ID,nodeList)
      end if
    end if
    if nodeList.count > 0 then exit repeat
  end repeat

  -- If there's only one node then return
  -- that node rather than a list
  if nodeList.count = 1 then
    return nodeList[1]
  else
    return void
  end if
end


on getText me

  -- Return all of the text under this node

  case me.type of

      -- If it's a char data node then we just
      -- return the text property - char data nodes
      -- have no children

    #charData:
      return me.text

      -- For every node in the child list call getText
      -- recursively to build up all of the text beneath
      -- this node

    #element:
      str = ""
      repeat with node in me.child
        str = str & node.getText()
      end repeat
      return str

      -- Return the processing instruction

    #procInst:
      return me.text

  end case

end


-- Added by Hussein Boon
-- www.shocknet.org.uk
-- info@shocknet.org.uk
-- 11 MAR 2002

-- As getElementByName
-- As attributes are held in a property list this method
-- returns the node
-- You'll still have to operate on the node by using getAttribute("some_attName")
-- to get the attribute value

-- Changed by Aldo Hoeben
-- 24 FEB 2006

-- node.attributeName sometimes doesn't reflect all attributes
-- in node.attributeValue.

on getAttributeByName me, tagName, attList

  -- Accessor method to return all elements of a particular name

  if attList.ilk = #void then

    -- If the nodeList is void then initialise
    attList = []

  end if

  -- Iterate through the child nodes and if it is an element then
  -- call this method recursively, passing in the current node list

  repeat with node in me.child

    if node.type = #element then
      if not voidP(node.attributeValue[tagName]) then
        attList.add(node)
      end if
      node.getAttributeByName(tagName,attList)
    end if

  end repeat

  -- If there's only one node then return
  -- that node rather than a list
  if attList.count = 1 then
    return [attList[1]]
  else
    return attList
  end if
end

-- Changed by Aldo Hoeben
-- 24 FEB 2006
--
-- less code
--
-- Return whether this node has attributes
--on hasAttribute me, attName
--
--  return not voidP(me.attributeValue[attName])
--
--end
--
-- Changed by Aldo Hoeben
-- 24 FEB 2006
--
-- less code
--
-- Returns the value for the named attribute
-- Return void if attribute not present
--on getAttribute me, attName
--
--  return me.attributeValue[attName]
--
--end
--
-- Return the number of attributes for this node
--on getAttributeCount me
--
--  return attributeValue.count
--
--end

-- Private API

--on toList me
--
--  -- Private method to return a property list in the same
--  -- format as the XML Parser Xtra.
--
--  case me.type of
--
--    #element:
--      elemList = [:]
--      attrList = duplicate(attributeValue)
--      elemList.addProp("!ATTRIBUTES", attrList)
--
--      repeat with i in child
--
--        case i.type of
--          #element: elemList.addProp(i.name, i.toList())
--          #charData: elemList.addProp("!CHARDATA", i.toList())
--          #procInst: elemList.addProp("!PROCINST", i.toList())
--        end case
--
--      end repeat
--
--      return elemList
--
--    #charData:
--      return me.text
--
--    #procInst:
--      piList = [:]
--      piList.addProp("NAME", me.name)
--      piList.addProp("TEXT", me.text)
--
--      return piList
--
--  end case
--
--end


on toString me

  -- Private method to return an XML node's string
  -- representation for inserting into a document

  case me.type of

    #element:

      if me.isEmpty then

        tag = "<" & me.name & me.attrString() & "/>"

      else

        tag = "<" & me.name & me.attrString() & ">"
        repeat with node in me.child
          tag = tag & node.toString()
        end repeat
        tag = tag & "</" & me.name & ">"

      end if

      return tag

    #charData:
      return me.encodeString(me.text)

    #procInst:
      return ("<?" & me.name && me.text & "?>")

  end case

end


on attrString me

  -- Private method for building the attributes of
  -- an element as a string to be inserted into a tag

  numAttr = count(me.attributeValue)
  attrStr = ""

  if numAttr then

    repeat with i = 1 to numAttr

      attrName = attributeValue.getPropAt(i)
      attrValue = me.encodeString(attributeValue.getAt(i))

      attrStr = attrStr && attrName & "=" & QUOTE & attrValue & QUOTE

    end repeat

  end if

  return attrStr

end


on encodeString me, str

  -- Private method to encode strings for insertion
  -- into an XML document

  encodedStr = ""
  numChars = str.length

  repeat with i = 1 to numChars

    currChar = str.char[i]

    case currChar of

      "<": encodedStr = encodedStr & "&lt;"
      ">": encodedStr = encodedStr & "&gt;"
      "'": encodedStr = encodedStr & "&apos;"
      QUOTE: encodedStr = encodedStr & "&quot;"
      "&": encodedStr = encodedStr & "&amp;"

      otherwise

        charNum = charToNum(currChar)
        if charNum > 127 then
          encodedStr = encodedStr & "&#" & charNum & ";"
        else
          encodedStr = encodedStr & currChar
        end if

    end case

  end repeat

  return encodedStr

end


-- This handler is provided for those developers who want to know
-- how many nodes are in memory for a particular document
-- or document fragment
--on totalNodes me, amtIn
--  repeat with i = 1 to child.count
--    amtIn = child[i].totalNodes(amtIn)
--  end repeat
--  amtIn = amtIn + child.count
--  return amtIn
--end

