-- Check licenses in the global node

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property poAdditionalText

property poAboutGroup
property poAboutBehavior

on new me, _parent
  parent = _parent

  return me
end

on destroy me

end

on addNodes me
  toGlobalNode = parent.poTour.poGlobalNode

  tRandomID = parent.randomID("_spv.about")
  poAboutGroup = addNode("uigroup", toGlobalNode, ["id":tRandomID,  \
  "width":"256", "height":"256", "zorder":"_spv.branding", "visible":"false"])
  toBG = addNode("uielement", poAboutGroup, ["silent":"true", "catchevents":"true"] )
  toBGImage = addNode("image", toBG, ["silent":"true"] )
  toBGLayer = addNode("layer", toBGImage, ["class":"base", "type":"bitmap", "src":"#_PanelBG"] )

  --poAboutTimer = addNode("ramp", poAboutGroup, ["duration":"0.5"])
  poAboutBehavior = addNode("behavior", poAboutGroup, ["enabled":"false"])
  toAboutAction = addNode("action", poAboutBehavior, ["event":"ready", \
    "type":"setProperty", "target":poAboutgroup.attributeValue["id"], "property":"visible", "value":"true"])
  toAboutAction = addNode("action", poAboutBehavior, ["event":"ready", \
    "type":"setProperty", "target":"_spv.aboutHide", "property":"active", "value":"true"])

  toSPV = addNode("uielement", poAboutGroup, ["silent":"true", \
    "halign":"left", "valign":"top", "hoffset":"5", "voffset":"-5"] )
  toSPVImage = addNode("image", toSPV, ["silent":"true"] )
  toSPVLayer = addNode("layer", toSPVImage, ["class":"base", "type":"bitmap", "src":"#spv-logo"] )
  toSPVBehavior = addNode("behavior", toSPV)
  toSPVPress = addNode("action", toSPVBehavior, ["event":"press", "type":"getURL", \
    "target":"_spv-info", "url":"http://www.fieldofview.com/spv"])

  toShockwave = addNode("uielement", poAboutGroup, ["silent":"true", \
  "halign":"left", "valign":"top", "hoffset":"5", "voffset":"-66"] )
  toShockwaveImage = addNode("image", toShockwave, ["silent":"true"] )
  toShockwaveLayer = addNode("layer", toShockwaveImage, ["class":"base", "type":"bitmap", "src":"#shockwave-logo"] )
  toShockwaveBehavior = addNode("behavior", toShockwave)
  toShockwavePress = addNode("action", toShockwaveBehavior, ["event":"press", "type":"getURL", \
  "target":"_spv-info", "url":"http://www.adobe.com/products/shockwaveplayer/"])

  toFOV = addNode("uielement", poAboutGroup, ["silent":"true", \
  "halign":"left", "valign":"top", "hoffset":"5", "voffset":"-186"] )
  toFOVImage = addNode("image", toFOV, ["silent":"true"] )
  toFOVLayer = addNode("layer", toFOVImage, ["class":"base", "type":"bitmap", "src":"#fov-logo"] )
  toFOVBehavior = addNode("behavior", toFOV)
  toFOVPress = addNode("action", toFOVBehavior, ["event":"press", "type":"getURL", \
    "target":"_spv-info", "url":"http://www.fieldofview.com/spv"])

  toText = addNode("uielement", poAboutGroup, ["silent":"true", \
    "halign":"left", "valign":"top", "hoffset":"64", "voffset":"-13", "zorder":"1"] )
  toTextImage = addNode("image", toText, ["silent":"true", "width":"180", "height":"240"] )
  toTextLayer = addNode("layer", toTextImage, ["class":"base", "type":"text", "antialias":"true"] )
  toTextText = addNode(VOID, toTextLayer, "SPi-V engine " & parent.pVersion & \
    "<BR><font size=2>&copy; Aldo Hoeben / fieldOfView" & \
    "<BR><BR><BR>Powered by Adobe Shockwave " & \
    (the globals).version & " build " & string((the environment).productBuildVersion) & \
    "<BR><BR><small>XML parser by Andrew White and Hussein Boon. Additional code by " & \
    "James Newton and Joni Huhmarniemi. Some code inspired by Barry Swan." & \
    "<BR><BR><BR><BR><BR>www.fieldofview.com/spv</font>")

  toAdditional = addNode("uielement", poAboutGroup, ["silent":"true", \
    "halign":"left", "valign":"top", "hoffset":"64", "voffset":"-42"] )
  toAdditionalImage = addNode("image", toAdditional, ["silent":"true", "width":"180", "height":"32"] )
  toAdditionalLayer = addNode("layer", toAdditionalImage, ["class":"base", "type":"text", "antialias":"true"] )
  poAdditionalText = addNode(VOID, toAdditionalLayer, "")

  toLicense = addNode("uielement", poAboutGroup, ["silent":"true", \
    "halign":"left", "valign":"top", "hoffset":"64", "voffset":"-64", "zorder":"1"] )
  toLicenseImage = addNode("image", toLicense, ["silent":"true", "width":"180", "height":"32"] )
  toLicenseLayer = addNode("layer", toLicenseImage, ["class":"base", "type":"text", "antialias":"true"] )

  toDismiss = addNode("uielement", poAboutGroup, ["silent":"true", \
    "valign":"bottom", "voffset":"2", "zorder":"1"] )
  toDismissImage = addNode("image", toDismiss, ["silent":"true", "width":"200", "height":"24"] )
  toDismissLayer = addNode("layer", toDismissImage, ["class":"base", "type":"text", "antialias":"true"] )
  toDismissText = addNode(VOID, toDismissLayer, "<center><i>Click to dismiss...</i></center>")

  toDismissBehavior = addNode("behavior", toDismiss)
  toDismissPress = addNode("action", toDismissBehavior, ["event":"press", "type":"setProperty", \
    "target":poAboutgroup.attributeValue["id"], "property":"visible", "value":"false"])
end

on setState me, aShowHide
  if not voidP(poAboutGroup) then
    tNowShowing = bool(getProperty(poAboutGroup.attributeValue["id"], "visible"))

    case aShowHide of
      #show:
        tShow = true
      #hide:
        tShow = false
      otherwise:
        tShow = not tNowShowing
    end case

    if tShow<>tNowShowing then
      if tShow = true then
        setProperty("_spv.logger", "visible", "false")
        setProperty(poAboutGroup.attributeValue["id"], "visible", "true")
      else
        setProperty(poAboutGroup.attributeValue["id"], "visible", "false")
      end if
    end if
  end if
end

