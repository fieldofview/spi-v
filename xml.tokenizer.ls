-- �1999 Andrew White for Cathode
-- andyw@cathode.co.uk
-- Version 1.0.5

-- Version 1.0.5
-- 17 FEB 2002: NO CHANGES

-- 1 JUL 99: Original Release

-- Description

-- The xml.tokenizer class implements a Lingo version of the
-- Java tokenizer. This class is used to break up the xml string
-- into bite-sized chunks for the parser.

-- It can handle multiple delimiter chars and can optionally
-- return the delimiter chars in the token stream if they
-- help provide contextual information.

-- Private properties
property _tokenList
property _currPos


on new me, str, delims, returnTokens

  -- Constructor method. The majority of work is done here

  -- Set the default values for the delims and return tokens
  -- if the user didn't specify those in the new() method call

  case (the paramCount) of

    2:
      delims = " " & TAB & RETURN
      returnTokens = FALSE

    3:
      returnTokens = FALSE

  end case

  me._tokenList = []
  numDelims = delims.length
  delimList = []

  -- Test each delim char to see whether it occurs in the
  -- search string. This can provide a small speed gain by
  -- not testing for delims that aren't there.

  repeat with i = 1 to numDelims

    delimChar = delims.char[i]

    if offset(delimChar, str) then
      delimList.add(delimChar)
    end if

  end repeat

  -- Test each delim char and find the minmum offset value.
  -- Then add the string upto that point as a token. If we're
  -- returning the delim chars then add that. Repeat until
  -- there are no more delim chars in the remaining string

  repeat while str.length

    offsetList = []

    repeat with j in delimList

      charOffset = offset(j, str)
      if charOffset then
        offsetList.add(charOffset)
      end if

    end repeat

    if offsetList.count then

      tokenOffset = min(offsetList)

      if tokenOffset > 1 then
        me._tokenList.add(str.char[1..(tokenOffset - 1)])
      end if

      if returnTokens then
        me._tokenList.add(str.char[tokenOffset])
      end if

      delete str.char[1..tokenOffset]

    else

      me._tokenList.add(str)
      str = ""

    end if

  end repeat

  -- Set the initial position for nextToken()

  me._currPos = 1

  return me

end


-- Public API

on countTokens me

  -- Simple accessor for querying the number of tokens

  return me._tokenList.count

end


on hasMoreTokens me

  -- Simple accessor method for querying whether we're at
  -- the end of the token list

  if me._currPos <= me._tokenList.count then
    return TRUE
  else
    return FALSE
  end if

end


on nextToken me

  -- Simple iterator method for walking through the token list

  if me._currPos <= me._tokenList.count then

    token = me._tokenList[me._currPos]
    me._currPos = me._currPos + 1
    return token

  else

    return VOID

  end if

end
