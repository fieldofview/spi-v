-- Rectangular modelresource

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pMesh
property pIsMesh

on new me, _parent, aUIElement
  pIsMesh = true
  return __new(me, _parent, void, aUIElement)
end

on destroy me
  -- Destroy model resource
  if not voidP(pMesh) then spv().pWorld.deleteModelResource(pMesh.name)
end

on buildMesh me, aUIElement
  tImageWidth = parent.poImageElement.pWidth
  tImageHeight = parent.poImageElement.pHeight

  if not(aUIElement) then
    tHFOV = getValue(#float, parent.pHFOV, 8.0)
    tHFOV = degRad(max(0.001, min(170, tHFOV)))
    tVFOV = getValue(#float, parent.pVFOV, radDeg(2*atan(tan(myfloat(tHFOV)/2) * float(tImageHeight) / tImageWidth)))
    tVFOV = degRad(max(0.001, min(170, tVFOV)))

    tOffsetH = getValue(#float, parent.pHOffset, 0)
    tOffsetH = degRad(tOffsetH)
    tOffsetV = getValue(#float, parent.pVOffset, 0)
    tOffsetV = degRad(tOffsetV)

    tMeshWidth = 2 * tan (tHFOV / 2.0)
    tMeshHeight = 2 * tan (tVFOV / 2.0)

    tMeshOffsetWidth = tMeshWidth * (tOffsetH / tHFOV)
    tMeshOffsetHeight = tMeshHeight * (tOffsetV / tVFOV)

    parent.pFOVRanges = [[radDeg(atan(-tMeshWidth /2 + tMeshOffsetWidth )), radDeg(atan(tMeshWidth /2 + tMeshOffsetWidth ))], \
                         [radDeg(atan(-tMeshHeight/2 + tMeshOffsetHeight)), radDeg(atan(tMeshHeight/2 + tMeshOffsetHeight))]]
  else
    -- create flat mesh for UI element
    tMeshWidth = parent.pWidth
    tMeshHeight = parent.pHeight

    tMeshOffsetWidth = 0
    tMeshOffsetHeight = 0

  end if
  tZ = -1

  tlFaces = []
  tlVertices = []
  tlLastVertices = []
  tlTextureCoordinates = [point(0,1)]

  repeat with tV = 1 to parent.plShaders.count
    repeat with tH = 1 to parent.plShaders[tV].count
      tFace = [:]
      tRect = parent.plShaders[tV][tH][#rect]

      tX =  ((float (tRect.left) / tImageWidth) -.5) * tMeshWidth  + tMeshOffsetWidth
      tY = -((float (tRect.top) / tImageHeight) -.5) * tMeshHeight + tMeshOffsetHeight

      tTexCoordX = myfloat(tRect.width) / parent.plShaders[tV][tH][#size].locH
      tTexCoordY = 1- myfloat(tRect.height) / parent.plShaders[tV][tH][#size].locV

      tlVertices.add(vector (tX, tY, tZ))

      tlFaceTexCoords = [ 1, \
        addOnce(tlTextureCoordinates, point(tTexCoordX, 1)), \
        addOnce(tlTextureCoordinates, point(0, tTexCoordY)), \
        addOnce(tlTextureCoordinates, point(tTexCoordX, tTexCoordY)) ]

      if tH = parent.plShaders[tV].count then
        tX2 = .5 * tMeshWidth + tMeshOffsetWidth
        -- vertex added to list after counting vertices for face (easier to keep count)
      end if

      if tV = parent.plShaders.count then
        tY2 = -(.5 * tMeshHeight - tMeshOffsetHeight)
        -- add vertex to list immediately (easier to keep count)
        tlLastVertices.add(vector (tX, tY2, tZ))
      end if

      if tH = parent.plShaders[tV].count and tV = parent.plShaders.count then
        -- add vertex to list immediately (easier to keep count)
        tlLastVertices.add(vector (tX2, tY2, tZ))
      end if

      tFace[#vertices] = [tlVertices.count, tlVertices.count + parent.plShaders[tV].count +1, tlVertices.count +1]
      tFace[#textureCoordinates] = [1,tlFaceTexCoords[3], tlFaceTexCoords[2]]
      tFace[#shader] = parent.plShaders[tV][tH][#shader]
      tlFaces.add(tFace.duplicate())

      tFace[#vertices] = [tlVertices.count +1, tlVertices.count + parent.plShaders[tV].count +1, tlVertices.count + parent.plShaders[tV].count +2]
      tFace[#textureCoordinates] = [tlFaceTexCoords[2],tlFaceTexCoords[3], tlFaceTexCoords[4]]
      tFace[#shader] = parent.plShaders[tV][tH][#shader]
      tlFaces.add(tFace.duplicate())

      if tH = parent.plShaders[tV].count then tlVertices.add(vector (tX2, tY, tZ))
    end repeat
  end repeat

  -- add last row of vertices
  repeat with tVertex in tlLastVertices
    tlVertices.add(tVertex)
  end repeat

  __buildMesh(me, tlFaces, tlVertices, tlTextureCoordinates)
end
