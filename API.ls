-- SPi-V API calls

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--


-- 
-- Internal API
--

on openURL aURL
  -- closes open scene and opens aURL in the engine
  sendSprite(spv(),#openURL,aURL)
end

on openXML aXML
  -- closes open scene and opens xml string in the engine
  sendSprite(spv(),#reset)
  sendSprite(spv(),#analyseXML,aXML) 
end

on getURL aURL, aTarget, aRelativeURL
  -- opens aURL in external browser window/frame aTarget
  if voidP(aRelativeURL) then aRelativeURL = spv().pSourceURL
  
  if not voidP(aURL) then
    -- check if we can open this in our own window
    if not(aURL starts "#") then
      tFileExt = fileExt(aURL)
      if (["xml","spv","jpg","jpeg","png","tif","tiff"].getOne(tFileExt) = 0 or string(aTarget) <> "") and aTarget <> "_spv" then
        sendSprite(spv(),#activate)
        gotoNetPage(relativeURL(aURL, aRelativeURL),aTarget)
      else openURL(relativeURL(aURL, aRelativeURL))
    else openURL(aURL)
  end if
end

on doEvent aTarget, aEvent
  -- calls property setter of current existing object
  if not voidP(aEvent) then
    if spv().member.memberNum > 0 then
      if not voidP(spv().poTour) then 
        if not voidP(spv().poTour.poScene) then
          tlTargets = getObjects(aTarget)
          repeat with toTarget in tlTargets
            spv().poTour.queueAction(toTarget, aEvent)
          end repeat
        end if
      end if  
    end if
  end if
end



on addLog aMessage, aType
  -- alias of logMessage
  logMessage(aMessage, aType)
end

on logMessage aMessage, aType
  if spv().member.memberNum > 0 then
    if voidP(aType) then aType = #info
    if aMessage starts "=" then
      aMessage = value(chars(aMessage, 2, aMessage.length))
    end if
    if not voidP(spv().poLogger) then spv().poLogger.show(aMessage, symbol(aType))
  end if
end


on setProperty aTarget, aProperty, aValue, aFlags
  -- calls property setter of currently existing object(s)
  if not (voidP(aTarget) or voidP(aProperty)) then
    if stringP(aFlags) then
      tPersistent = aFlags contains "persistent"
    end if
    
    if spv().member.memberNum > 0 then
      if aValue starts "=" then
        tValue = value(chars(aValue, 2, aValue.length))
      else tValue = aValue
      
      tlTargets = getObjects(aTarget)

      repeat with tTarget = 1 to tlTargets.count
        toTarget = tlTargets[tTarget]
        toTarget.setProp(aProperty, tValue)
        if tPersistent and not voidP(toTarget[#node]) then
          toTarget.node.attributeValue[aProperty] = aValue
        end if
      end repeat
    end if
  end if
end


on getProperty aTarget, aProperty, aFlags
  -- calls property getter of currently existing object(s)
  
  if stringP(aFlags) then
    tPersistent = aFlags contains "persistent"
    tSilent = aFlags contains "silent"
  end if
  
  if spv().member.memberNum > 0 then
    if tSilent then tFlags = "silent"
    
    tlTargets = getObjects(aTarget, tFlags)
    if tlTargets.count >0 then
      toTarget = tlTargets[1] 
      -- TODO: add proper getter to objects
      --return toTarget.getProperty(aProperty)
      
      if tPersistent and not voidP(toTarget[#node]) then
        return toTarget.node.attributeValue[aProperty]
      else return toTarget[symbol("p" & aProperty)]
    else
      return VOID
    end if
  end if
end

on getObjects aTarget, aFlags
  if stringP(aFlags) then
    tSilent = aFlags contains "silent"
  end if
  
  if spv().member.memberNum > 0 then
    if(aTarget) = "_this" then
      return [spv().pThis]
    else
      if not voidP(spv().poTour) then
        if not voidP(spv().poTour.plNamedObjects) then
          tlTargets = []
          toTarget = spv().poTour.plNamedObjects[aTarget]
          if not voidP(toTarget) then tlTargets.add(toTarget)
          else if aTarget.length > 2 and not(aTarget starts "_spv.") and the last char of aTarget = "*" then
            aTarget = chars(aTarget, 1, aTarget.length-1)
            repeat with tObjectNr = 1 to spv().poTour.plNamedObjects.count
              if spv().poTour.plNamedObjects.getPropAt(tObjectNr) starts aTarget then tlTargets.add(spv().poTour.plNamedObjects.getAt(tObjectNr))
            end repeat
          end if
          if tlTargets.count>0  then
            return tlTargets
          else
            if voidP(tSilent) and not voidP(spv().poTour.poScene) and chars(aTarget,1,5) <> "_spv." then
              spv().poLogger.show("Could not find target '"& aTarget & "'.", #warning)
            end if
            
            return []
          end if
        end if
      end if
    end if
  end if
  return []
end

on destroy aTarget
  if spv().member.memberNum > 0 then
    if chars(aTarget,1,1)<>"_" or (aTarget = "_scene" or aTarget = "_tour") then
      tlTargets = getObjects(aTarget)
      repeat with toTarget in tlTargets 
        if not voidP(toTarget.pBuilt) then toTarget.destroy()
        else
          spv().poLogger.show("Can not destroy '"& aTarget & "', object is not built.", #warning)
        end if
      end repeat
    else
      spv().poLogger.show("Can not destroy '"& aTarget & "'.", #warning)
    end if
  end if    
end

on build aTarget, aRebuild
  if spv().member.memberNum > 0 then
    if chars(aTarget,1,1)<>"_" or (aTarget = "_scene" or aTarget = "_tour") then
      tlTargets = getObjects(aTarget)
      repeat with toTarget in tlTargets 
        if voidP(toTarget.pBuilt) then toTarget.initNode()
        else
          if voidP(aRebuild) then
            spv().poLogger.show("Can not build '"& aTarget & "', object is already built.", #warning)
          else
            toTarget.destroy()
            toTarget.initNode()
          end if
        end if
      end repeat
    else
      spv().poLogger.show("Can not build '"& aTarget & "'.", #warning)
    end if
  end if    
end

on setView aSceneID, aPan, aTilt, aFOV
  if spv().member.memberNum > 0 then
    tPan = myfloat(aPan)
    tTilt = myfloat(aTilt)
    tFOV = myfloat(aFOV)
    
    tSceneID = void
    if not voidP(spv().poTour) then
      if not voidP(spv().poTour.poScene) then
        tSceneID = spv().poTour.poScene.pID
      end if
    end if
    
    
    if voidP(aSceneID) or aSceneID = "" or aSceneID = tSceneID then
      -- simply set view
      if floatP(tFOV)  then spv().poNavigator.setProp("fov",  tFOV)      
      if floatP(tPan)  then spv().poNavigator.setProp("pan",  tPan)
      if floatP(tTilt) then spv().poNavigator.setProp("tilt", tTilt)
      
    else
      -- find node to switch to
      repeat with tSceneNode in spv().poTour.plSceneNodes
        if tSceneNode.attributeValue["id"] = aSceneID then
          tNewSceneNode = tSceneNode
          exit repeat
        end if
      end repeat
      
      if voidP(tNewSceneNode) then spv().poLogger.show("Scene '"& aSceneID &"' not defined in XML file.", #warning)
      else
        spv().poTour.poScene.openScene(tNewSceneNode, tPan, tTilt, tFOV)
      end if
    end if
  end if
end


--
-- Standalone viewer API
--

on setWindow aState
  if window("stage")._movie.member(1).name = "spv.launch.movie" then
    window("stage")._movie.setWindow(aState)
  end if
end

on exitWindow aCheckExitLock
  if window("stage")._movie.member(1).name = "spv.launch.movie" then
    tell window("stage") 
      if not (aCheckExitLock = true and the exitLock = true) then quit
    end tell
  end if  
end

