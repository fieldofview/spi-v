-- <panogroup> node

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt
property pReady

property pID

property pElementType

property pGroup
property poParent
property pParentGroup

property plBehaviorObjects

property pPan, pTilt, pRoll

property pZorder

property pZBaseName

property pVisible, pEnabled

property pMouseHoverMe, pMousePressMe

on new me,  _parent, aoNode
  pElementType = #pano
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []

    -- remove group and subgroups
    repeat with tLevel = 0 to 9
      spv().pWorld.deleteGroup(pZBaseName & string(tLevel))
    end repeat
    spv().pWorld.deleteGroup("_png_" & pID)
  end if
end


on initNode me
  if voidP(pBuilt) then
    pBuilt = true
    pReady = false

    pID = getValue(#string, node.attributeValue["id"])

    tParentNode = node.parent
    if tParentNode = parent.node or tParentNode = parent.parent.poGlobalNode then
      poParent = #root
    else poParent = parent.plPanoGroups[tParentNode.attributeValue["id"]][#object]

    me.getBehaviors()

    -- get parameters from node
    pPan  = getValue(#float, node.attributeValue["pan"], 0)
    pTilt = getValue(#float, node.attributeValue["tilt"], 0)
    pRoll = getValue(#float, node.attributeValue["roll"], 0)

    pVisible = getValue(#boolean, node.attributeValue["visible"], true)

    pGroup = spv().pWorld.newGroup("_png_" & pID)
    pZBaseName = "_pnz_" & pID & "_#"
    repeat with tLevel = 0 to 9
      tGroup = spv().pWorld.newGroup(pZBaseName & string(tLevel))
      tGroup.parent = pGroup
    end repeat

    me.updateTransform()

    pZorder = node.attributeValue["zorder"]
    if pZorder <> "_spv.preview" then pZorder = string(max(0,min(9, getValue(#integer, pZorder, 0))))

    if poParent = #root then
      pParentGroup = spv().pWorld.group("_zScene#"&pZorder)
    else
      if not integerP(value(pZorder)) then pZorder = "0"
      pParentGroup = spv().pWorld.group(poParent.pZBaseName & string(pZorder))
    end if

    if pVisible = true then pGroup.parent = pParentGroup
    else pGroup.parent = spv().pVoidGroup

    pGroup.AddToWorld()
  end if
end

on updateTransform me
  if not voidP(pGroup) then
    pGroup.transform.rotation = vector(pTilt, -pPan, 0)
    pGroup.rotate(vector(0,0, -pRoll), #self)
  end if
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end


--
-- event handling
--

on readyEvent me
  __readyEvent(me, #group)
end

on mouseEnterEvent me
  __mouseEnterEvent(me, #group)
end

on mouseLeaveEvent me
  __mouseLeaveEvent(me, #group)
end

on mouseDownEvent me
  __mouseDownEvent(me, #group)
end

on mouseUpEvent me
  __mouseUpEvent(me, #group)
end


--
-- public properties
--

on setProp me, aProperty, aValue
  case aProperty of
    "visible", "enabled":
      __setPropGroup (me, symbol("p" & aProperty), aValue)

    "pan", "tilt", "roll":
      __setPropPano (me, aProperty, aValue)

    "zorder":
      __setPropZorder (me, aValue, pGroup, "_zScene#")
  end case
end