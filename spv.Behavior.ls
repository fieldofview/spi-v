-- <behavior> node
-- Manages <action> nodes

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pID
property pEnabled

property pBuilt

property plActions

property pRelativeURL

on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    spv().poTour.removeNamedObject(me)
    pBuilt = void
  end if
end


on initNode me
  if voidP(pBuilt) then
    pBuilt = true
    
    pID = getValue(#string, node.attributeValue["id"], "")
    
    pRelativeURL = node.attributeValue["relativeurl"]
    if [void, "","@","#"].getPos(pRelativeURL) > 0 then pRelativeURL = spv().pSourceURL
    
    pEnabled = getValue(#boolean, node.attributeValue["enabled"], true)
    
    plActions = [:]
    
    tlActionNodes = node.getElementByName("action")
    repeat with toActionNode in tlActionNodes
      tEvent = toActionNode.attributeValue["event"]
      if not voidP(tEvent) then
        tlArguments = toActionNode.attributeValue.duplicate()
        
        tType = forceLowerCase(tlArguments["type"])
        if ["logmessage","addlog","setproperty","geturl"].getOne(tType) > 0 then
          case tType of
            "setproperty":
              tArgument = "value"
            "geturl":
              tArgument = "url"
            otherwise:
              tArgument = "message"
          end case
          tValue = tlArguments[tArgument]
          
          if stringP(tValue) then
            if tValue starts "=" then
              if not validFunctions(chars(tValue, 2, tValue.length)) then
                tlArguments[tArgument] = ""
                spv().poLogger.show("Behavior '"& pID &"', event '"&tEvent&"' contains illegal function calls.", #warning)
              end if
            end if
          end if
        end if
        
        if voidP(plActions[tEvent]) then plActions[tEvent] = []
        plActions[tEvent].add(tlArguments)
      end if
    end repeat
  end if
end

on doAction me, aEvent, aoObject
  if pEnabled then
    tlActions = plActions[aEvent]
    if not voidP(tlActions) then
      
      repeat with tlAction in tlActions
        spv().pThis = aoObject -- pThis declaration must be inside the repeat loop, for reentry
        case tlAction["type"] of
          "setProperty":
            setProperty(varOrValue(tlAction["target"]), \
                        varOrValue(tlAction["property"]), \
                        varOrValue(tlAction["value"]), \
                        varOrValue(tlAction["flags"]))
            
          "getURL":
            getURL(varOrValue(tlAction["url"]), \
                   varOrValue(tlAction["target"]), \
                   pRelativeURL)
            
          "setView":
            setView(varOrValue(tlAction["scene"]), \
                    varOrValue(tlAction["pan"]), \
                    varOrValue(tlAction["tilt"]), \
                    varOrValue(tlAction["fov"]))
            
          "setWindow":
            setWindow(symbol(varOrValue(tlAction["state"])))
          "exitWindow":
            exitWindow()
            
          "logMessage", "addLog":
            addLog(varOrValue(tlAction["message"]),varOrValue(tlAction["importance"]))
            
          "doEvent":
            doEvent(varOrValue(tlAction["target"]),varOrValue(tlAction["action"]))
        end case
        spv().pThis = void
      end repeat
      
    end if
  end if
end


--
-- public calls
--

on setProp me, aProperty, aValue
  case aProperty of
    "enabled":
      pEnabled = getValue(#boolean, aValue, not pEnabled)
  end case
end
