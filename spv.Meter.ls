-- <meter> node

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pID

property plBehaviorObjects

property pActive

property pLayerClass

property pDistribution
property pMaxStep
property pStepIncrease

property pSpots

property pPointerWeight

property pDefaultValue
property pMinValue, pMaxValue

property pValue
property pStep

property plSpots

property plMeterPanoObjects, plMeterUIObjects

property pBuilt

on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on enterFrame me
  me.updateMeter()
end


on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []

    if pActive then spv().scriptInstanceList.deleteOne(me)
  end if
end


on initNode me
  if voidP(pBuilt) then
    pBuilt = true

    pID = node.attributeValue["id"]

    me.getBehaviors()

    pActive = getValue(#boolean, node.attributeValue["active"], true)
    if pActive = true then spv().scriptInstanceList.add(me)

    pMinValue = getValue(#float, node.attributeValue["minvalue"], 0)
    pMaxValue = getValue(#float, node.attributeValue["maxvalue"], 100)

    pDefaultValue = getValue(#float, node.attributeValue["defaultvalue"], (pMinValue + pMaxValue)/2.0)
    pDefaultValue = min(max(pMaxValue, pMinValue), max(min(pMaxValue, pMinValue), pDefaultValue))

    if pMaxValue<>pMinValue then
      pDefaultValue = myfloat(pDefaultValue - pMinValue) / (pMaxValue - pMinValue)
    else pDefaultValue = 0

    pValue = getValue(#float, node.attributeValue["value"], pMinValue + pDefaultValue * (pMaxValue - pMinValue))

    pMaxStep = abs(getValue(#float, node.attributeValue["maxstep"], void))
    pStepIncrease = abs(getValue(#float, node.attributeValue["stepincrease"], (pMaxValue-pMinValue)/1000.0))

    pPointerWeight = max(0,min(1, getValue(#float, node.attributeValue["pointerweight"], 0)))

    pSpots = getValue(#integer, node.attributeValue["spots"], 1)
    me.updateSpots()

    pDistribution = max(0, min(1, getValue(#float, node.attributeValue["distribution"], .5)))

    pLayerClass = getValue(#string, node.attributeValue["class"], "meter")

    plMeterPanoObjects = []
    plMeterUIObjects = []

    me.updateObjects()
  end if
end

on updateMeter me
  if pActive then
    tMeteredValue = 0

    tlMeterMesh = []
    repeat with toMeterObject in plMeterUIObjects
      if not voidP(toMeterObject.pModel) then
        tlMeterMesh.add(toMeterObject.pModel)
        tMeterUI = true
      end if
    end repeat
    repeat with toMeterObject in plMeterPanoObjects
      if not voidP(toMeterObject.pModel) then
        tlMeterMesh.add(toMeterObject.pModel)
        tMeterPano = true
      end if
    end repeat
    -- TODO: add ui elements properly
    if tlMeterMesh.count > 0 then
      tMeterCenter = point(spv().rect.width, spv().rect.height)/2.0

      tSceneRect = spv().pSceneCamera.rect
      tMouseInSprite = spv().poMouseManager.pMouseLoc
      tMouseInsideScene = tMouseInSprite.inside(spv().rect)
      if tMouseInsideScene and pPointerWeight > 0 then
        tMeterFromCenter = tMouseInSprite - \
          point(tSceneRect.left, tSceneRect.top) - \
          point(tSceneRect.width, tSceneRect.height)/2.0
        tMeterCenter = tMeterCenter + pPointerWeight * tMeterFromCenter
      end if

      tOverlayOffset = point(tSceneRect.left, tSceneRect.top)

      repeat with tSpot in plSpots
        tSpotValue = VOID

        tSpotLoc =  tMeterCenter * (1 + tSpot * pDistribution)
        if spv().pSWV >= 10 then
          tlPickData = []
          if not voidP(tMeterUI) then
            tlPickData = spv().pOverlayCamera.modelsUnderLoc(tSpotLoc+tOverlayOffset, [#levelOfDetail: #detailed, #modelList:tlMeterMesh])
          end if
          if not voidP(tMeterPano) then
            tlPickData = appendList(tlPickData, spv().pSceneCamera.modelsUnderLoc(tSpotLoc, [#levelOfDetail: #detailed, #modelList:tlMeterMesh]))
          end if
        else
          -- todo: cleanup shockwave 8.5 picking
          tlPutBack = []
          repeat with tMeterMesh in tlMeterMesh
            if tMeterMesh.parent = spv().pVoidGroup then
              tMeterMesh.parent = spv().pSceneCamera.rootNode
              tlPutBack.add(tMeterMesh)
            end if
          end repeat

          tlPickData = spv().pOverlayCamera.modelsUnderLoc(tSpotLoc+tOverlayOffset, 100, #detailed)
          tlPickData = appendList(tlPickData, spv().pSceneCamera.modelsUnderLoc(tSpotLoc, 100, #detailed))

          repeat with tPick = tlPickData.count down to 1
            if tlMeterMesh.getOne(tlPickData[tPick][#model]) = 0 then
              tlPickData.deleteAt(tPick)
            end if
          end repeat

          repeat with tPutBack in tlPutBack
            tPutBack.parent = spv().pVoidGroup
          end repeat
        end if

        tSpotValue = VOID

        if tlPickData.count > 0 then
          tPickData = tlPickData[1]

          tModel = tPickData.model
          tMeshID = tPickData.meshID

          toPickedObject = VOID
          if tMeterUI then
            repeat with toMeterObject in plMeterUIObjects
              if toMeterObject.pModel = tModel then
                toPickedObject = toMeterObject
                exit repeat
              end if
            end repeat
          end if
          if voidP(toPickedObject) and tMeterPano then
            repeat with toMeterObject in plMeterPanoObjects
              if toMeterObject.pModel = tModel then
                toPickedObject = toMeterObject
                exit repeat
              end if
            end repeat
          end if

          if not voidP(toPickedObject) then
            -- account for multiple submeshes in model
            tPickData[#faceID] = tPickData[#faceID] + toPickedObject.plMeshFaceCounts[tMeshID]

            tPickedImage = toPickedObject.poImageElement.plImageLayers[pLayerClass][#object].pImage

            if not voidP(tPickedImage) then
              tShadersH = toPickedObject.plShaders[1].count
              tH = ((tMeshID-1) mod (tShadersH)) + 1
              tV = integer((tMeshID-1) / float(tShadersH) -.499) + 1

              tPickedShader = toPickedObject.plShaders[tV][tH].shader
              tUV = mapPointToTexture(tPickData)


              -- if model has has a visible layer, find exact pick point in texture chunk
              tlImageLayers = toPickedObject.poImageElement.plImageLayers
              if not voidP(max(tlImageLayers["base"], tlImageLayers["hover"], tlImageLayers["press"])) then
                repeat with tlShaderV in toPickedObject.plShaders
                  repeat with tShader in tlShaderV
                    if tShader.shader.name = tPickedShader.name then
                      tShaderSize = tShader.size
                      tImageSubRect = tShader.rect
                      exit repeat
                    end if
                  end repeat
                end repeat
              else
                tImageSubRect = tPickedImage.rect
                tShaderSize = point(tImageSubRect.width, tImageSubRect.height)
              end if

              tPointInRect = point ( \
                (tUV.locH * tShaderSize.locH) + tImageSubRect.left, \
                (tUV.locV * tShaderSize.locV) + tImageSubRect.top )

              tSpotValue = colorToValue(tPickedImage.getPixel(tPointInRect))
            end if
          end if
        end if
        if voidP(tSpotValue) then tSpotValue = pDefaultValue

        tMeteredValue = tMeteredValue + tSpotValue
      end repeat

      -- average spot values
      tMeteredValue = tMeteredValue / plSpots.count
    else
      -- can not meter
      tMeteredValue = pDefaultValue
    end if

    tValue = pMinValue + tMeteredValue * (pMaxValue - pMinValue)

    if pStepIncrease > 0 or pMaxStep > 0 then
      if pStepIncrease > 0 then
        if sign(pStep) <> sign(tValue - pValue) then
          if tValue<pValue then pStep = pStep - pStepIncrease
          else pStep = pStep + pStepIncrease
        end if

        tDistReq = pStep*pStep/(2*pStepIncrease)
        if abs(tValue-pValue) > tDistReq then
          tIncDir = 1
        else tIncDir = -1

        if tValue<pValue then pStep = pStep -(pStepIncrease * tIncDir)
        else pStep = pStep + (pStepIncrease * tIncDir)
      else
        pStep = tValue - pValue
      end if

      if pMaxStep>0 then pStep = max(-pMaxStep, min(pMaxStep, pStep))

      if abs(pStep)>abs(tValue-pValue) then pStep = tValue - pValue

      if pMinValue < pMaxValue then
        tValue = max(pMinValue,min(pMaxValue, pValue + pStep))
      else tValue = min(pMinValue,max(pMaxValue, pValue + pStep))
    end if

    if tValue <> pValue then
      pValue = tValue
      spv().poTour.queueAction(me, "change")
    else
      pStep = 0
    end if
  end if

end

on updateValue me, aValue
  tValue = pValue
  if not voidP(aValue) then pValue = aValue
  pValue = min(max(pMaxValue, pMinValue), max(min(pMaxValue, pMinValue), pValue))
  if pValue<>tValue and pActive = true then
    spv().poTour.queueAction(me, "change")
  end if

  pDefaultValue = min(max(pMaxValue, pMinValue), max(min(pMaxValue, pMinValue), pDefaultValue))
  if pMaxValue<>pMinValue then
    pDefaultValue = myfloat(pDefaultValue - pMinValue) / (pMaxValue - pMinValue)
  else pDefaultValue = 0
end

on updateSpots me
  case pSpots of
    2:
      plSpots = [point(-1,0), point(1,0)]
    3:
      plSpots = [point(-1,0), point(0,0), point(1,0)]
    4:
      plSpots = [point(-1,0), point(0,1), point(1,0), point(0,-1)]
    5:
      plSpots = [point(0,0), point(-1,0), point(0,1), point(1,0), point(0,-1)]
    6:
      plSpots = [point(-1,0), point(-.333,0), point(.333,0), point(1,0), \
                 point(0,.5), point(0,-.5) ]
    9:
      plSpots = [point(-1, 1), point(0, 1), point(1, 1), \
                 point(-1, 0), point(0, 0), point(1, 0), \
                 point(-1,-1), point(0,-1), point(1,-1) ]
    otherwise: -- also 1
      plSpots = [point(0,0)]
  end case
  plSpots = plSpots /2.0
end

on updateObjects me
  repeat with toImage in parent.plImages
    if not voidP (toImage.plImageLayers[pLayerClass]) then
      repeat with toElement in toImage.plInterestedElements
        if toElement.node.name = "panoelement" then
          plMeterPanoObjects.add(toElement)
        else
          plMeterUIObjects.add(toElement)
        end if
      end repeat
    end if
  end repeat
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end

--
-- public calls
--


on setProp me, aProperty, aValue
  case aProperty of
    "active":
      tActive = getValue(#boolean, aValue, not pActive)

      if pActive <> tActive then
        pActive = tActive
        if pActive = true then
          spv().scriptInstanceList.add(me)
        else
          spv().scriptInstanceList.deleteOne(me)
        end if
      end if
    "class":
      if aValue<>"" then
        pClass = aValue
        me.updateObjects()
      end if
    "minvalue":
      pDefaultValue = pDefaultValue * (pMaxValue - pMinValue) + pMinValue
      pMinValue = getValue(#float, aValue, pMinValue)
      me.updateValue()
    "maxvalue":
      pDefaultValue = pDefaultValue * (pMaxValue - pMinValue) + pMinValue
      pMaxValue = getValue(#float, aValue, pMaxValue)
      me.updateValue()
    "defaultvalue":
      pDefaultValue = getValue(#float, aValue, pDefaultValue)
      me.updateValue()
    "value":
      tValue = getValue(#float, aValue, pValue)
      me.updateValue(tValue)
    "spots":
      pSpots = getValue(#integer, aValue, pSpots)
      me.updateSpots()
    "distribution":
      pDistribution = max(0, min(1, getValue(#float, aValue, pDistribution)))
    "pointerweight":
      pPointerWeight = max(0,min(1, getValue(#float, aValue, pPointerWeight)))
    "maxstep":
      tValue = (getValue(#float, aValue, void))
      if tValue>0 or voidP(tValue) then pMaxStep = tValue
    "stepincrease":
      pStepIncrease = max(0,(getValue(#float, aValue, pStepIncrease)))
  end case
end

