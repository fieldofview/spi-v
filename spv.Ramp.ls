-- <ramp> node

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt

property pID

property plBehaviorObjects

property pValue1, pValue2

property pMinValue, pMaxValue

property pDuration, pLoop, pPingpong

property pActive
property pDirection
property pValue, pActualValue

property pMaxStep
property pStepIncrease

property pInterrupt

property pLastClick,pLastRoll, pLastKey

property pStep

property pLastTime

property pLoopsLeft

on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on enterFrame me
  if not spv().pError then me.updateRamp()
end


on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []

    if pActive then spv().scriptInstanceList.deleteOne(me)
  end if
end



on initNode me
  if voidP(pBuilt) then
    pBuilt = true

    pID = node.attributeValue["id"]

    me.getBehaviors()

    pActive = getValue(#boolean, node.attributeValue["active"], false)

    pDirection = getValue(#integer, node.attributeValue["direction"], 1)
    if pDirection >= 0 then pDirection = 1
    else pDirection = -1

    pLoop = node.attributeValue["loop"]
    if pLoop <> "true" then pLoop = max(0,getValue(#integer, pLoop, 1))
    pPingPong = getValue(#boolean, node.attributeValue["pingpong"], false)

    pDuration = 60 * max(0, getValue(#float, node.attributeValue["duration"], 0))
    pMaxStep = abs(getValue(#float, node.attributeValue["maxstep"], VOID))
    pStepIncrease = abs(getValue(#float, node.attributeValue["stepincrease"], VOID))
    pValue1 = getValue(#float, node.attributeValue["value1"], 0.0)
    pValue2 = getValue(#float, node.attributeValue["value2"], pValue1+1)

    pActualValue = min(max(getValue(#float, node.attributeValue["value"], pValue1),\
      min(pValue1, pValue2)), max(pValue1, pValue2))

    pMinValue = getValue(#float, node.attributeValue["minvalue"], void)
    pMaxValue = getValue(#float, node.attributeValue["maxvalue"], void)
    if voidP(pMinValue) and not voidP(pMaxValue) then pMinValue = min(pActualValue, pValue1, pValue2)
    else if not voidP(pMinValue) and voidP(pMaxValue) then pMaxValue = max(pActualValue, pValue1, pValue2)

    me.clipValue()

    if pMaxStep=0 and pStepIncrease=0 and pDuration = 0 then pDuration = 60

    tValue = node.attributeValue["interrupt"]
    if not voidP(tValue) then
      if tValue = "all" then
        pInterrupt = ["press","hover","key"]
      else
        pInterrupt = []
        the itemDelimiter = ","
        repeat with tItemnr = 1 to tValue.item.count
          tItem = tValue.item[tItemnr]
          if ["press","hover","key"].getOne(tItem) > 0 then addOnce(pInterrupt, tItem)
        end repeat
        if pInterrupt.count = 0 then pInterrupt = void
      end if
      pLastClick = spv().pLastClick
      pLastRoll = spv().pLastRoll
      pLastKey = spv().pLastKey
    end if


    if pActive then
      me.startRamp()
    end if
  end if
end

on startRamp me
  spv().scriptInstanceList.add(me)

  pLastTime = the timer

  if voidP(pLoopsLeft) then pLoopsLeft = integer(pLoop)

  pLastClick = spv().pLastClick
  pLastRoll = spv().pLastRoll
  pLastKey = spv().pLastKey

  pStep = 0

  spv().poTour.queueAction(me, "start")
end

on updateRamp me
  tInterrupted = false
  if not voidP(pInterrupt) then
    if (pInterrupt.getOne("hover") > 0 and spv().pLastRoll <= pLastRoll)  or \
       (pInterrupt.getOne("press") > 0 and spv().pLastClick<= pLastClick) or \
       (pInterrupt.getOne("key")   > 0 and spv().pLastKey  <= pLastKey) then
      tInterrupted = true
    end if
  end if

  pLastClick = spv().pLastClick
  pLastRoll = spv().pLastRoll
  pLastKey = spv().pLastKey

  tDoChange = false
  tResetValue = false

  if pActive then
    if tInterrupted then
      spv().poTour.queueAction(me, "interrupt")
      if pDirection > 0 then pActualValue = pValue1
      else pActualValue = pValue2
      pLastTime = the timer
    else


      tValue1 = pValue1
      tValue2 = pValue2
      if not voidP(pMaxValue) then
        tClipSpan = pMaxValue - pMinValue
        --        tSpans = integer((float(pActualValue - pMinValue)/tClipSpan)-.4999)
        --
        --        tValue1 = pValue1+tClipSpan*tSpans
        --        tValue2 = pValue2+tClipSpan*tSpans

        if pDirection > 0 then
          tDistance = tValue2 - pActualValue
          if abs(tDistance - tClipSpan) < abs(tDistance) then
            tValue2 = tValue2 - tClipSpan
          else if abs(tDistance + tClipSpan) < abs(tDistance) then
            tValue2 = tValue2 + tClipSpan
          end if
        else
          tDistance = tValue1 - pActualValue
          if abs(tDistance - tClipSpan) < abs(tDistance) then
            tValue1 = tValue1 - tClipSpan
          else if abs(tDistance + tClipSpan) < abs(tDistance) then
            tValue1 = tValue1 + tClipSpan
          end if
        end if
      end if

      if pDirection > 0 then
        tTarget = tValue2
        tSpan = tValue2 - tValue1
      else
        tTarget = tValue1
        tSpan = tValue1 - tValue2
      end if

      if (tSpan > 0 and pActualValue < tTarget) or (tSpan < 0 and pActualValue > tTarget) then
        if pDuration > 0 then
          tPassedTime = the timer - pLastTime
          tStep = tSpan * float(tPassedTime) / pDuration
          pLastTime = the timer
          tFixedStep = true
        else tStep = tTarget - pActualValue

        if pStepIncrease>0 then
          -- dampen step

          tStepInc = pStepIncrease
          if spv().pFrameFactor > 1 then tStepInc = tStepInc * spv().pFrameFactor

          tDistReq = pStep*pStep/(2*tStepInc)
          if spv().pFrameFactor > 1 then tDistReq = tDistReq * spv().pFrameFactor

          if abs(tTarget-pActualValue) > tDistReq then
            tIncDir = 1
          else tIncDir = -1

          tNewStep = pStep + sign(tSpan) * tStepInc * tIncDir
          if tSpan > 0 then
            tStep = min(tStep, tNewStep)
          else tStep = max(tStep, tNewStep)
        end if

        if pMaxStep>0 then
          tMaxStep = pMaxStep
          if spv().pFrameFactor > 1 then tMaxStep = tMaxStep * spv().pFrameFactor

          tStep = max(-tMaxStep, min(tMaxStep, tStep))
        end if

        pStep = tStep

        if voidP(tFixedStep) and spv().pFrameFactor > 1 then tStep = tStep * spv().pFrameFactor

        tActualValue = pActualValue + tStep
        if tActualValue <> tTarget then
          if sign(tActualValue - tTarget)<>sign(pActualValue - tTarget) then
            if pDirection > 0 then
              tActualValue = tValue2
            else tActualValue = tValue1
          end if
        end if
        pActualValue = tActualValue

        me.clipValue()
        spv().poTour.queueAction(me, "change")
      else
        if pActualValue <> tTarget then
          pActualValue = tTarget
          me.clipValue()
          spv().poTour.queueAction(me, "change")
        end if

        if pPingPong = true then
          pDirection = -pDirection
        else
          if pLoopsLeft>1 or [0,"true"].getOne(pLoop) then
            if pDirection > 0 then
              pActualValue = pValue1
            else pActualValue = pValue2
          end if
        end if
        me.clipValue()

        if pLoopsLeft>1 or [0,"true"].getOne(pLoop) > 0 then
          pLoopsLeft = pLoopsLeft -1
          spv().poTour.queueAction(me, "loop")
          --spv().poTour.queueAction(me, "change")
          pStartTime = the timer
        else
          pLoopsLeft = void
          pActive = false
          spv().poTour.queueAction(me, "stop")
          spv().scriptInstanceList.deleteOne(me)
        end if
      end if
    end if
  end if
end

on clipValue me
  if voidP(pMaxValue) then
    pValue = pActualValue
  else
    pValue = clip(pActualValue, pMinValue, pMaxValue)
  end if
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end

--
-- public calls
--


on setProp me, aProperty, aValue
  case aProperty of
    "active":
      tActive = getValue(#boolean, aValue, not pActive)

      if tActive <> pActive then
        pActive = tActive
        if pActive then me.startRamp()
        else
          --spv().poTour.queueAction(me, "stop")
          pLoopsLeft = void
          spv().scriptInstanceList.deleteOne(me)
        end if

      else if tActive then
        if pPingPong then
          pDirection = -pDirection
          me.startRamp()
        end if
      end if

    "loop":
      if aValue = "true" then pLoop = "true"
      else pLoop = max(0,getValue(#integer, pLoop, pLoop))
    "pingpong":
      pPingPong = getValue(#boolean, aValue, not pPingPong)
    "duration":
      pDuration = 60 * max(0,getValue(#float, aValue, 0))
    "direction":
      aValue = sign(getValue(#integer, aValue, 1))
      if aValue <> 0 then pDirection = aValue
    "value1":
      aValue = getValue(#float, aValue, void)
      if floatP(aValue) then pValue1 = aValue
    "value2":
      aValue = getValue(#float, aValue, void)
      if floatP(aValue) then pValue2 = aValue
    "value":
      aValue = getValue(#float, aValue, void)
      if floatP(aValue) then
        pActualValue = min(max(aValue, min(pValue1, pValue2)), max(pValue1, pValue2))
        me.clipValue()
      end if
    "minvalue":
      aValue = getValue(#float, aValue, void)
      if floatP(aValue) then
        pMinValue = aValue
        pMaxValue = max(pValue, pValue1, pValue2)
      else
        pMinValue = void
        pMaxValue = void
      end if
      me.clipValue()
    "maxvalue":
      aValue = getValue(#float, aValue, void)
      if floatP(aValue) then
        pMaxValue = aValue
        pMinValue = min(pValue, pValue1, pValue2)
      else
        pMinValue = void
        pMaxValue = void
      end if
      me.clipValue()
    "maxstep":
      tValue = (getValue(#float, aValue, void))
      if tValue>0 or voidP(tValue) then pMaxStep = tValue
    "stepincrease":
      pStepIncrease = max(0,(getValue(#float, aValue, pStepIncrease)))
    "interrupt":
      if aValue<>"" then
        if tValue = "all" then
          pInterrupt = ["press","hover","key"]
        else
          pInterrupt = []
          the itemDelimiter = ","
          repeat with tItemnr = 1 to tValue.item.count
            tItem = tValue.item[tItemnr]
            if ["press","hover","key"].getOne(tItem) > 0 then addOnce(pInterrupt, tItem)
          end repeat
          if pInterrupt.count = 0 then pInterrupt = void
        end if
        pLastClick = spv().pLastClick
        pLastRoll = spv().pLastRoll
        pLastKey = spv().pLastKey
      else pInterrupt = void
  end case
end

