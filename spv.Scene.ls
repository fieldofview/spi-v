-- <scene> node
-- Instantiates/manages these nodes:
--  <panoelement>
--  <uielement>
--  etc

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt

property pID

property plBehaviorObjects

property plPanoElements, plPanoGroups
property plUIElements, plUIGroups

property plSounds

property plRamps, plMeters, plVariables, plLists

property plImages
property plBehaviors

property pTitle, pDescription, pCopyright

property pBuilding
property pReadySent

property pScaleTextures

on new me,  _parent
  parent = _parent
  parent.addNamedObject(me, "_scene")
  
  plImages = [:]
  
  plPanoGroups = [:]
  plPanoElements = [:]
  plUIGroups = [:]
  plUIElements = [:]
  
  plSounds = [:]
  
  plVariables = [:]
  plRamps = [:]
  plMeters = [:]
  plLists = [:]
  
  plBehaviors = [:]
  
  plBehaviorObjects = []
  
  return me
end

on destroy me
  if not voidP(pBuilt) then
    killTimeout("sceneready")
    
    spv().poTour.queueAction(me, "exit",true)
    spv().poLogger.show("Exit scene", #info)
    
    pBuilt = void
    
    repeat with tlObjectList in [\
      plPanoElements, plPanoGroups, \
      plUIElements, plUIGroups, \
      plImages, plSounds, \
      plRamps, plMeters, plLists, \
      plVariables, plBehaviors ]
      
      me.destroyObjects(tlObjectList)
    end repeat
    
    parent.removeNamedObject(me)
  end if
end

on initNode me
  if voidP(pBuilt) then
    me.openScene(node)
  end if
end

on openScene me, aoNode, aPan, aTilt, aFOV
  pBuilt = void
  killTimeout("sceneready")
  
  setProperty("_spv.preview.bg","visible","true")
  
  if not voidP(node) then
    spv().poTour.queueAction(me, "exit", true)
    spv().poLogger.show("Exit scene", #info)
    
    --
    -- remove elements from previous scene
    --
    repeat with tlObjectList in [\
      plPanoElements, plPanoGroups, \
      plUIElements, plUIGroups, \
      plImages, plSounds, \
      plRamps, plMeters, plLists, \
      plVariables, plBehaviors ]
      
      me.destroyObjects(tlObjectList, #scene)
    end repeat
    
    --
    -- reset scaled image nodes of remaining elements
    --
    me.scaleTextures()
  end if
  
  node = aoNode
  pID = node.attributeValue["id"]
  
  pReadySent = void
  
  
  --
  -- Check scene integrity
  --
  tlIDs = []
  tlIDnodes = node.getAttributeByName("id")
  repeat with tIDnode in tlIDNodes
    tID = tIDnode.attributeValue["id"]
    if not voidP(tID) then
      if tlIDs.getOne(tID)=0 then tlIDs.add(tID)
      else spv().poLogger.show("Duplicate ID '"& tID &"' in scene '"& pID &"'.", #error)
    end if
  end repeat
  
  -- 
  -- Update _scenes_* lists
  --
  setProperty("_scenes_id","index",parent.plSceneNodes.getPos(node))
  
  --
  -- Get title, description and copyright info
  --
  toMeta = node["meta"]
  repeat with tlValue in [[#pTitle, "title"],[#pDescription, "description"],[#pCopyright, "copyright"]]
    if not voidP(toMeta[tlValue[2]]) then me[tlValue[1]] = toMeta[tlValue[2]].getText()
    else me[tlValue[1]] = ""
  end repeat
  
  --
  -- set navigator
  --
  spv().poNavigator.setCameraValues()
  
  toDefaultView = toMeta["defaultview"]
  
  repeat with tlParam in [[aPan,"pan"], [aTilt, "tilt"], [aFov, "fov"]]
    tVal = myfloat(tlParam[1])
    if not floatP(tVal) and not voidP(toDefaultView) then
      tVal = myfloat(toDefaultView.attributeValue[tlParam[2]])
    end if
    
    if floatP(tVal) then spv().poNavigator.setProp(tlParam[2], tVal)
  end repeat
  
  pBuilding = true
  
  if voidP(spv().pWindow) then
    -- clutch to fix screen update in LDM
    tell the activeWindow to updateStage  
  end if
  
  --
  -- build scene
  --
  
  -- build behaviors
  me.buildObjects (plBehaviors,  "behavior",  "spv.Behavior","_bvr")
  plBehaviorObjects = me.getBehaviorObjects(node)
  
  -- reattach behaviors for global nodes
  repeat with tlObjectList in [\
    plPanoElements, plPanoGroups, \
    plUIElements, plUIGroups, \
    plRamps, plMeters]
    
    repeat with tlObject in tlObjectList
      if not voidP(tlObject[#object]) then tlObject[#object].getBehaviors()
    end repeat
  end repeat
  
  -- build UI
  me.buildObjects (plUIGroups,   "uigroup",   "spv.UIGroup", "_uig")
  me.buildObjects (plUIElements, "uielement", "spv.UIElement",  "_uie")
  
  -- build Pano
  me.buildObjects (plPanoGroups,   "panogroup",   "spv.PanoGroup", "_png")
  me.buildObjects (plPanoElements, "panoelement", "spv.PanoElement",  "_pne")
  
  -- build Sound
  me.buildObjects (plSounds, "sound", "spv.Sound", "_snd")
  
  -- Other elements (ramps, meters, etc)
  me.buildObjects (plVariables, "variable", "spv.Variable", "_var")
  me.buildObjects (plRamps, "ramp", "spv.Ramp", "_sig")
  me.buildObjects (plMeters,  "meter",  "spv.Meter",  "_mtr")
  me.buildObjects (plLists, "list", "spv.List", "_lst")
  
  pBuilding = void
  me.elementReady()
  
  pBuilt = true
  
  -- scene event: done setting up scene
  spv().poTour.queueAction(me, "enter")
end

on elementReady me, aID, aElementType
  -- called from panoelement and uielement nodes when they are fully built
  if not voidP(aID) then
    if aElementType = #pano then
      plPanoElements[aID][#ready] = true
    else
      plUIElements[aID][#ready] = true
    end if
  end if
  
  tAllReady = true
  if plPanoElements.count = 0 and not (voidP(node["panoelement"]) and voidP(parent.poGlobalNode["panoelement"])) then
    tAllReady = false
  else
    repeat with tlElements in [plPanoElements, plUIElements]
      repeat with tElement in plPanoElements
        if not tElement[#ready] then
          tAllReady = false
          exit repeat
        end if
      end repeat
      if not tAllReady then exit repeat
    end repeat
  end if
  
  -- delay 'Scene ready' event until 3d sprite has redrawn
  if voidP(pBuilding) and voidP(pReadySent) and tAllReady = true then newTimeout("sceneready",10,#sceneReady, me)
end

on sceneReady me
  killTimeout("sceneready")
  
  setProperty("_spv.preview.bg","visible","false")
  
  spv().poLogger.show("Scene ready", #info)
  spv().resetFPS()
  
  spv().poTour.queueAction(me, "ready")
  pReadySent = true
end


on destroyObjects me, alObjects, aSource
  if voidP(aSource) then
    repeat with tElement = alObjects.count down to 1
      tlObject = alObjects[tElement]
      if ilk(tlObject) = #propList then
        if not voidP(tlObject[#object]) then destroyObject(tlObject[#object])
        
      else if not voidP(tlObject) then destroyObject(tlObject)
    end repeat
    alObjects = VOID
  else
    repeat with tElement = alObjects.count down to 1
      if ilk(alObjects[tElement]) = #propList then
        tSource = alObjects[tElement][#source]
      else tSource = alObjects[tElement].pSource
      
      if tSource = aSource then
        destroyObject(alObjects[tElement][#object])
        alObjects.deleteAt(tElement)
      end if
    end repeat
  end if
end


on buildObjects me, alObjects, aNodeName, aScriptName, aShortName
  -- get scene nodes
  tlNodes = node.getElementByName(aNodeName)
  repeat with tNodeNr = 1 to tlNodes.count
    tNode = tlNodes[tNodeNr]
    
    tID = getValue(#string, tNode.attributeValue["id"], spv().randomID(aShortName))
    tNode.attributeValue["id"] = tID
    
    if not voidP(alObjects[tID]) then spv().poLogger.show("Duplicate " & aNodeName & " ID '"& tID &".", #error)
    
    alObjects[tID] = [#node:tNode, #source: #scene, #ready: false]
  end repeat
  
  -- add global nodes
  tlNodes = parent.poGlobalNode.getElementByName(aNodeName)
  repeat with tNodeNr = 1 to tlNodes.count
    tNode = tlNodes[tNodeNr]
    
    tID = getValue(#string, tNode.attributeValue["id"], spv().randomID(aShortName))
    tNode.attributeValue["id"] = tID
    
    if voidP(alObjects[tID]) then
      alObjects[tID] = [#node:tNode, #source: #global, #ready: false]
    end if
  end repeat
  
  if not voidP(aScriptName) then
    -- build nodes
    repeat with tlObject in alObjects
      if voidP(tlObject[#object]) then
        tlObject[#object] = new(script(aScriptName), me, tlObject[#node])
        tlObject[#object].initNode()
      end if
    end repeat
  end if
end


on getImageNode me, aoElement, aoElementNode
  tImageNode = VOID
  tImageSource = VOID
  
  tImageID = aoElementNode.attributeValue["image"]
  if not voidP(tImageID) then
    -- search for image node in scene
    tImageNode = node.getElementById(tImageID)
    if not voidP(tImageNode) then tImageSource = #scene
    else
      tImageNode = parent.poGlobalNode.getElementById(tImageID)
      if not voidP(tImageNode) then tImageSource = #global
    end if
    
    if voidP(tImageNode) then spv().poLogger.show("Image '"& tImageID &"' for panoelement '"& aoElementNode.attributeValue["id"] &"' not defined.", #error)
    else
      if tImageNode.name <> "image" then spv().poLogger.show("Node '"& tImageID &"' for panoelement '"& aoElementNode.attributeValue["id"] &"' is not an image node.", #error)
    end if
  else
    tImageNode = aoElementNode["image"]
    
    if not voidP(tImageNode) then
      tImageID = getValue(#string, tImageNode.attributeValue["id"], spv().randomID("_img"))
      tImageNode.attributeValue["id"] = tImageID
      
      tImageSource = aoElement.pSource
    else
      tImageID = "_spv.emptyImage"
      aoElementNode.attributeValue["image"] = tImageID
      tImageNode = parent.poGlobalNode.getElementById(tImageID)
      tImageSource = #global
    end if
  end if
  
  if not voidP(tImageNode) then
    if voidP(plImages[tImageID]) then
      plImages[tImageID] = new(script("spv.Image"), me, tImageNode, tImageSource)
      plImages[tImageID].initNode()
    end if
    
    return plImages[tImageID]
  else
    return VOID
  end if
end


on getBehaviorObjects me, aoNode
  tlBehaviors = []
  
  repeat with tlBehavior in plBehaviors
    toBehaviorNode = tlBehavior[#node]
    
    if ((not voidP(aoNode.attributeValue["behavior"])) and toBehaviorNode.attributeValue["id"] = aoNode.attributeValue["behavior"]) or \
        ((not voidP(aoNode.attributeValue["id"])) and toBehaviorNode.attributeValue["object"] = aoNode.attributeValue["id"]) or \
        (voidP(toBehaviorNode.attributeValue["object"]) and aoNode.name = "scene" and toBehaviorNode.parent = parent.poGlobalNode) or \
        (toBehaviorNode.attributeValue["object"] = "_scene" and (aoNode = node or aoNode = parent.poGlobalNode)) or \
        (voidP(toBehaviorNode.attributeValue["object"]) and toBehaviorNode.parent = aoNode) then
      tlBehaviors.add(tlBehavior)
    end if
  end repeat
  
  return tlBehaviors
  
  -- by behavior id -> behavior.id=aoNode.behaviorid
  -- by reference   -> behavior.object=aoNode.id
  -- inline         -> behavior.parent = aoNode
  
  -- special case: inline behavior in global node for scene
  --                -> aoNode = _scene AND behavior.parent = _global
end


on scaleTextures me, aDirection
  if voidP(aDirection) or aDirection > 0 then pScaleTextures = 0
  else pScaleTextures = pScaleTextures + aDirection
  
  if pScaleTextures > -4 then
    if aDirection+0 < 0 then spv().poLogger.show("Low framerate detected; scaling down textures", #warning)
    repeat with toImage in plImages
      toImage.updateScale(pScaleTextures)
    end repeat
  end if
end

