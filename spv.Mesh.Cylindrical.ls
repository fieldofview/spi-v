-- Cylindrical modelresource

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pMesh
property pIsMesh

on new me, _parent, aUIElement
  pIsMesh = true
  return __new(me, _parent, void, aUIElement)
end

on destroy me
  -- Destroy model resource
  if not voidP(pMesh) then spv().pWorld.deleteModelResource(pMesh.name)
end

on buildMesh me
  tImageWidth = parent.poImageElement.pWidth
  tImageHeight = parent.poImageElement.pHeight

  tHFOV = getValue(#float, parent.pHFOV, 360)
  tHFOV = degRad(max(0.001, min(360, tHFOV)))
  tVFOV = getValue(#float, parent.pVFOV, radDeg(2*(atan((myfloat(tImageHeight * tHFOV) / tImageWidth) /2))))
  tVFOV = degRad(max(0.001, min(170, tVFOV)))

  tOffsetV = getValue(#float, parent.pVOffset, 0)
  tOffsetV = degRad(tOffsetV)

  tMeshHeight = 2*tan(tVFOV/2)
  tMeshOffsetHeight = tMeshHeight * (tOffsetV / tVFOV)

  parent.pFOVRanges = [[radDeg(-tHFOV/2), radDeg(tHFOV/2)], \
                       [radDeg(atan(-tMeshHeight/2 + tMeshOffsetHeight)), radDeg(atan(tMeshHeight/2 + tMeshOffsetHeight))]]

  tResolution = getValue(#integer, parent.pMeshResolution ,6)
  tPixelsPerVertex = float(tImageWidth) * degRad(tResolution) / tHFOV

  tlFaces = []
  tlVertices = []
  tlTextureCoordinates = []

  repeat with tV = 1 to parent.plShaders.count
    tRowRect = parent.plShaders[tV][1][#rect]

    tFOVStartV = tMeshHeight * (-.5 + myfloat(tRowRect.top) / tImageHeight) - tMeshOffsetHeight
    tFOVHeight = tMeshHeight * tRowRect.height / tImageHeight

    -- find vertical points in texture chunks
    tlPointsV = [0, tRowRect.height]

    repeat with tH = 1 to parent.plShaders[tV].count
      tShader = parent.plShaders[tV][tH].shader
      tRect = parent.plShaders[tV][tH][#rect]
      tChunkSize = parent.plShaders[tV][tH][#size]

      tFOVStartH = tHFOV * (-.5 + myfloat(tRect.left) / tImageWidth)
      tFOVWidth = tHFOV * tRect.width / tImageWidth

      -- find horizontal points in texture chunks
      tlPointsH = []
      repeat with tHLoc = 0 to tRect.width / tPixelsPerVertex
        tlPointsH.add(integer(tHLoc * tPixelsPerVertex))
      end repeat

      if tlPointsH.count > 1 and tRect.width - tlPointsH[tlPointsH.count] < .3 * tPixelsPerVertex then
        tlPointsH[tlPointsH.count] = tRect.width
      else tlPointsH.add(tRect.width)

      repeat with tVertexV = 1 to tlPointsV.count
        tY = tFOVStartV + tFOVHeight * myfloat(tlPointsV[tVertexV]) / tRect.height

        tTexCoordV = 1 - myfloat(tlPointsV[tVertexV]) / tChunkSize.locV
        repeat with tVertexH = 1 to tlPointsH.count
          tU = tFOVStartH + tFOVWidth * myfloat(tlPointsH[tVertexH])/tRect.width

          tX = sin(tU)
          tZ = cos(tU)

          tlVertices.add(vector(tX, -tY, -tZ))

          tTexCoordH = myfloat(tlPointsH[tVertexH]) / tChunkSize.locH
          tlTextureCoordinates.add(point(tTexCoordH, tTexCoordV))

          if tVertexH < tlPointsH.count and tVertexV < tlPointsV.count then
            tFace = [:]

            tFace[#vertices] = [tlVertices.count, tlVertices.count+tlPointsH.count, tlVertices.count+1]
            --tFace[#textureCoordinates] = [] -- use same index as #vertices...
            tFace[#shader] = tShader
            tlFaces.add(tFace.duplicate())

            tFace[#vertices] = [tlVertices.count+1, tlVertices.count+tlPointsH.count, tlVertices.count+tlPointsH.count+1]
            --tFace[#textureCoordinates] = [] -- use same index as #vertices...
            tFace[#shader] = tShader
            tlFaces.add(tFace.duplicate())
          end if
        end repeat
      end repeat
    end repeat
  end repeat

  __buildMesh(me, tlFaces, tlVertices, tlTextureCoordinates, true)
end
