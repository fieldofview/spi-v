-- <uielement> node
-- Instantiates/manages <image> and <behavior>

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt
property pPreviewBuilt
property pReady

property pID

property pElementType

property poParent
property pSource

property poImageElement
property poMesh
property plBehaviorObjects
property plShaders

property pModel
property pGroup

property plMeshFaceCounts

property pDimension, pPropDimension
property pActualDimension
property pWidth
property pHeight

property pOffset, pPropOffset
property pHOffset, pVOffset
property pAlign

property pZorder

property pRotation
property pBlend

property pEnabled, pVisible

property pMouseHoverMe, pMousePressMe

property pHasActivity, pHasClickAction

property pCatchEvents

property pSeamFix

on new me,  _parent, aoNode
  pElementType = #ui
  return __new(me, _parent, aoNode)
end

on initNode me
  __initNode(me)
end

on destroy me, aKeepImage
  __destroy(me, aKeepImage)
end

on imageReady me, aPreview
  -- shared code with PanoElemNode
  __removePreview(me, aPreview)
  __createShaders(me,false,true,aPreview)
  
  pDimension = []
  pPropDimension = []
  
  -- get parameters from node
  tFP = floatOrPercentage(node.attributeValue["width"], poImageElement.pWidth)
  pDimension[1] = tFP[1]
  pPropDimension[1] = tFP[2]
  
  tFP = floatOrPercentage(node.attributeValue["height"], poImageElement.pHeight)
  pDimension[2] = tFP[1]
  pPropDimension[2] = tFP[2]
  
  pAlign = point(0,0)
  pAlign.locH = 1*["center":0,"right":1,"left":-1][string(node.attributeValue["halign"])]
  pAlign.locV = 1*["center":0,"top":1,"bottom":-1][string(node.attributeValue["valign"])]
  
  pOffset = []
  pPropOffset = []
  
  pHOffset = node.attributeValue["hoffset"]
  tFP = floatOrPercentage(pHOffset,0)
  pOffset[1] = tFP[1]
  pPropOffset[1] = tFP[2]
  
  pVOffset = node.attributeValue["voffset"]
  tFP = floatOrPercentage(pVOffset,0)
  pOffset[2] = tFP[1]
  pPropOffset[2] = tFP[2]
  
  -- fake width and height for mesh builder
  pWidth = 100
  pHeight = 100
  
  -- create mesh
  poMesh = new(script("spv.Mesh.Flat"), me, true)
  
  pZorder = node.attributeValue["zorder"]
  if ["_spv.branding","_spv.management","_spv.preview"].getOne(pZorder) = 0 then pZorder = string(max(0,min(9, getValue(#integer, pZorder, 0))))
  
  if poParent = #root then
    pGroup = spv().pWorld.group("_zOverlay#"&pZorder)
  else
    if not integerP(value(pZorder)) then pZorder = "0"
    pGroup = spv().pWorld.group(poParent.pZBaseName & string(pZorder))
  end if
  
  if voidP(pVisible) then pVisible = getValue(#boolean, node.attributeValue["visible"], true)
  if voidP(pEnabled) then pEnabled = getValue(#boolean, node.attributeValue["enabled"], true)
  
  pCatchEvents = getValue(#boolean, node.attributeValue["catchevents"], false)
  
  -- add model to scene
  if not voidP(poMesh.pMesh) then
    pModel = spv().pWorld.newModel("_uim_" & pID, poMesh.pMesh)
    pModel.visibility = #front_noDepth
    
    -- count faces in submeshes for picking
    plMeshFaceCounts = [0]
    pModel.addModifier(#meshDeform)
    repeat with tMesh = 1 to pModel.meshDeform.mesh.count-1
      plMeshFaceCounts.add(pModel.meshDeform.mesh[tMesh].face.count)
    end repeat
    plMeshFaceCounts = accumulate(plMeshFaceCounts)
    pModel.removeModifier(#meshDeform)
    
    
    me.updateLoc()
    pModel.AddToWorld()
    if pVisible = true then pModel.parent = pGroup
    else pModel.parent = spv().pVoidGroup
    
    pRotation = getValue(#float, node.attributeValue["rotation"], 0)
    pModel.transform.rotation.z = -pRotation
    
    if not aPreview then me.getBehaviors()
  end if
  
  -- notify scene, parent group(s)
  if not aPreview then
    me.readyEvent()
  else pPreviewBuilt = true
end

on updateLoc me
  -- update location within sprite after sprite dimensions have changed
  if not voidP(pModel) then
    if poParent = #root then
      tParentWidth = spv().width
      tParentHeight = spv().height
    else
      tParentWidth = poParent.pActualDimension.locH
      tParentHeight = poParent.pActualDimension.locV
    end if
    
    -- calculate actual width and offset
    pActualDimension = point(pDimension[1], pDimension[2]).duplicate()
    if pPropDimension[1] then pActualDimension.locH = float(tParentWidth * pActualDimension.locH) / 100
    if pDimension[1] < 0 then pActualDimension.locH = tParentWidth + pActualDimension.locH
    
    if pPropDimension[2] then pActualDimension.locV = float(tParentHeight * pActualDimension.locV) / 100
    if pDimension[2] < 0 then pActualDimension.locV = tParentHeight + pActualDimension.locV
    
    tOffset = point(pOffset[1], pOffset[2]).duplicate()
    if pPropOffset[1] then tOffset.locH = float(tParentWidth * pOffset[1]) / 100
    if pPropOffset[2] then tOffset.locV = float(tParentHeight * pOffset[2]) / 100
    
    tPosition = vector(0,0,0)
    tPosition.x = pAlign.locH * (float(tParentWidth -  pActualDimension.locH)/2) + tOffset.locH
    tPosition.y = pAlign.locV * (float(tParentHeight - pActualDimension.locV)/2) + tOffset.locV
    pModel.transform.position = tPosition
    
    tScale = vector(1,1,1)
    tScale.x = float(pActualDimension.locH) / 100 -- use fake width instead of pWidth
    if tScale.x = 0 then tScale.x = 0.001
    tScale.y = float(pActualDimension.locV) / 100 -- use fake height instead of pHeight
    if tScale.y = 0 then tScale.y = 0.001
    
    pModel.transform.scale = tScale
  end if
end

on updateTransform me
  if not voidP(pModel) then pModel.transform.rotation.z = -pRotation
end


on showLayer me, aLayer
  __showLayer(me, aLayer)
end

on groupProperty me, aProperty
  return __groupProp(me, aProperty)
end

on getBehaviors me
  __getBehaviors(me)
end

--
-- event handling
--

on readyEvent me
  __readyEvent(me, #element)
end

on mouseEnterEvent me
  __mouseEnterEvent(me, #element)
end

on mouseLeaveEvent me
  __mouseLeaveEvent(me, #element)
end

on mouseDownEvent me
  __mouseDownEvent(me, #element)
end

on mouseUpEvent me
  __mouseUpEvent(me, #element)
end

--
-- public properties
--

on setProp me, aProperty, aValue
  case aProperty of
    "visible","enabled":
      __setPropElement(me, symbol("p"&aProperty), aValue)
      
    "catchevents":
      __setPropCatch(me, aValue)
      
    "halign":
      pAlign.locH = 1*["center":0,"right":1,"left":-1][string(aValue)]
      me.updateLoc()
      
    "valign":
      pAlign.locV = 1*["center":0,"top":1,"bottom":-1][string(aValue)]
      me.updateLoc()
      
    "hoffset":
      __setPropUI (me, aValue, #pOffset,#pPropOffset, 1, #pHOffset)
      
    "voffset":
      __setPropUI (me, aValue, #pOffset,#pPropOffset, 2, #pVOffset)
      
    "width":
      __setPropUI (me, aValue, #pDimension, #pPropDimension, 1, #pWidth)
      
    "height":
      __setPropUI (me, aValue, #pDimension, #pPropDimension, 2, #pHeight)
      
    "rotation":
      __setPropPano (me, aProperty, aValue)
      
    "blend":
      __setPropBlend (me, aValue)
      
    "zorder":
      __setPropZorder (me, aValue, pModel, "_zOverlay#")
  end case
end