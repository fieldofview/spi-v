-- Preload external files

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pReady

property plJob

property poConnection
property pNetID

property pServer
property pPort
property pURI

property pStatus
property pBytesTotal
property pBytesSofar
property pMIME

property pCallbackHandler
property poCallbackObject

on new me, _parent, alJob
  parent = _parent
  plJob = alJob

  spv().scriptInstanceList.add(me)
  return me
end

on destroy me, aFlag
  if not voidP(poConnection) then
    poConnection.breakConnection("_spv")
    poConnection = VOID
  end if
  if not voidP(pNetID) then
    parent.pActiveConnections = parent.pActiveConnections-1
    if not aFlag then netAbort(pNetID)
    pNetID = void
  end if

  spv().scriptInstanceList.deleteOne(me)
end

on enterFrame me
  if voidP(pNetID) and voidP(pReady) then
    if parent.pActiveConnections < parent.pMaxConnections then
      if not voidP(poConnection) then
        poConnection.breakConnection("_spv")
        poConnection = VOID
      end if
      if plJob.result = #text then
        pNetID = getNetText(plJob.importURL)
      else pNetID = preloadNetThing(plJob.importURL)
      parent.pActiveConnections = parent.pActiveConnections+1
    else if voidP(poConnection) and voidP(pBytesTotal) then
      me.muxHead()
    end if
  else
    me.getPreloadStatus()
  end if
end

on getPreloadStatus me
  tStreamStatus = getStreamStatus(pNetID)

  if voidP(pMIME) then pMIME = netMIME(pNetID)
  pBytesSofar = tStreamStatus.bytesSofar
  if tStreamStatus.bytesTotal > 0 then
    pBytesTotal = tStreamStatus.bytesTotal
  else
    -- could not get total filesize
    pBytesTotal = min(4096, pBytesSofar)
  end if

  if netDone(pNetID) then
    plJob[#streamError] = tStreamStatus.error
    if plJob[#result] = #text then
      plJob[#text] = netTextResult(pNetID)
    end if
    me.destroy(true)
    pReady = true
  end if
end

on muxHead me
  tURL = plJob[#importURL]
  if not(tURL contains "://") then tURL = the moviePath & tURL
  tURL = chars(tURL, offset("://",tURL)+3, length(tURL))
  the itemDelimiter = "/"
  pServer = tURL.item[1]
  pURI = chars(tURL, length(pServer)+2, length(tURL))
  the itemDelimiter = ":"
  if pServer.item.count>1 then
    pPort = integer(pServer.item[2])
    pServer = pServer.item[1]
  else pPort = 80

  poConnection = new(xtra "multiuser")
  poConnection.setNetMessageHandler(#muxMessage,me)
  poConnection.connectToNetServer("_spv","",pServer,pPort,"spv",1)
end

on muxMessage me
  tMsg = poConnection.getNetMessage()
  if tMsg.errorCode = 0 then
    case TRUE of:
      (tMsg.senderID = "System" AND tMsg.subject = "ConnectToNetServer"):
        RET = numToChar(13)&numToChar(10)

        poConnection.sendNetMessage("_spv","-",\
          "HEAD" && pURI && "HTTP/1.0" & RET & \
          "Host: " & pServer & RET & \
          "Connection: close" & RET & RET)
      (tMsg.senderID = pServer AND tMsg.subject = "String"):
        tMsg = tMsg.content
        pStatus = integer(tMsg.line[1].word[2])

        if pStatus = "200" then
          repeat with tLineNr = 2 to tMsg.line.count
            tLine = tMsg.line[tLineNr]
            case tLine.word[1] of
              "Content-Length:":
                pBytesTotal = integer(the last word of tLine)
              "Content-Type:":
                pMIME = the last word of tLine
            end case
            if not (voidP(pBytesTotal) or voidP(pMIME)) then exit repeat
          end repeat
        end if
        if voidP(pBytesTotal) then pBytesTotal = 0
        poConnection = VOID
        parent.pActiveConnections = parent.pActiveConnections-1
    end case
  else
    spv().poLogger.show("Could not get info for file '"& plJob[#URL] &": " & \
      poConnection.GetNetErrorString(tMsg.errorCode),#warning)
    pBytesTotal = 0
    poConnection = VOID
    parent.pActiveConnections = parent.pActiveConnections-1
  end if
end