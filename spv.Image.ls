-- <image> node
-- Instantiates/manages <layer>

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt

property pID
property pSource

property plImageLayers

property plInterestedElements

property pImageReady

property pWidth
property pHeight

property pAutoHeight

property pScaleTextures

on new me,  _parent, aoNode, aSource
  pSource = aSource

  plInterestedElements = []

  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)

    -- destroy layers associated with this image
    repeat with toElement in plInterestedElements
      toElement.destroy()
    end repeat

    if not voidP(plImageLayers) then
      repeat with tImageLayer in plImageLayers
        tImageLayer[#object].destroy()
      end repeat
    end if

    plImageLayers = VOID

    if not voidP(parent) then parent.plImages.deleteProp(pID)
  end if
end


on initNode me
  if voidP(pBuilt) then
    pBuilt = true

    pID = getValue(#string, node.attributeValue["id"])

    pScaleTextures = 0

    -- build layers
    plImageLayers = [:]
    tlLayerNodes = node.getElementByName("layer")

    if tlLayerNodes.count > 0 then
      repeat with tLayerNode in tlLayerNodes
        tLayerClass = getValue(#string, tLayerNode.attributeValue["class"], "base")

        if voidP(plImageLayers[tLayerClass]) then
          plImageLayers[tLayerClass] = [#node: tLayerNode , #object: VOID, #state: #init ]
        end if
      end repeat

      if not ((the moviePath & spv().pSourceURL) contains "://") then
        plImageLayers.deleteProp("preview")
      end if

      repeat with tLayer in plImageLayers
        tLayer[#object] = new(script("spv.Layer"), me, tLayer[#node])
        tLayer[#object].initNode()
        tLayer[#object].prepareLayer()
      end repeat
    else
      me.layerReady()
    end if
  end if
end

on layerReady me, aReadyLayer
  if not voidP(aReadyLayer) then plImageLayers[aReadyLayer][#state] = #prepped

  tAllReady = true
  if aReadyLayer <> "preview" then
    tlImageLayers = plImageLayers.duplicate()
    tlImageLayers.deleteProp("preview")

    repeat with tlLayer in tlImageLayers
      if tlLayer[#state] = #init then
        tAllReady = false
        exit repeat
      end if
    end repeat
  else
    tlImageLayers = ["preview":plImageLayers["preview"]]

    -- do not proceed if the main image has already been built
    if pImageReady = true then tAllReady = false
  end if

  if tAllReady then
    if not (getValue(#boolean, node.attributeValue["silent"], false) or aReadyLayer <> "preview")then
      spv().poLogger.show("Creating textures for image '"& pID &"'", #info)
    end if

    -- get width from image node attributes
    pWidth = getValue(#integer, node.attributeValue["width"], void)
    pHeight = getValue(#integer, node.attributeValue["height"], void)

    if not (integerP(pWidth) and integerP(pHeight)) then
      -- width not defined in image attributes
      repeat with tlLayer in tlImageLayers
        tResource = VOID

        if not voidP(tlLayer[#object].plResources[#src]) then
          tResource = tlLayer[#object].plResources[#src][#resource]
        else if not voidP(tlLayer[#object].plResources[#alphasrc]) then
          tResource = tlLayer[#object].plResources[#alphasrc][#resource]
        end if

        if tResource.ilk=#image then
          pWidth = tResource.width
          pHeight = tResource.height

          exit repeat
        end if
      end repeat

      -- no width and height implied by base image?
      if not integerP(pWidth) then pWidth = 120
      if not integerP(pHeight) then
        pAutoHeight = true
        pHeight = 120
      end if
    end if

    repeat with tlLayer in tlImageLayers
      tlLayer[#object].makeLayer()
      --plImageLayers[aReadyLayer][#state] = #ready
    end repeat

    if aReadyLayer <> "preview" then
      me.updateScale(parent.pScaleTextures)
    end if

    repeat with toElement in plInterestedElements
      toElement.imageReady(aReadyLayer="preview")
    end repeat

    if aReadyLayer <> "preview" then pImageReady = true
  end if
end

on addInterest me, aoElement
  plInterestedElements.add(aoElement)

  if pImageReady then aoElement.imageReady()
end

on removeInterest me, aoElement
  plInterestedElements.deleteOne(aoElement)

  if plInterestedElements.count = 0 then me.destroy()
end

on updateScale me, aScale
  aScale = min(1,aScale)
  if(aScale<>pScaleTextures) then
    if pWidth>250 or pHeight>250 then
      tPanoElem = false
      repeat with toInterested in plInterestedElements
        if toInterested[#pElementType] = #pano then
          tPanoElem = true
          exit repeat
        end if
      end repeat

      if tPanoElem then
        repeat with tlLayer in plImageLayers
          tlLayer[#object].updateScale(aScale - pScaleTextures)
        end repeat
        pScaleTextures = aScale
      end if
    end if
  end if
end
