-- movie and utility scripts

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--



-- ------------------------
-- create SPi-V object
--
on startMovie
  new(script "spv.Main")
end


-- ------------------------
-- keep the movie running
--

on exitFrame
  go to the frame
end

-- ------------------------
-- to keep the engine from
-- doing too much when it
-- isn't active
--

on activateWindow
  sendSprite(spv(),#activate,true)
end

on deactivateWindow
  sendSprite(spv(),#activate,false)
end

-- ------------------------
-- return a reference to
-- the SPi-V sprite
--
on spv
  return sprite(1)
end spv


-- ------------------------
-- reset renderer after
-- window was minimized
--
on resizeWindow
  if spv()[#pSWV]>=10 then
    if (the activeWindow).sizeState <> #minimized and voidP(spv()[#pLockRenderer]) then
      the preferred3dRenderer = #auto
    end if
  end if
end resizeWindow

-- ------------------------
-- relay evalScript events
--
on evalScript aMessage
  if not voidP(spv()[#poLocalConnection]) then return spv().poLocalConnection.incomingMessage(void, aMessage)
end

-- ------------------------
-- relay keypresses
--
on handleKey aKeyCode, aCommandDown, aOptionDown, aShiftDown
  if not voidP(spv()[#poKeyHandler]) then spv().poKeyHandler.handleKey(aKeyCode, aCommandDown, aOptionDown, aShiftDown)
end

-- ------------------------
-- utility handlers
-- ------------------------

-- ------------------------
-- return a script reference
-- in this movie or the 
-- parent LDM
--
on scriptEx aScriptName
  tScript = script(aScriptName)
  if voidP(tScript) then
    toLDMscript = spv().poLDMscript
    if not voidP(toLDMscript) then
      if toLDMscript.handler(#scriptEx) then
        tScript = toLDMscript.scriptEx(aScriptName)
      end if
    end if
  end if
  return tScript
end 

-- ------------------------
-- work around a bug with
-- Firefox 1.5 and newer
--

on myfloat aValue
  case aValue.ilk of
    #float, #integer:
      return float(aValue)
    #string:
      if not floatP(float("1.0")) then
        tLength = aValue.length
        tOffset=offset(".", aValue)
        repeat while tOffset>0
          aValue = chars(aValue,0,tOffset-1)&","&chars(aValue,tOffset+1,tLength)
          tOffset=offset(".", aValue)
        end repeat
      end if
      tValue = float(aValue)
      if floatP(tValue) then return tValue
    otherwise:
      return VOID
  end case
end


-- ------------------------
-- provides alternative for
-- native timeouts (which
-- don't work in LDMs
--

on newTimeout aName, aPeriod, aHandler, aObject
  setAprop(spv().plTimeouts, symbol(aName),[#start:the milliseconds, \
    #period:aPeriod, #handler:aHandler, #object: aObject])
end

on killTimeout aName
  spv().plTimeouts.deleteProp(symbol(aName))
end



-- ------------------------
-- destroy a script object
-- and it's children
--
on destroyObject aObject
  if not voidP(aObject) then
    if aObject.handler(#destroy) then aObject.destroy()
    repeat with tProp = 1 to aObject.count
      aObject[aObject.getPropAt(tProp)] = void
    end repeat
    aObject = VOID
  end if
end


-- ------------------------
-- string methods
--

on forceLowerCase aInput
  -- returns the lowercase equivalent of the string
  
  tOutput = EMPTY
  repeat with tCharNum = 1 to length(aInput)
    tASCII = charToNum(aInput.char[tCharNum])
    if tASCII = min(max(64, tASCII), 91) then
      tASCII = tASCII + 32
      if tASCII = min(max(95, tASCII), 123) then
        tOutput = tOutput & numToChar(tASCII)
      end if
    else
      tOutput = tOutput & aInput.char[tCharNum]
    end if
  end repeat
  return tOutput
end

on forceAlphaNumeric aInput, aKeepCase
  -- returns the lowercase, alphanumeric equivalent of the string

  tOutput = EMPTY
  repeat with tCharNum = 1 to length(aInput)
    tASCII = charToNum(aInput.char[tCharNum])
    if tASCII = min(max(64, tASCII), 91) then
      -- A-Z
      if voidP(aKeepCase) then tASCII = tASCII + 32

    else if tASCII = min(max(96, tASCII), 123) or tASCII = min(max(48, tASCII), 57) then
      -- a-z or 0 to 9
      nothing

    else
      -- any other char
      tASCII = void

    end if
    if not voidP(tASCII) then tOutput = tOutput & numToChar(tASCII)
  end repeat
  return tOutput
end

on replaceString aHaystack, aNeedle, aInsert
  -- replaces all occurances of needle in haystack with insert

  tOutput = empty
  tFindLen = aNeedle.length - 1
  repeat while aHaystack contains aNeedle
    tOffset = offset(aNeedle, aHaystack)
    tOutput = tOutput & aHaystack.char [1..tOffset]
    delete the last char of tOutput
    tOutput = tOutput & aInsert
    delete aHaystack.char [1.. (tOffset + tFindLen)]
  end repeat
  return tOutput & aHaystack
end


-- ------------------------
-- list methods
--

on appendList alDest, alSrc
  -- appends one list to another

  repeat with tItem in alSrc
    alDest.append(tItem)
  end repeat

  return alDest
end

on accumulate aList
  -- accumulates values in a list

  tList = aList.duplicate()
  tTotal = 0
  repeat with tItemNr = 1 to tList.count
    tItem = tList[tItemNr]
    tList[tItemNr] = tList[tItemNr] + tTotal

    tTotal = tTotal + tItem
  end repeat

  return tList
end

on addOnce aList, aValue
  -- adds a value to a list if it isn't in it already

  tPos = aList.getOne(aValue)
  if tPos = 0 then
    aList.add(aValue)
    tPos = aList.count
  end if
  return tPos
end

on sortProps alPropList, aProperty
  -- sort a list of property lists by a property or index

  if alPropList.ilk() = #list then
    tList = []
    repeat with tItem = 1 to alPropList.count
      tList.append([alPropList[tItem][aProperty], tItem])
    end repeat
    tList.sort()

    tlNewList = []
    repeat with tItem in tList
      tlNewList.append(alPropList[tItem[2]])
    end repeat

    return tlNewList
  else if alPropList.ilk() = #propList then
    tList = []
    repeat with tItem = 1 to alPropList.count
      tList.append([alPropList[tItem][aProperty], tItem, alPropList.getPropAt(tItem)])
    end repeat
    tList.sort()

    tlNewList = [:]
    repeat with tItem in tList
      tlNewList[tItem[3]] = (alPropList[tItem[2]])
    end repeat

    return tlNewList
  end if
end

on nearestValue alList, aValue
  tPos = alList.getOne(aValue)
  if tPos = 0 then
    if alList.count > 0 then
      tlList = alList.duplicate()
      tlList.add(aValue)
      tlList.sort()
      tPos = tlList.getOne(aValue)
      if tPos>1 then
        if tPos < tlList.count then
          return [tlList[tPos-1], false, alList.getOne(tlList[tPos-1])]
        else
          return [tlList[tlList.count-1],void,alList.getOne(tlList[tlList.count-1])]
        end if
      else
        return [tlList[2],void,alList.getOne(tlList[2])]
      end if
    else return [void,void,void]
  else return [aValue, true,tPos]
end nearestPos

-- ------------------------
-- netlingo error codes
--

on netErrorText aError
  case aError of
    ""   : return "No error specified"
    "OK", 0                 : return "OK"
    -128, 4167, 4242        : return "Transfer was interrupted by client."
    1, 4155                 : return "Not enough memory or memory error."
    4, 5, 4240              : return "Network Xtras may be improperly installed."
    6, 905, 4159, 4165, 4152: return "Invalid URL or not found."
    20   : return "Internal error. Browser detected a network or internal error."
    4149 : return "Unexpected format."
    4150 : return "Connection terminated unexpectedly."
    4154 : return "Operation could not be completed due to a timeout."
    4156 : return "Unexpected protocol."
    4157 : return "Access denied."
    4166 : return "Generic proxy failure."
    otherwise :
      if aError > 4143 AND aError < 4169 then
        return "Failed network operation:" && aError
      else
        return "Unknown network error:" && aError
      end if
  end case
end

-- ------------------------
-- URL utilities
--

on relativeURL aURL, aRelative
  -- returns a URL relative to another

  if aURL starts "#" then return aURL

  if stringP(aRelative) then
    the itemDelimiter = "#"
    aRelative = aRelative.item[1]
    the itemDelimiter = "?"
    aRelative = aRelative.item[1]
  end if

  if aURL contains "://" or aURL starts "javascript:" then return aURL
  else if the platform contains "Windows" and (chars(aURL, 2,3) = ":\" or aURL starts "\\") then return aURL
  else if the platform contains "Macintosh" and aURL contains ":" then return aURL
  else
    if string(aRelative) ="" then
      if the moviePath contains "://" then
        aRelative = the moviePath
      else aRelative = "@/"
    end if

    the itemDelimiter = "/"
    if not(aRelative contains "/") then
      tItemDelimiter = the last char of the moviePath

      -- replace / with path delimiter for this system
      the itemDelimiter = "/"
      tURL = ""

      repeat with tChunk = 1 to aURL.item.count-1
        tURL = tURL & aURL.item[tChunk] & tItemDelimiter
      end repeat
      aURL = tURL & aURL.item[aURL.item.count]

      the itemDelimiter = tItemDelimiter
    end if

    if aURL starts "/" then
      if aRelative contains "://" then
        the itemDelimiter = "/"
        if aRelative.item[1] <> (the moviePath).item[1] then
          return aRelative.item[1]&"//"&aRelative.item[3]&aURL
        else return aURL
      else return aURL
    else
      tRelativePath = chars(aRelative, 1, aRelative.length - (the last item of aRelative).length)
      if aURL starts tRelativePath then return aURL
      else return tRelativePath & aURL
    end if
  end if
end

on domainFromPath aPath
  -- gets the domain from a path
  tPath = forceLowerCase(aPath)

  -- strip http://
  the itemDelimiter = "://"
  tProtocolLength = tPath.item[1].length + 4
  tPath = chars(tPath, tProtocolLength, tPath.length)

  -- strip www.
  if tPath starts "www." then tPath = chars(tPath, 5, tPath.length)

  -- get domain only
  the itemDelimiter = "/"
  if tPath.item.count > 1 then
    tPath = tPath.item[1]
  else tPath = ""

  return tPath
end

on fileExt aFile
  -- gets extension from file name or url

  the itemDelimiter = "?"
  aFile = aFile.item[1]
  the itemDelimiter = "#"
  aFile = aFile.item[1]

  the itemDelimiter = "."
  aFile = the last item of aFile

  return forceLowerCase(aFile)
end




-- ------------------------
-- gets either a float or a
-- percentage from a string
--

on floatOrPercentage aString, aDefault
  tReturn = [void, false]

  if not voidP(aString) then
    aString = string(aString)
    if chars(aString, aString.length, aString.length) = "%" then
      tReturn[2] = true
      aString = chars(aString, 1, aString.length-1)
    end if
    tValue = myfloat(aString)
    if aString<>"" and floatP(tValue) then tReturn[1] = tValue
  end if

  if voidP(tReturn[1]) then
    tReturn = [aDefault, false]
  end if

  return tReturn
end


-- ------------------------
-- gets either a singular
-- value or a variable value
--

on varOrValue aString
  if aString starts "$" then
    aString = chars(aString,2,length(aString))
    if aString starts "$" then
      return aString
    else
      the itemDelimiter = ","
      if aString.item.count>1 then
        tDefault = aString.item[2]
        aString = aString.item[1]
        tFlags = "silent"
      end if

      tValue = getProperty(aString,"value", tFlags)
      if not voidP(tValue) then
        return tValue
      else return tDefault
    end if
  else return aString
end



-- ------------------------
-- misc math
--

on degRad aNumber
  return PI * aNumber / 180
end

on radDeg aNumber
  return 180 * aNumber / PI
end

on asin ratio
  if ratio < 1 and ratio > -1 then
    return atan(ratio/sqrt(1.0 - (ratio * ratio)))
  else if ratio = 1 then
    return pi()/2
  else if ratio = -1 then
    return -pi()/2
  end if
end

on acos ratio
  if ratio < 0 then
    return pi() + atan(sqrt(1.0 - (ratio * ratio))/ratio)
  else if ratio > 0 then
    return atan(sqrt(1.0 - (ratio * ratio))/ratio)
  else
    return pi()/2
  end if
end

on bool aValue
  if voidP(aValue) then return false
  else
    if floatP(myfloat(aValue)) then return myfloat(aValue)<>0
    else return aValue = "true"
  end if
end

on sign aNum
  if aNum=0 then return 0
  else return integer((aNum/abs(aNum)))
end

on randomFloat aMin, aMax
  return float(random(10000 * aMin, 10000 * aMax)) / 10000
end

on powerOf2 aNumber
  -- returns the next power of 2
  return integer(power(2,integer(0.499999 + log(aNumber)/log(2) ) ))
end

on clip aNumber, aMin, aMax
  tQuotient = float(aNumber - aMin) / (aMax-aMin)
  tModulo = tQuotient - integer(tQuotient)
  if tModulo<0 then tModulo = 1+ tModulo
  tClipped = aMin + tModulo * (aMax-aMin)
  if float(integer(tClipped)) = tClipped then tClipped = integer(tClipped)
  return tClipped
end


-- ------------------------
-- converts from rgb color
-- to a value from 0 to 100
--

on colorToValue aColor
  if aColor.ilk = #color then
    if aColor.colorType = #paletteIndex then
      return float(aColor.paletteIndex) / 255
    else
      return float(aColor.red + aColor.green + aColor.blue) / 765
    end if
  else
    return VOID
  end if
end


-- ------------------------
-- Returns texture uv from
-- face uv in pickdata
--
-- by James Newton
--
on mapPointToTexture(iSectList)
  -- iSectList is the result of aCamera.modelsUnderLoc(aLoc, #detailed) or
  -- aMember.modelsUnderRay(aPoint, aVector, #detailed)

  tModel    = iSectList.model
  tResource = tModel.resource

  tFace    = iSectList.faceID
  tUVCoord = iSectList.uvCoord

  tFaceList  = tResource.face[tFace].textureCoordinates

  -- Calculate the position of the points <a>, <b> and <c> within the
  -- Bitmap member.

  tLocA = tResource.textureCoordinateList[tFaceList[1]] -- [<float>, <float>]
  tLocA = point(tLocA[1] , (1 - tLocA[2]) )

  tLocB = tResource.textureCoordinateList[tFaceList[2]] -- [<float>, <float>]
  tLocB = point(tLocB[1] , (1 - tLocB[2]) )

  tLocC = tResource.textureCoordinateList[tFaceList[3]] -- [<float>, <float>]
  tLocC = point(tLocC[1] , (1 - tLocC[2]) )

  tUVector = (tLocB - tLocA) * tUVCoord.u -- actually a 2D point...
  tVVector = (tLocC - tLocA) * tUVCoord.v -- ... rather than a vector

  -- Start from the origin <a>, move first in the u direction then in
  -- the v direction to end up at the point in the texture

  return tLocA + tUVector + tVVector
end


-- ------------------------
-- Checks if a string contains
-- 'dangerous' functioncalls
--

on validFunctions aString, aAPI
  aString=forceLowerCase(aString)

  if aAPI then
    tlValidFunctions = ["openurl","geturl","addlog", "logmessage", "setview", "setproperty", "getproperty", "setwindow", "exitwindow"]
  else
    tlValidFunctions = [ "getproperty", \
    "sign", "abs", "max", "min", "random", "randomfloat", \
    "integer", "floa    t", "string", "bool", \
    "sin", "cos", "tan", "sqrt", "power", "log"]
  end if


  tlTokenDelimiters = " ,.+-=/*[(&"

  toTokens = new(script "xml.tokenizer", aString, tlTokenDelimiters, TRUE)
  tlTokens = []
  repeat while toTokens.hasMoreTokens()
    tlTokens.add(toTokens.nextToken())
  end repeat

  tAllOk = true
  repeat with tTokenNr = 1 to tlTokens.count
    if tlTokens[tTokenNr] = "(" then
      -- a function was called, find out the function name

      repeat with tTraceToken = tTokenNr-1 down to 1
        if offset(tlTokens[tTraceToken], tlTokenDelimiters) = 0 then
          if getPos(tlValidFunctions, tlTokens[tTraceToken]) = 0 then
            -- not one of our valid functions, complain
            tAllOk = false
          end if

          exit repeat
        end if
        if tAllOk = false then exit repeat
      end repeat
    end if
  end repeat

  return tAllOk
end


-- ------------------------
-- adds an XML node as a
-- child to another node
--

on addNode aType, aoParent, aData
  toXML = spv().poXML
  if not voidP(aType) then
    toNewNode = new(script "xml.node", aType, #element, "", aoParent)
    if not voidP(aData) then
      toNewNode.attributeValue = aData
      if not voidP(aData["id"]) then tlTags = toXML._idList[aData["id"]] = toNewNode
    end if
    if voidP(toXML._tagList[aType]) then toXML._tagList[aType] = []
    toXML._tagList[aType].add(toNewNode)
  else
    toNewNode = new(script "xml.node", "", #chardata, aData, aoParent)
  end if

  aoParent.child.add(toNewNode)
  aoParent.count = aoParent.count + 1
  return toNewNode
end


-- ------------------------
-- gets value of a certain
-- type
--

on getValue aType, aValue, aDefault
  if not voidP(aValue) then
    case aType of
      #string:
        return aValue
      #integer:
        aValue = integer(aValue)
        if integerP(aValue) then return aValue
        else return aDefault
      #float:
        aValue = myfloat(aValue)
        if floatP(aValue) then return aValue
        else return aDefault
      #boolean:
        if aValue = "true" or aValue = "1" then return true
        else if aValue = "false" or aValue = "0" then return false
        else return aDefault
    end case
  else return aDefault
end
