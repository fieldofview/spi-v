-- <panoelement> node
-- Instantiates/manages <image> and <behavior>

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt
property pPreviewBuilt
property pReady

property pID

property pElementType

property poParent
property pSource

property poImageElement
property poMesh
property plBehaviorObjects
property plShaders

property pModel
property pGroup

property plMeshFaceCounts
property pFOVRanges

property pMouseHoverMe, pMousePressMe

property pHasActivity, pHasClickAction
property pCatchEvents

property pSeamFix

property pType

property pPan, pTilt, pRoll
property pHFOV, pVFOV, pHOffset, pVOffset
property pZorder

property pBlend

property pMeshResolution

property pEnabled, pVisible

on new me,  _parent, aoNode
  pElementType = #pano
  return __new(me, _parent, aoNode)
end

on initNode me
  __initNode(me)
end

on destroy me, aKeepImage
  __destroy(me, aKeepImage)
end

on imageReady me, aPreview
  __removePreview(me, aPreview)

  -- get parameters from node
  repeat with tAttribute in ["pan","tilt","roll"]
    me[symbol("p" & tAttribute)] = getValue(#float, node.attributeValue[tAttribute], 0)
  end repeat
  repeat with tAttribute in ["hfov","vfov","hoffset","voffset"]
    me[symbol("p" & tAttribute)] = getValue(#float, node.attributeValue[tAttribute], void)
  end repeat

  pMeshResolution = getValue(#integer, node.attributeValue["meshresolution"], void)

  pZorder = node.attributeValue["zorder"]
  if pZorder <> "_spv.preview" then pZorder = string(max(0,min(9,value(getValue(#integer, pZorder, 0)))))

  if poParent = #root then
    pGroup = spv().pWorld.group("_zScene#"&pZorder)
  else
    if not integerP(value(pZorder)) then pZorder = "0"
    pGroup = spv().pWorld.group(poParent.pZBaseName & string(pZorder))
  end if

  pVisible = getValue(#boolean, node.attributeValue["visible"], true)
  pEnabled = getValue(#boolean, node.attributeValue["enabled"], true)

  pCatchEvents = getValue(#boolean, node.attributeValue["catchevents"], false)

  -- check panoElement type
  pType = node.attributeValue["type"]
  if voidP(pType) then
    -- determine node type by image dimensions
    tWidth = poImageElement.pWidth
    tHeight = poImageElement.pHeight

    if tWidth = 6*tHeight or tHeight = 6*tWidth then
      pType = "cubic"
    else if tWidth = 2*tHeight then
      pType = "spherical"
    else if tWidth > 2*tHeight then
      pType = "cylindrical"
    else
      pType = "flat"
    end if

    tMessage = "Assuming "& pType &" type for panoelement '"& node.attributeValue["id"] &"'"
    spv().poLogger.show(tMessage, #info)
  end if

  -- check antialias setting
  if pType = "spherical" then tAntialias = false
  else tAntiAlias = true

  -- shared code with UIElemNode
  __createShaders(me,tAntiAlias,false,aPreview)


  me.makeMesh(aPreview)

  -- notify scene, parent group(s)
  if not aPreview then
    me.readyEvent()
  else pPreviewBuilt = true
end

on makeMesh me, aPreview
  -- destroy current mesh, if any
  if not voidP(poMesh) then
    destroyObject(poMesh)
    if not voidP(pModel) then spv().pWorld.deleteModel(pModel.name)
  end if

  -- create mesh, depending on type
  tScript = scriptEx("spv.Mesh." & pType)
  if voidP(tScript) then
    spv().poLogger.show("Unknown type for panoelement '"& node.attributeValue["id"] &"': " & pType, #error)
    exit
  end if

  poMesh = new(tScript, me)

  -- if the cameralimits node depends on this panoelement, notify it.
  if pID = spv().poNavigator.pLimitPanoElement then spv().poNavigator.setCameraValues(false,me)

  -- add object to scene
  if not voidP(poMesh.pMesh) then
    tSize = 1000

    pModel = spv().pWorld.newModel("_mod_" & pID, poMesh.pMesh)
    pModel.visibility = #front_noDepth
    pModel.transform.scale(tSize, tSize, tSize)
    me.updateTransform()

    -- count faces in submeshes for picking
    plMeshFaceCounts = [0]
    pModel.addModifier(#meshDeform)
    repeat with tMesh = 1 to pModel.meshDeform.mesh.count-1
      plMeshFaceCounts.add(pModel.meshDeform.mesh[tMesh].face.count)
    end repeat
    plMeshFaceCounts = accumulate(plMeshFaceCounts)
    pModel.removeModifier(#meshDeform)

    pModel.AddToWorld()
    if pVisible = true then pModel.parent = pGroup
    else pModel.parent = spv().pVoidGroup

    if not aPreview then me.getBehaviors()
  end if
end makeMesh

on updateTransform me
  if not voidP(pModel) then
    pModel.transform.rotation = vector(pTilt, -pPan, 0)
    pModel.rotate(vector(0,0, -pRoll), #self)
  end if
end


on showLayer me, aLayer
  __showLayer(me, aLayer)
end

on groupProperty me, aProperty
  return __groupProp(me, aProperty)
end

on getBehaviors me
  __getBehaviors(me)
end


--
-- event handling
--

on readyEvent me
  __readyEvent(me, #element)
end

on mouseEnterEvent me
  __mouseEnterEvent(me, #element)
end

on mouseLeaveEvent me
  __mouseLeaveEvent(me, #element)
end

on mouseDownEvent me
  __mouseDownEvent(me, #element)
end

on mouseUpEvent me
  __mouseUpEvent(me, #element)
end

--
-- public properties
--

on setProp me, aProperty, aValue
  case aProperty of
    "pan", "tilt", "roll":
      __setPropPano (me, aProperty, aValue)

    "type","hfov","vfov","hoffset","voffset":
      tProperty = symbol("p" & aProperty)
      if aProperty <> "type" then
        tValue = getValue(#float, aValue, me[tProperty])
      else if not voidP(scriptEx("spv.Mesh." & aValue)) then tValue = aValue
      if not voidP(tValue) and tValue <> me[tProperty] then
        me[tProperty] = tValue
        me.makeMesh()
      end if

    "blend":
      __setPropBlend (me, aValue)

    "zorder":
      __setPropZorder (me, aValue, pModel, "_zScene#")

    "visible","enabled":
      __setPropElement(me, symbol("p"&aProperty), aValue)

    "catchevents":
      __setPropCatch(me, aValue)
  end case
end