-- shared code between node types

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com


on __new me, aParent, aoNode, aArg
  me.parent = aParent

  if voidP(me[#pIsMesh]) then
    me.node = aoNode
    if ["layer"].getPos(me.node.name) = 0 then
      spv().poTour.addNamedObject(me)
    end if
  else me.buildMesh(aArg)

  return me
end


on __initNode me
  if voidP(me.pBuilt) then
    tParent = me.parent

    if me.pElementType = #pano then
      tID = "_pne"
      tlGroups = tParent.plPanoGroups
      tlElements = tParent.plPanoElements
    else
      tID = "_uie"
      tlGroups = tParent.plUIGroups
      tlElements = tParent.plUIElements
    end if

    me.pBuilt = true
    me.pReady = false

    me.pID = getValue(#string, me.node.attributeValue["id"], spv().randomID(tID))

    spv().poTour.addNamedObject(me)

    tParentNode = me.node.parent
    if tParentNode = tParent.node or tParentNode = tParent.parent.poGlobalNode then
      me.poParent = #root
    else me.poParent = tlGroups[tParentNode.attributeValue["id"]][#object]

    me.pSource = tlElements[me.pID][#source]

    me.poImageElement = tParent.getImageNode(me, me.node)
    if not voidP(me.poImageElement) then me.poImageElement.addInterest(me)
  end if
end

on __destroy me, aKeepImage
  if not voidP(me.pBuilt) then
    me.pBuilt = void

    spv().poTour.removeNamedObject(me)
    spv().poMouseManager.removeActiveModel(me)

    me.plBehaviorObjects = []

    if not aKeepImage then me.poImageElement.removeInterest(me)
    destroyObject(me.poMesh)

    -- remove shaders from world - if any
    if not voidP(me.plShaders) then
      repeat with tlShadersV in me.plShaders
        repeat with tlShaders in tlShadersV
          spv().pWorld.deleteShader(tlShaders[#name])
        end repeat
      end repeat
      me.plShaders = VOID
    end if

    if not voidP(me.pModel) then spv().pWorld.deleteModel(me.pModel.name)

    if me.pElementType = #pano then
      me.parent.plPanoElements.deleteOne(me.pID)
    else me.parent.plUIElements.deleteOne(me.pID)
  end if
end

on __removePreview me, aPreview
  if me.pPreviewBuilt then
    -- destroy preview objects
    destroyObject(me.poMesh)

    -- remove shaders from world - if any
    if not voidP(me.plShaders) then
      repeat with tlShadersV in me.plShaders
        repeat with tlShaders in tlShadersV
          spv().pWorld.deleteShader(tlShaders[#name])
        end repeat
      end repeat
      me.plShaders = VOID
    end if

    if not voidP(me.pModel) then spv().pWorld.deleteModel(me.pModel.name)

    me.pPreviewBuilt = void
  end if

  if not aPreview then
    if not voidP(me.poImageElement.plImageLayers["preview"]) then
      me.poImageElement.plImageLayers["preview"][#object].destroy()
      me.poImageElement.plImageLayers.deleteProp("preview")
    end if
  end if
end

on __createShaders me, aAntialias, aIsUIElement, aPreview
  if not getValue(#boolean, me.node.attributeValue["silent"], false) then
    tType = "pano"
    if aIsUIElement then tType = "ui"
    spv().poLogger.show("Building shaders and mesh for " & tType & "element '"& me.node.attributeValue["id"] &"'", #info)
  end if

  tAntialias = getValue(#boolean, me.node.attributeValue["antialias"], aAntialias)
  me.pBlend = max(0, min(100, getValue(#float, me.node.attributeValue["blend"], 100)))

  -- create shaders from image(layers)
  tlImageLayers = me.poImageElement.plImageLayers
  if not aPreview then
    tlShaderLayers = [tlImageLayers["base"], \
                      tlImageLayers["hover"], \
                      tlImageLayers["press"], \
                      tlImageLayers["disabled"]]
  else tlShaderLayers = [tlImageLayers["preview"]]

  tModelLayer = VOID
  repeat with tLayer in tlShaderLayers
    if not voidP(tLayer) then
      tModelLayer = tLayer[#object]
      exit repeat
    end if
  end repeat

  tTransparentTexture = spv().pTransparentTexture

  if not voidP(tModelLayer) then
    spv().pDontCountMS = true

    -- create shaders
    tVTextures = tModelLayer.plTextures.count
    tHTextures = tModelLayer.plTextures[1].count

    -- turn off filtering and apply scaledown
    if tAntialias = false then
      repeat with tlLayer in tlShaderLayers
        if not voidP(tlLayer) then
          repeat with tV = 1 to tVTextures
            repeat with tH = 1 to tHTextures
              tTexture = tlLayer[#object].plTextures[tV][tH][#texture]
              if tAntiAlias = false then
                if aIsUIElement then
                  tTexture.quality = #low
                  tTexture.nearFiltering = false
                else tTexture.quality = #lowFiltered
              end if
            end repeat
          end repeat
        end if
      end repeat
    end if

    me.plShaders = []
    tBaseName = "_shd_" & me.pID

    me.pSeamFix = float(tModelLayer.pSeamFix)

    repeat with tV = 1 to tVTextures
      tlShadersV = []
      repeat with tH = 1 to tHTextures
        tChunkName = tBaseName & "(" & tH & "," & tV & ")"
        tShader = spv().pWorld.newShader(tChunkName, #standard)

        tShader.blend = me.pBlend

        tShader.emissive = rgb(255, 255, 255)
        tShader.ambient = rgb(0, 0, 0)
        tShader.diffuse = rgb(0, 0, 0)
        tShader.specular = rgb(0, 0, 0)

        tShader.textureRepeat = false
        tShader.textureMode = #none

        tShader.textureTransform.position = vector(-me.pSeamFix / tModelLayer.plTextures[tV][tH][#size].locH, me.pSeamFix / tModelLayer.plTextures[tV][tH][#size].locV,0)

        if not voidP(tlShaderLayers[1]) then
          tShader.texture = tlShaderLayers[1][#object].plTextures[tV][tH][#texture]
        else tShader.texture = tTransparentTexture

        tlShadersV.add([#name:tChunkName, #shader:tShader, \
          #rect:tModelLayer.plTextures[tV][tH][#rect], \
          #size:tModelLayer.plTextures[tV][tH][#size]])
      end repeat
      me.plShaders.add(tlShadersV)
    end repeat
  else
    -- no prescribed shaders (behavior only?)
    tShaderName = "_shd_" & me.pID
    tShader = spv().pWorld.newShader(tShaderName, #standard)
    tShader.texture = tTransparentTexture

    me.plShaders = [[[#name:tShaderName, #shader:tShader, \
      #rect:rect(0,0, tTransparentTexture.width, tTransparentTexture.height), \
    #size:point(tTransparentTexture.width, tTransparentTexture.height) ]]]

    me.poImageElement.pWidth = tTransparentTexture.width
    me.poImageElement.pHeight = tTransparentTexture.height
  end if
end

on __showLayer me, aLayer
  repeat with tV = 1 to me.plShaders.count
    repeat with tH = 1 to me.plShaders[1].count
      if not voidP(aLayer) then
        me.plShaders[tV][tH][#shader].texture = aLayer[#object].plTextures[tV][tH][#texture]
      else me.plShaders[tV][tH][#shader].texture = spv().pTransparentTexture
    end repeat
  end repeat
end

on __groupProp me, aProperty
  tAllSet = true

  tParent = me.poParent
  repeat while tParent <> #root
    if tParent[aProperty] = false then
      tAllSet = false
      exit repeat
    end if
    tParent = tParent.poParent
  end repeat

  return tAllSet
end


on __getBehaviors me
  if not voidP(me.poMesh) then
    if not voidP(me.poMesh.pMesh) then
      me.plBehaviorObjects = me.parent.getBehaviorObjects(me.node)

      if me.plBehaviorObjects.count>0 or \
       not voidP(me.poImageElement.plImageLayers["hover"]) or \
       not voidP(me.poImageElement.plImageLayers["press"]) then

        me.pHasActivity = true

        repeat with toBehavior in me.plBehaviorObjects
          if not (voidP(toBehavior[#object].plActions["press"]) and \
           voidP(toBehavior[#object].plActions["release"])) then
            me.pHasClickAction = true
          end if
        end repeat

        if me.pEnabled = true then
          if me.pVisible = true and me.groupProperty(#pVisible) then spv().poMouseManager.addActiveModel(me)
          --
        else
          tShowLayer = me.poImageElement.plImageLayers["disabled"]
          if not voidP(tShowLayer) then me.showLayer(tShowLayer)
        end if
      else if me.pCatchEvents =true then
        if me.pVisible = true and me.groupProperty(#pVisible) = true then spv().poMouseManager.addActiveModel(me)
        --
      else
        me.showLayer(me.poImageElement.plImageLayers["base"])
        spv().poMouseManager.removeActiveModel(me)
      end if
    end if
  end if
end

on __readyEvent me, aType
  tAllDone = true

  if aType = #group then
    if me.pElementType = #pano then
      tlElementLists = [me.parent.plPanoElements, me.parent.plPanoGroups]
    else tlElementLists = [me.parent.plUIElements, me.parent.plUIGroups]

    repeat with tlElements in tlElementLists
      repeat with tlElement in tlElements
        if not voidP(tlElement[#object]) then
          if tlElement[#object].poParent = me then
            if tlElement[#object].pReady = false then
              tAllDone = false
              exit repeat
            end if
          end if
        end if
      end repeat
      if tAllDone = false then exit repeat
    end repeat
  end if

  if tAllDone then
    me.pReady = true
    spv().poTour.queueAction(me, "ready")
    if me.poParent <> #root then me.poParent.readyEvent()

    if aType = #element then
      me.parent.elementReady(me.pID, me.pElementType)
    end if
  end if
end


on __mouseEnterEvent me, aType
  me.pMouseHoverMe = true

  if aType = #element then
    if not me.pMousePressMe then
      tShowLayer = me.poImageElement.plImageLayers["hover"]
    else
      tShowLayer = me.poImageElement.plImageLayers["press"]
      if voidP(tShowLayer) then tShowLayer = me.poImageElement.plImageLayers["hover"]
    end if

    if not voidP(tShowLayer) then me.showLayer(tShowLayer)
  end if

  spv().poTour.queueAction(me, "enter")
  if me.poParent <> #root then me.poParent.mouseEnterEvent()
end

on __mouseLeaveEvent me, aType
  me.pMouseHoverMe = false

  if aType = #element then
    tShowLayer = me.poImageElement.plImageLayers["base"]
    me.showLayer(tShowLayer)
  end if

  spv().poTour.queueAction(me, "leave")
  if me.poParent <> #root then me.poParent.mouseLeaveEvent()
end

on __mouseDownEvent me, aType
  me.pMousePressMe = true

  if aType = #element then
    tShowLayer = me.poImageElement.plImageLayers["press"]
    if not voidP(tShowLayer) then me.showLayer(tShowLayer)
  end if

  spv().poTour.queueAction(me, "press")
  if me.poParent <> #root then me.poParent.mouseDownEvent()
end

on __mouseUpEvent me, aType
  me.pMousePressMe = false

  if aType = #element then
    tDoAction = false
    if me.pMouseHoverMe then
      tShowLayer = me.poImageElement.plImageLayers["hover"]
      if voidP(tShowLayer) then tShowLayer = me.poImageElement.plImageLayers["base"]

      tDoAction = true
    else tShowLayer = me.poImageElement.plImageLayers["base"]

    me.showLayer(tShowLayer)
  else tDoAction = true

  if tDoAction then spv().poTour.queueAction(me,"release")
  if me.poParent <> #root then me.poParent.mouseUpEvent()
end

on __setPropPano me, aProperty, aValue
  tValue = myfloat(aValue)
  if floatP(tValue) then
    me[symbol("p"&aProperty)] = tValue
    me.updateTransform()
  end if
end

on __setPropUI me, aValue, aPropertyVar1,aPropertyVar2, aPropertyIndex, aPropertyVal
  tFP = floatOrPercentage(aValue,void)
  if not voidP(me[aPropertyVar1]) and not voidP(tFP[1]) then
    if not voidP(aPropertyVal) then me[aPropertyVal] = aValue
    me[aPropertyVar1][aPropertyIndex] = tFP[1]
    me[aPropertyVar2][aPropertyIndex] = tFP[2]

    me.updateLoc()
  end if
end

on __setPropElement me, aProperty, aValue
  me[aProperty] = getValue(#boolean, aValue, not me[aProperty])
  if not voidP(me.pModel) then
    tActive = (me.pHasActivity = true or me.pCatchEvents = true)
    if me[aProperty] then
      if aProperty = #pVisible then
        me.pModel.parent = me.pGroup
      else if tActive then
        me.showLayer(me.poImageElement.plImageLayers["base"])
      end if
      if tActive then spv().poMouseManager.addActiveModel(me)
      --
    else
      if aProperty = #pVisible then
        me.pModel.parent = spv().pVoidGroup
        me.showLayer(me.poImageElement.plImageLayers["base"])
      else if tActive then
        tShowLayer = me.poImageElement.plImageLayers["disabled"]
        if not voidP(tShowLayer) then me.showLayer(tShowLayer)
      end if
      if tActive then spv().poMouseManager.removeActiveModel(me)
      --
    end if
  end if
end

on __setPropGroup me, aProperty, aValue
  me[aProperty] = getValue(#boolean, aValue, not me[aProperty])
  if not voidP(me.pGroup) then

    if me.pElementType = #pano then
      tlElements = me.parent.plPanoElements
    else tlElements = me.parent.plUIElements
    tlChildren = me.node.getElementByName(string(me.pElementType)&"element")

    if me[aProperty] = true then
      if aProperty = #pVisible then me.pGroup.parent = me.pParentGroup

      repeat with tChild in tlChildren
        toChild = tlElements[tChild.attributeValue["id"]]
        if not voidP(toChild) then
          toChild = toChild[#object]
          if toChild[aProperty] = true and toChild.groupProperty(aProperty)=true then
            if (toChild.pHasActivity = true and toChild.pEnabled = true) or toChild.pCatchEvents = true then
              if aProperty = #pEnabled then
                toChild.showLayer(toChild.poImageElement.plImageLayers["base"])
              end if
              spv().poMouseManager.addActiveModel(toChild)
            end if
          end if
        end if
      end repeat
    else
      if aProperty = #pVisible then me.pGroup.parent = spv().pVoidGroup

      repeat with tChild in tlChildren
        toChild = tlElements[tChild.attributeValue["id"]]
        if not voidP(toChild) then
          toChild = toChild[#object]
          if aProperty = #pEnabled then
            tShowLayer = toChild.poImageElement.plImageLayers["disabled"]
            if voidP(tShowLayer) then tShowLayer = toChild.poImageElement.plImageLayers["base"]
            toChild.showLayer(tShowLayer)
          end if
          if toChild.pHasActivity = true and toChild.pEnabled = true then
            toChild.showLayer(toChild.poImageElement.plImageLayers["base"])
          end if
          spv().poMouseManager.removeActiveModel(toChild)
        end if
      end repeat
    end if
  end if
end


on __setPropZorder me, aValue, aObject, aRoot
  if not voidP(aObject) then
    tZorder = min(9, max(0, integer(aValue)))
    me[#pZOrder]=tZorder

    if me.poParent = #root then
      me.pGroup = spv().pWorld.group(aRoot & string(tZorder))
    else me.pGroup = spv().pWorld.group(me.poParent.pZBaseName & string(tZorder))

    if me.pVisible = true then aObject.parent = me.pGroup
  end if
end

on __setPropBlend me, aValue
  tValue = float(min(100, max(0, myfloat(aValue))))
  if floatP(tValue) then
    me.pBlend = tValue
    if not voidP(me.plShaders) then
      repeat with tV = 1 to me.plShaders.count
        tlShaders = me.plShaders[tV]
        repeat with tH = 1 to tlShaders.count
          tlShaders[tH][#shader].blend = tValue
        end repeat
      end repeat
    end if
  end if
end

on __setPropCatch me, aValue
  me.pCatchEvents = getValue(#boolean, aValue, not me.pCatchEvents)
  if not voidP(me.pModel) then
    if not(me.pCatchEvents or me.pHasActivity) then
      spv().poMouseManager.removeActiveModel(me)
    else if me.pVisible and me.pEnabled then
      spv().poMouseManager.addActiveModel(me)
    end if
  end if
end




on __buildMesh me, alFaces, alVertices, alTextureCoordinates, aUseFaceVertices
  me.pMesh = spv().pWorld.newMesh("_msh_" & me.parent.pID, alFaces.count, alVertices.count, 0, 0, alTextureCoordinates.count)
  me.pMesh.vertexList = alVertices
  me.pMesh.textureCoordinateList = alTextureCoordinates

  repeat with tFaceNr = 1 to alFaces.count
    tFace = alFaces[tFaceNr]

    me.pMesh.face[tFaceNr].vertices  = tFace.vertices
    me.pMesh.face[tFaceNr].shader = tFace.shader

    if not aUseFaceVertices then
      me.pMesh.face[tFaceNr].textureCoordinates = tFace.textureCoordinates
    else me.pMesh.face[tFaceNr].textureCoordinates = tFace.vertices
  end repeat

  me.pMesh.generateNormals(#flat)
  me.pMesh.build()
end
