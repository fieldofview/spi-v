-- <list> node
-- Manages <value> nodes, interpolates, etc

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pID

property pBuilt
property plBehaviorObjects

property pIndex, pValue

property plIndices, plValues

property pIndexMax, pIndexMin
property pValueMax, pValueMin

property pInterpolator
property pIndexStep
property pWrap

property pIsNumeric

property plSortedList
property plCMIndices, plCMValues

on new me,  _parent, aoNode
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void
    
    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []
  end if
end

on initNode me
  if voidP(pBuilt) then
    pBuilt = true
    
    pID = node.attributeValue["id"]
    
    me.getBehaviors()
    
    pIsNumeric = true
    
    pInterpolator = getValue(#string, node.attributeValue["interpolator"],"none")
    
    pIndexStep = max(0.01, getValue(#integer, node.attributeValue["indexstep"], 1))
    pWrap = getValue(#boolean, node.attributeValue["wrap"], false)
    
    plIndices = []
    plValues = []
    
    tlValueNodes = node.getElementByName("value")
    repeat with toValueNode in tlValueNodes
      tIndex = toValueNode.attributeValue["index"]
      if floatP(myFloat(tIndex)) then tIndex = myFloat(tIndex)
      else if voidP(tIndex) then
        if not voidP(pIndexMax) then
          tIndex = pIndexMax + pIndexStep
        else tIndex = 1
      else pIsNumeric = false
      
      if tIndex > pIndexMax or voidP(pIndexMax) then
        pIndexMax = tIndex
      end if
      if tIndex < pIndexMin or voidP(pIndexMin) then
        pIndexMin = tIndex
      end if
      
      tValue = toValueNode.getText()
      if floatP(myFloat(tValue)) then tValue = myFloat(tValue)
      else pIsNumeric = false
      
      plIndices.add(tIndex)
      plValues.add(tValue)
    end repeat
    
    pIndex = pIndexMin
    me.interpolateValue()
  end if
end

on interpolateValue me
  if plValues.count > 0 then
    tValue = pValue
    tNearest = nearestValue(plIndices, pIndex)
    
    if pIsNumeric then
      if tNearest[2] = true then pValue = plValues[max(1,tNearest[3])]
      else
        if voidP(plSortedList) then
          plSortedList = []
          repeat with tIndex = 1 to plIndices.count
            plSortedList.add([plIndices[tIndex], plValues[tIndex], tIndex])
          end repeat
          plSortedList.sort()
          plCMIndices = void
        end if
        case pInterpolator of
          "linear":
            if not voidP(tNearest[2]) then
              repeat with tIndex = 1 to plSortedList.count
                if plSortedList[tIndex][1] = tNearest[1] then
                  tIndexOffset = float(pIndex - plSortedList[tIndex][1]) / (plSortedList[tIndex+1][1] - plSortedList[tIndex][1])
                  pValue = plSortedList[tIndex][2] + tIndexOffset * (plSortedList[tIndex+1][2]-plSortedList[tIndex][2])
                  exit repeat
                end if
              end repeat
            else pValue = plValues[tNearest[3]]
          "spline":
            if voidP(plCMIndices) then
              plCMIndices = []
              plCMValues = []
              tlCMList = plSortedList.duplicate()
              tlCMList.add(2*tlCMList[tlCMList.count]-tlCMList[tlCMList.count-1])
              tlCMList.add(2*tlCMList[1]-tlCMList[2])
              
              repeat with tPair = 1 to tlCMList.count
                plCMIndices.add(tlCMList[tPair][1])
                plCMValues.add(tlCMList[tPair][2])
              end repeat
            end if
            
            -- make sure requested key is inside key ranges
            tIndex = max(plCMIndices[2], min(plCMIndices[plCMIndices.count-1], pIndex))
            
            -- get nearest key index
            tlCMIndices = plCMIndices.duplicate()
            tlCMIndices.add(tIndex)
            tlCMIndices.sort()
            tCMIndex = max(2,tlCMIndices.getOne(tIndex)-1)
            
            tSteps = plCMIndices[tCMIndex+1] - plCMIndices[tCMIndex]
            
            if tSteps=0 then pValue = plCMValues[tCMIndex]
            else
              pValue = me.catmull(tCMIndex,float(tIndex - plCMIndices[tCMIndex])/tSteps)
            end if
            
          otherwise:
            pValue = plValues[max(1,tNearest[3])]
        end case
      end if
    else
      if not voidP(tNearest[2]) then
        if tNearest[3] > 0 then
          if tNearest[3] <= plIndices.count then
            pValue = plValues[tNearest[3]]
          else pValue = plValues[plIndices.count]
        else pValue = plValues[1]
      else
        if plValues.count>0 then
          if tNearest[3] = 0 then
            pValue = plValues[1]
          else pValue = plValues[plIndices.count]
        end if
      end if
    end if
    
    if tValue<>pValue then
      spv().poTour.queueAction(me, "change",true)
    end if
  end if
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end

on catmull me, i, t
  tValue=0
  t=float(t)
  
  repeat with j = -2 to 1
    tV = plCMValues[i+j+1]
    
    case j of
      -2: tC = ((-t+2)*t-1)*t/2
      -1: tC = (((3*t-5)*t)*t+2)/2
      0:  tC = ((-3*t+4)*t+1)*t/2
      1:  tC = ((t-1)*power(t,2))/2
    end case
    
    tValue=tValue + tC*tV
  end repeat
  
  return tValue
end

--
-- public calls
--


on setProp me, aProperty, aValue
  case aProperty of
    "index":
      tIndex = pIndex
      if pIsNumeric then
        case aValue of:
          "_next":
            pIndex = pIndex + pIndexStep
            if pIndex > pIndexMax and pWrap then pIndex = pIndexMin
          "_previous":
            pIndex = pIndex - pIndexStep
            if pIndex > pIndexMax and pWrap then pIndex = pIndexMin
          "_last":
            pIndex = pIndexMax
          "_first":
            pIndex = pIndexMin
          "_random":
            pIndex = plIndices[random(1, plIndices.count)]
          otherwise:
            tValue = myFloat(aValue)
            if not floatP(tValue) then
              pIsNumeric = false
            else if pWrap then
              tValue = clip(tValue, pIndexMin, pIndexMax)
            end if
            pIndex = tValue
        end case
      else
        tNearest = nearestValue(plIndices, pIndex)
        case aValue of:
          "_next":
            if tNearest[3] < plIndices.count then pIndex = plIndices[tNearest[3]+1]
            else if pWrap then pIndex = plIndices[1]
          "_previous":
            if tNearest[2] = true then tNearest[3] = tNearest[3]-1
            if tNearest[3] >= 1 then pIndex = plIndices[tNearest[3]]
            else if pWrap then pIndex = plIndices[plIndices.count]
          "_last":
            pIndex = plIndices[plIndices.count]
          "_first":
            pIndex = plIndices[1]
          "_random":
            if plIndices.count > 1 then
              tIndex = pIndex
              repeat while tIndex = pIndex
                pIndex = plIndices[random(1, plIndices.count)]
              end repeat
            else pIndex = 1
          otherwise:
            pIndex = aValue
        end case
      end if
      if pIndex <> tIndex then
        spv().poTour.queueAction(me, "index",true)
      end if
      
      me.interpolateValue()
    "value":
      pValue = aValue
      
      if not floatP(myFloat(aValue)) then
        pIsNumeric = false
      end if
      
      tNearest = nearestValue(plIndices, pIndex)
      if tNearest[2] <> true then
        plIndices.addAt(tNearest[3] + 1, pIndex)
        if pIndex > pIndexMax or voidP(pIndexMax) then pIndexMax = pIndex
        if pIndex < pIndexMin or voidP(pIndexMin) then pIndexMin = pIndex
        plValues.addAt(tNearest[3] + 1, pValue)
        if pValue > pValueMax or voidP(pValueMax) then pValueMax = pValue
        if pValue < pValueMin or voidP(pValueMin) then pValueMin = pValue
        nothing
      else plValues[tNearest[3]] = aValue
      
      plSortedIndices = void
    "interpolator":
      pInterpolator = aValue
      plSortedIndices = void
      me.interpolateValue()
    "wrap":
      pWrap = getValue(#boolean, aValue, not pWrap)
  end case
end
