-- <uigroup> node

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property node

property pBuilt
property pReady

property pID

property pElementType

property pGroup
property poParent
property pParentGroup

property plBehaviorObjects

property pZBaseName

property pDimension, pPropDimension
property pActualDimension

property pOffset, pPropOffset
property pAlign

property pZorder

property pRotation

property pVisible, pEnabled

property pMouseHoverMe, pMousePressMe

on new me,  _parent, aoNode
  pElementType = #ui
  return __new(me, _parent, aoNode)
end

on destroy me
  if not voidP(pBuilt) then
    pBuilt = void

    spv().poTour.removeNamedObject(me)
    plBehaviorObjects = []

    -- remove group and subgroups
    repeat with tLevel = 0 to 9
      spv().pWorld.deleteGroup(pZBaseName & string(tLevel))
    end repeat
    spv().pWorld.deleteGroup("_uig_" & pID)
  end if
end


on initNode me
  if voidP(pBuilt) then
    pBuilt = true
    pReady = false

    pID = getValue(#string, node.attributeValue["id"])

    tParentNode = node.parent
    if tParentNode = parent.node or tParentNode = parent.parent.poGlobalNode then
      poParent = #root
    else poParent = parent.plUIGroups[tParentNode.attributeValue["id"]][#object]

    me.getBehaviors()

    pDimension = [0,0]
    pPropDimension = [false, false]

    -- get parameters from node
    tFP = floatOrPercentage(node.attributeValue["width"], 0)
    pDimension[1] = tFP[1]
    pPropDimension[1] = tFP[2]

    tFP = floatOrPercentage(node.attributeValue["height"], 0)
    pDimension[2] = tFP[1]
    pPropDimension[2] = tFP[2]


    pAlign = point(0,0)
    pAlign.locH = 1*["center":0,"right":1,"left":-1][string(node.attributeValue["halign"])]
    pAlign.locV = 1*["center":0,"top":1,"bottom":-1][string(node.attributeValue["valign"])]

    pOffset = []
    pPropOffset = []

    tFP = floatOrPercentage(node.attributeValue["hoffset"],0)
    pOffset[1] = tFP[1]
    pPropOffset[1] = tFP[2]

    tFP = floatOrPercentage(node.attributeValue["voffset"],0)
    pOffset[2] = tFP[1]
    pPropOffset[2] = tFP[2]


    pVisible = getValue(#boolean, node.attributeValue["visible"], true)

    pGroup = spv().pWorld.newGroup("_uig_" & pID)
    pZBaseName = "_uiz_" & pID & "_#"
    repeat with tLevel = 0 to 9
      tGroup = spv().pWorld.newGroup(pZBaseName & string(tLevel))
      tGroup.parent = pGroup
    end repeat

    pZorder = node.attributeValue["zorder"]
    if ["_spv.branding","_spv.management","_spv.preview"].getOne(pZorder) = 0 then pZorder = string(max(0,min(9, getValue(#integer, pZorder, 0))))

    if poParent = #root then
      pParentGroup = spv().pWorld.group("_zOverlay#"&pZorder)
    else
      if not integerP(value(pZorder)) then pZorder = "0"
      pParentGroup = spv().pWorld.group(poParent.pZBaseName & string(pZorder))
    end if

    me.updateLoc()

    pRotation = getValue(#float, node.attributeValue["rotation"], 0)
    pGroup.transform.rotation.z = -pRotation

    if pVisible = true then pGroup.parent = pParentGroup
    else pGroup.parent = spv().pVoidGroup

    pGroup.AddToWorld()
  end if
end

on updateLoc me
  -- update location within sprite after sprite dimensions have changed
  if not voidP(pGroup) then
    if poParent = #root then
      tParentWidth = spv().width
      tParentHeight = spv().height
    else
      tParentWidth = poParent.pActualDimension.locH
      tParentHeight = poParent.pActualDimension.locV
    end if

    -- calculate actual width and offset
    tPreviousDimension = pActualDimension
    pActualDimension = point(pDimension[1], pDimension[2]).duplicate()
    if pPropDimension[1] then pActualDimension.locH = float(tParentWidth * pActualDimension.locH) / 100
    if pDimension[1] < 0 then pActualDimension.locH = tParentWidth + pActualDimension.locH

    if pPropDimension[2] then pActualDimension.locV = float(tParentHeight * pActualDimension.locV) / 100
    if pDimension[2] < 0 then pActualDimension.locV = tParentHeight + pActualDimension.locV

    tOffset = point(pOffset[1], pOffset[2]).duplicate()
    if pPropOffset[1] then tOffset.locH = float(tParentWidth *  pOffset[1]) / 100
    if pPropOffset[2] then tOffset.locV = float(tParentHeight * pOffset[2]) / 100

    tPosition = vector(0,0,0)
    tPosition.x = pAlign.locH * (float(tParentWidth -  pActualDimension.locH)/2) + tOffset.locH
    tPosition.y = pAlign.locV * (float(tParentHeight - pActualDimension.locV)/2) + tOffset.locV
    pGroup.transform.position = tPosition

    if pID = "_viewport" then
      -- special UI group
      tPosition = pGroup.getWorldTransform().position

      tLeft =  ( tPosition.x + float(tParentWidth -  pActualDimension.locH)/2)
      tTop  =  (-tPosition.y + float(tParentHeight - pActualDimension.locV)/2)

      if pActualDimension.locH = 0 then pActualDimension.locH = tParentWidth
      if pActualDimension.locH < 1 then pActualDimension.locH = 1

      if pActualDimension.locV = 0 then pActualDimension.locV = tParentHeight
      if pActualDimension.locV < 1 then pActualDimension.locV = 1

      tPosition.x = pAlign.locH * (float(tParentWidth -  pActualDimension.locH)/2) + tOffset.locH
      tPosition.y = pAlign.locV * (float(tParentHeight - pActualDimension.locV)/2) + tOffset.locV
      pGroup.transform.position = tPosition

      spv().pSceneCamera.rect = rect(tLeft, tTop, tLeft+pActualDimension.locH, tTop+pActualDimension.locV)
    end if

    -- update children if necessary
    if not voidP(tPreviousDimension) and tPreviousDimension <> pActualDimension then
      repeat with tElements in [#plUIGroups, #plUIElements]
        repeat with tlElements in parent[tElements]
          if tlElements[#object].poParent = me then tlElements[#object].updateLoc()
        end repeat
      end repeat
    end if
  end if
end

on updateTransform me
  if not voidP(pGroup) then pGroup.transform.rotation.z = -pRotation
end

on getBehaviors me
  plBehaviorObjects = parent.getBehaviorObjects(node)
end

--
-- event handling
--

on readyEvent me
  __readyEvent(me, #group)
end

on mouseEnterEvent me
  __mouseEnterEvent(me, #group)
end

on mouseLeaveEvent me
  __mouseLeaveEvent(me, #group)
end

on mouseDownEvent me
  __mouseDownEvent(me, #group)
end

on mouseUpEvent me
  __mouseUpEvent(me, #group)
end

--
-- public calls
--

on setProp me, aProperty, aValue
  case aProperty of
    "visible", "enabled":
      __setPropGroup (me, symbol("p" & aProperty), aValue)

    "halign":
      pAlign.locH = 1*["center":0,"right":1,"left":-1][srting(aValue)]
      me.updateLoc()

    "valign":
      pAlign.locV = 1*["center":0,"top":1,"bottom":-1][string(aValue)]
      me.updateLoc()

    "hoffset":
      __setPropUI (me, aValue, #pOffset,#pPropOffset, 1, #pHOffset)

    "voffset":
      __setPropUI (me, aValue, #pOffset,#pPropOffset, 2, #pVOffset)

    "width":
      __setPropUI (me, aValue, #pDimension,#pPropDimension, 1, #pWidth)

    "height":
      __setPropUI (me, aValue, #pDimension,#pPropDimension, 2, #pHeight)

    "rotation":
      __setPropPano (me, aProperty, aValue)

    "zorder":
      __setPropZorder (me, aValue, pGroup, "_zOverlay#")
  end case
end