-- Set up the viewer
-- Creates sprite and world, loads and parses XML file, etc

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property pVersion

property pSWV
property pD85
property plSettings

property pWindow
property poLDMscript

property pBrowserOffset

property pActive

property pSprite
property pWorld
property pSceneCamera
property pOverlayCamera

property pSWFSprite

property pVoidGroup
property pTransparentTexture

property pSourceURL
property pSourceArgs
property pSimpleMode

property poKeyHandler
property poMouseManager
property poLocalConnection
property poDownloader
property poLogger
property poNavigator
property poLicenseManager
property poPlugin

property poTour

property poXML

property pLastMS
property pFrameFactor
property plFrameMS
property pFPS
property pDontCountMS

property pLockRenderer

property pLastClick, pLastClickOffset
property pLastKey, pLastKeyOffset
property pLastRoll

property pForeColor

property pError

property plTimeouts

property pLastID

property pThis -- used by API instead of global variable

on new me
  me.init()
  
  return me
end

on endSprite me
  me.destroy()
end

on exitFrame me
  me.doTimeouts()
  me.getFPS()
  me.getLastEvents()
  
  if pSWV >= 10 then
    tMinimised = (the activeWindow).sizeState = #minimized
  else tMinimised = false
  
  if tMinimised = false then
    if not (voidP(poTour) or pError) then
      if the active3drenderer = #software and not(pLockRenderer) then preferred3dRenderer = #auto
      me.updateRect()
    end if
  else
    if voidP(pWindow) then
      pSprite.rect = rect(0,0,0,0)
    end if
  end if
end

on keyDown me
  if not voidP(poKeyHandler) then poKeyHandler.handleKey(the keyCode, the commandDown, the optionDown, the shiftDown)
end

on activate me, aActive
  if not voidP(aActive) then pActive = aActive
  
  if not voidP(poMouseManager) then
    poMouseManager.pMouseHoverObject = void
    poMouseManager.pMouseDownObject = void
  end if
end

on init me
  pVersion = "1.4.10" --�
  
  plTimeouts = [:]
  pActive = true
  plSettings = [:]
  
  the itemDelimiter="."
  tSWVersion = (the environment)[#productVersion]
  if not voidP(tSWVersion) then
    tSWVersion = tSWVersion.word[1]
    if tSWVersion.item.count > 2 then tSWVersion = tSWVersion.item[1] & "." & tSWVersion.item[2]
    pSWV = myfloat(tSWVersion)
  end if
  pD85 = (pSWV <= 8.5) -- for legacy (undocumented) use
  
  
  pWindow = the activeWindow
  tell pWindow
    tPath = the moviePath
    tName = the movieName
  end tell
  if tPath <> the moviePath or tName <> the movieName then pWindow = VOID
  
  -- create util sprite
  pSWFSprite = sprite(2)
  pSWFSprite.puppet = true
  pSWFSprite.member = member("swf-utils")
  pSWFSprite.locZ = 1
  
  -- create sprite
  pSprite = spv()
  pSprite.puppet = true
  pSprite.scriptInstanceList.add(me)
  pSprite.locZ = 1000
  
  pSprite.rect = rect(0,0,2,2)
  
  -- fix background color
  if not voidP(pWindow) then
    if externalParamName("bgColor") = "bgColor" then
      tColor = rgb(externalParamValue("bgColor"))
    else tColor = (the stage).bgColor
    pWindow.bgColor = tColor
  else tell the activeWindow to tColor = (the stage).bgColor
  
  (the stage).bgColor = tColor
  pSprite.bgColor = tColor
  
  if colorToValue(pSprite.bgColor) < .5 then
    pForeColor = "FFFFFF"
  else pForeColor = "000000"
  
  -- create _PanelBG
  tPanelBgImg = image(256,256,32)
  tPanelBgImg.useAlpha = true
  tPanelBgImg.fill(tPanelBgImg.rect, [#shapeType: #rect, #color: rgb(255,255,255)])
  tPanelBgImg.draw(tPanelBgImg.rect, [#shapeType: #rect, #color: rgb(0,0,0), #lineSize: 1])
  tPanelBgImg.setAlpha(192)
  
  if not voidP(member("_PanelBG")) then member("_PanelBG").erase()
  tPanelBG = new(#bitmap)
  tPanelBG.name = "_PanelBG"
  tPanelBG.image = tPanelBgImg
  
  -- create log manager
  poLogger = new(script "spv.Logger", me)
  --poLogger.pSilent = true
  
  tVisit = "Please visit http://www.adobe.com/shockwave/download"
  if tSWVersion<8.5 then poLogger.show("You need a newer version of Shockwave to display this content."&&tVisit, #error)
  if voidP(xtra("Shockwave3dAsset")) then poLogger.show("Missing Shockwave 3d Xtra. You may need to reinstall Shockwave."&&tVisit, #error)
  
  -- check for required Xtras
  --  tMovieXtraList = the movieXtraList
  --  tXtraList = the XtraList
  --
  --  repeat with tMovieXtra in tMovieXtraList
  --    tXtraFound = false
  --    repeat with tXtra in tXtraList
  --      if tXtra.fileName = tMovieXtra.fileName then tXtraFound = true
  --    end repeat
  --    if tXtraFound = false then
  --      put "Xtra not found:" & tMovieXtra.fileName
  --      exit repeat
  --    end if
  --  end repeat
  
  -- create empty 3d member
  if not voidP(member("_World")) then member("_World").erase()
  pWorld = new(#shockwave3d)
  pWorld.name = "_World"
  
  repeat with tLight = pWorld.light.count down to 1
    pWorld.deleteLight(pWorld.light(tLight).name)
  end repeat
  
  pWorld.bgColor = (the stage).bgColor
  
  pSprite.member = pWorld
  updateStage
  
  -- init cameras
  -- don't render anything in the default camera
  tCamera = pWorld.camera(1)
  tCamera.removeFromWorld()
  tCamera.rootNode = tCamera
  tCamera.colorBuffer.clearAtRender = false
  
  -- create scene camera and groups
  pSceneCamera = pWorld.newCamera("_SceneCamera")
  pSceneCamera.rect = rect(0,0, pSprite.rect.width, pSprite.rect.height)
  tSceneGroup = pWorld.newGroup("_Scene")
  repeat with tGroupName in ["_spv.preview",0,1,2,3,4,5,6,7,8,9]
    tGroup = pWorld.newGroup("_zScene#" & string(tGroupName))
    tGroup.parent = tSceneGroup
  end repeat
  pSceneCamera.rootNode = tSceneGroup
  pSceneCamera.colorBuffer.clearValue = pWorld.bgColor
  pSceneCamera.colorBuffer.clearAtRender = false
  pSprite.addCamera(pSceneCamera)
  
  -- create overlay camera and groups
  pOverlayCamera = pWorld.newCamera("_OverlayCamera")
  pOverlayCamera.projection = #orthographic
  
  pOverlayCamera.colorBuffer.clearAtRender = false
  
  tOverlayGroup = pWorld.newGroup("_Overlays")
  repeat with tGroupName in ["_spv.preview",0,1,2,3,4,5,6,7,8,9,"_spv.management","_spv.branding"]
    tGroup = pWorld.newGroup("_zOverlay#" & string(tGroupName))
    tGroup.parent = tOverlayGroup
  end repeat
  
  pOverlayCamera.rootNode = tOverlayGroup
  pSprite.addCamera(pOverlayCamera)
  
  -- weird ATI fix to make sure overlay camera displays
  tATIfix = pWorld.newModel("_spv.ATIfix", pWorld.modelResource(1))
  tATIfix.transform.scale.x = 4094
  tATIfix.parent = tOverlayGroup
  tATIfix.visibility = #none
  
  -- create void group (removed from world)
  pVoidGroup = pWorld.newGroup("_Void")
  pVoidGroup.removeFromWorld()
  
  -- create transparent texture
  tEmptyImage = image(4,4,32)
  tEmptyImage.setAlpha(0)
  tEmptyImage.useAlpha=true
  pTransparentTexture = pWorld.newTexture("_spv.transparent", #fromImageObject, tEmptyImage)
  pTransparentTexture.quality = #low
  pTransparentTexture.nearFiltering = false
  pTransparentTexture.renderFormat = #rgba5551
  
  -- create _PanoBG
  tPanoBGTemplate = member("bgTemplate").image
  if not voidP(member("_PanoBG")) then member("_PanoBG").erase()
  tPanoBG = new(#bitmap)
  tPanoBG.name = "_PanoBG"
  tPanoBG.image = image(tPanoBGTemplate.width, tPanoBGTemplate.height, 24)
  tPanoBG.image.fill(tPanoBG.image.rect, pWorld.bgColor)
  tPanoBG.image.copyPixels(tPanoBGTemplate, tPanoBG.rect, tPanoBG.rect, [#blend:20])
  
  -- get parameters
  case the runmode of
    "Plugin":
      -- see if we can get a userAgent
      pBrowserOffset = 0
      poPlugin = new(script("spv.BrowserSniffer"), me)
      
      -- if we are not running as a LDM
      if not voidP(pWindow) then
        -- check file to display
        tSourceURL = externalParamValue("swURL")
        
        -- check settings passed to us
        tSettings = externalParamValue("swRemote")
        if stringP(tSettings) then
          toTokens = new(script "xml.tokenizer", tSettings, " =", TRUE)
          tlTokens = []
          
          repeat while toTokens.hasMoreTokens()
            tlTokens.add(toTokens.nextToken())
            if tlTokens.count >= 3 then
              if tlTokens[tlTokens.count-1] = "=" and tlTokens[tlTokens.count-2] <> "" then
                tValue = tlTokens[tlTokens.count]
                if tValue starts "'" and the last char of tValue = "'" or \
                  tValue starts QUOTE and the last char of tValue = QUOTE then
                  tValue = chars(tValue, 2, tValue.length-1)
                end if
                plSettings[tlTokens[tlTokens.count-2]] = tValue
              end if
            end if
          end repeat
        end if
      end if
    "Projector":
      
    "Author":
      -- if running inside Director, but not running as LDM, set file to open for testing
      if not voidP(pWindow) then
        tSourceURL = "../cathedral.jpg"
      end if
  end case
  
  -- create management objects
  poXML = new(script "xml.parser")
  
  poKeyHandler = new(script "spv.KeyHandler", me)
  poLocalConnection = new(script "spv.LocalConnection", me)
  poLicenseManager = new(script "spv.LicenseManager", me)
  
  poDownloader = new(script "spv.DownloadManager", me)
  
  -- init fps counter
  me.resetFPS()
  
  poLogger.show("Engine initialised", #info)
  
  me.updateRect()
  me.openURL(tSourceURL)  
end

on destroy me
  repeat with toObject in [poTour, poMouseManager, poNavigator, \
    poKeyHandler, poLocalConnection, poLicenseManager, poDownloader, poLogger, poXML, poPlugin]
    destroyObject(toObject)
  end repeat
  
  -- destroy cameras and groups
  if not voidP(pWorld) then
    pWorld.deleteCamera("_OverlayCamera")
    pWorld.deleteCamera("_SceneCamera")
    
    tlGroups = ["_Void", "_zScene#_spv.preview", "_zOverlay#_spv.management", "_zOverlay#_spv.branding", "_Scene", "_Overlays"]
    repeat with tGroupNr = 0 to 9
      tlGroups.add("_zScene#"&tGroupNr)
      tlGroups.add("_zOverlay#"&tGroupNr)
    end repeat
    repeat with tGroup in tlGroups
      pWorld.deleteGroup(tGroup)
    end repeat
    
    -- destroy common resources
    pWorld.deleteTexture("_spv.transparent")
    pWorld.deleteModel("_spv.ATIfix")
  end if
  
  if not voidP(member("_PanelBG")) then member("_PanelBG").erase()
  if not voidP(member("_PanoBG")) then member("_PanoBG").erase()
  
  if not voidP(pWorld) then pWorld.erase()
  
  plTimeouts = []
  
  pSprite.scriptInstanceList = []
  pSprite.puppet = false
end

on reset me
  if not voidP(poMouseManager) then tDisableCursor = poMouseManager.pDisableCursor
  repeat with toObject in [poTour, poMouseManager, poNavigator, poDownloader, poXML]
    destroyObject(toObject)
  end repeat
  
  -- create management objects
  poDownloader = new(script "spv.DownloadManager", me)
  
  poNavigator = new(script "spv.Navigator", me)
  poMouseManager = new(script "spv.MouseManager", me)
  poMouseManager.pDisableCursor=tDisableCursor
  -- reset camera rect
  pSceneCamera.rect = pOverlayCamera.rect
  
  pSimpleMode = false
end

on updateRect me
  if pSprite.member<>pWorld then exit
  if not voidP(pWindow) then
    if pWindow.rect.width>0 and pWindow.rect.height>0 then
      -- make sure sprite/canvas has the right dimensions, including browser specific fixes
      tRect = rect(0,0,pWindow.rect.width, pWindow.rect.height) 
      if getRendererServices().renderer <> #software then tRect = tRect + rect(0,pBrowserOffset,0,0)
    end if
  else
    if not voidP(poLDMscript) then
      tLDMrect = poLDMscript[#pRect]
      if voidP(tLDMrect) then tLDMrect = poLDMscript.rect
      if not voidP(tLDMrect) then
        tRect = rect(0,0, tLDMrect.width, tLDMrect.height)
      end if
    else tRect = rect(0,0,(the activeWindow).rect.width, (the activeWindow).rect.height)
  end if
  
  if pSprite.rect<>tRect then
    -- resize sprite
    pSprite.rect = tRect
    tCamRect = rect(0,0, tRect.width, tRect.height)
    pSceneCamera.rect = tCamRect
    pOverlayCamera.rect = tCamRect
    pOverlayCamera.orthoheight = tCamRect.height
    
    -- fix top uielement offset on directX renderers
    if string(the active3drenderer) starts "directX" then pOverlayCamera.transform.position.y = -1
    
    me.resetFPS()
    --pActive = true
    
    -- notify ui objects
    if not voidP(poTour) then
      if not voidP(poTour.poScene) then
        if not voidP(poTour.poScene[#plUIGroups]) then
          repeat with tlUIGroup in poTour.poScene.plUIGroups
            tlUIGroup[#object].updateLoc()
          end repeat
        end if
        
        if not voidP(poTour.poScene[#plUIElements]) then
          repeat with tlUIElement in poTour.poScene.plUIElements
            tlUIElement[#object].updateLoc()
          end repeat
        end if
      end if
    end if
  end if
end

on openURL me, aURL
  me.reset()
  
  if not voidP(aURL) and aURL<>"" then
    pSourceURL = aURL
    pSourceArgs = VOID
    
    -- if member specified, check type
    if aURL starts "#" then
      tURL = chars(aURL, 2, aURL.length)
      tMember = member(tURL)
      if not voidP(tMember) then tIsText = (tMember.type=#text)
      else if voidP(pWindow) then
        tell the activeWindow
          tMember = member(tURL)
          if not voidP(tMember) then tIsText = (tMember.type=#text)
        end tell
      end if
    else tURL = relativeURL(aURL, the moviePath)
    
    -- check for source arguments
    tOffset = offset("#", tURL)
    if tOffset>1 then
      the itemDelimiter = "#"
      pSourceArgs = tURL.item[2]
      pSourceURL = tURL.item[1]
      if aURL starts "#" then pSourceURL = "#" & pSourceURL
      --
    else if not (aURL starts "#") then pSourceURL = tURL
    
    tMessage = "Loading '" & pSourceURL & "'"
    if ["xml", "spv", "txt", "php", "asp", "aspx"].getOne(fileExt(pSourceURL)) > 0 or tIsText then
      pSimpleMode = false
      tURL = pSourceURL
    else
      -- if the input is not XML, build XML file around it
      pSimpleMode = true
      tMessage = tMessage & " in simple mode"
      tURL = "default.xml"
    end if
    poLogger.show(tMessage, #info)
    poDownloader.addJob(tURL, the moviePath, 0, #text, #analyseXML, me)
    
    if not voidP(poTour) then
      if not voidP(poTour.poScene) then poTour.poScene.destroy()
    end if
  else
    -- load empty scene if no source scene is specified
    poLogger.show("Showing empty document", #info)
    me.openURL("#_PanoBG")
  end if
end

on analyseXML me, alResult
  if voidP(alResult[#result]) then
    if not pSimpleMode then
      poLogger.show("No file to open", #error)
    else alResult[#result] = member("xmlTemplate").text
  end if
  
  tError = poXML.parseString(alResult[#result])
  if voidP(tError) then
    toTourNode = poXML["tour"]
    if not voidP(toTourNode) then
      poTour = new(script("spv.Tour"), me, toTourNode)
      poTour.initNode()
    else
      if pSimpleMode then
        poLogger.show("No tour node specified in 'default.xml', using default", #info)
        me.analyseXML(member("xmlTemplate").text)
      else poLogger.show("No tour node specified in XML file", #error)
    end if
  else
    if pSimpleMode then
      me.analyseXML([#result:member("xmlTemplate").text])
    else poLogger.show("Error parsing XML;" & tError, #error)
  end if
end

on getFPS me
  tMS = the milliseconds
  
  if not voidP(plFrameMS) and voidP(pDontCountMS) then
    if not voidP(pLastMS) then plFrameMS.add(tMS - pLastMS)
    
    if plFrameMS.count > 8 then
      tlFrameMS = plFrameMS.duplicate()
      tlFrameMS.sort()
      pFPS = 1000.0/tlFrameMS[tlFrameMS.count/2]
      
      if pFPS < 10 then
        if (the active3drenderer<>#software) and not voidP(poTour) and pActive then
          if not voidP(poTour.poScene) then
            poTour.poScene.scaleTextures(-1)
            me.resetFPS()
          end if
        end if
      end if
      
      if plFrameMS.count > 12 then plFrameMS.deleteAt(1)
    end if
  end if
  
  -- normalise
  if not voidP(pLastMS) then
    pFrameFactor = 30 * (tMS - pLastMS) / 1000.0
  else pFrameDelta = 1
  pLastMS = tMS
  pDontCountMS = void
end

on resetFPS me
  plFrameMS = []
  pLastMS = void
end

on getLastEvents me
  tLastClick = the lastClick
  if pActive and (the mouseDown or the rightMouseDown) then pLastClickOffset = tLastClick
  pLastClick = tLastClick - pLastClickOffset
  
  tLastKey = the lastKey
  if pActive and (the shiftDown or the commandDown or the optionDown) then pLastKeyOffset = tLastKey
  pLastKeyOffset = min(tLastKey, pLastKeyOffset)
  pLastKey = tLastKey - pLastKeyOffset
  
  pLastRoll = the lastRoll
end

on doTimeouts me
  repeat with tTimeout = plTimeouts.count down to 1
    tlTimeout = plTimeouts.getAt(tTimeout)
    if the milliseconds >= tlTimeout[#start] + tlTimeout[#period] then
      call(tlTimeout[#handler], tlTimeout[#object])
      tlTimeout[#start] = tlTimeout[#start] + tlTimeout[#period]
    end if
  end repeat
end

on inspectScene me
  if not voidP(pWorld) then
    pWorld.camera(1).rootNode = pSceneCamera.rootNode
    pWorld.camera(1).colorBuffer.clearAtRender = true
  end if
end

on randomID me, aShortName
  if voidP(pLastID) then pLastID = random(10000,99999)
  else pLastID = pLastID + random(1,9)
  
  tRandomID = aShortName & string(pLastID)
  pLastID = pLastID + random(1,9)
  return tRandomID
end

