-- Receive keyboard events

-- Author: Aldo Hoeben / fieldOfView, http://www.fieldofview.com
--

property parent

property pControlAlt

on new me, _parent
  parent = _parent
  parent.pSprite.scriptInstanceList.add(me)

  return me
end

on destroy me
  parent.pSprite.scriptInstanceList.deleteOne(me)
end destroy

on enterFrame me
  if the optionDown and the controlDown then
    if not pControlAlt then
      if not voidP(parent.poLicenseManager.poAboutGroup) then
        tAboutShowing = bool(getProperty(parent.poLicenseManager.poAboutGroup.attributeValue["id"], "visible"))
      end if
      tLogShowing = bool(getProperty("_spv.logger", "visible"))

      if not(tAboutShowing or tLogShowing) then
        parent.poLicenseManager.setState(true)
      else
        parent.poLogger.setState()
      end if

      pControlAlt = true
    end if
  else pControlAlt = false
end enterFrame

on handleKey me, aKeyCode, aCommandDown, aOptionDown, aShiftDown

end
